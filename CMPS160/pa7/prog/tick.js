/**
 * Responsible for animating the Scene.
 */
function tick(gl, shapes, canvas, shape) {
  //
  // YOUR CODE HERE
  //
  //console.log("in tick");

  // Recomendations: You're going to want to call this at the end of your main()
  // in main.js. requestAnimationFrame() needs to be used here (read the book).
  //console.log("vapes length in tick: " + shapes.geometries.length);
  var tock = function(){
    //console.log("calling tock");
    shapes.render(gl, false);
    resize(canvas);
    gl.viewport(0,0, gl.canvas.width, gl.canvas.height);
    requestAnimationFrame(tock, canvas);
  };
  tock();
}
