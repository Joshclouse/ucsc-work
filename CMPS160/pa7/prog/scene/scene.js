/**
 * Specifies a WebGL scene.
 *
 * @author "Josh Clouse"
 * @this {Scene}
 */
class Scene {
  /**
   * Constructor for Scene.
   *
   * @constructor
   */
  constructor(gl, canvas, normalShader) {
    //console.log("In constructor for shapes");
    this.geometries = []; // Geometries being drawn on canvas
    this.camera = new Camera(canvas);
    this.light = new Light();
    this.moveRight = true;
    this.normalShading = false;
    this.normalShader = normalShader;
    //
    // YOUR CODE HERE
    //

    // Recommendations: Setting the canvas's clear color and clearing the canvas
    // here is a good idea.
    gl.clearColor(0.137, 0.231, 0.423,1);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  }

  /**
   * Adds the given geometry to the the scene.
   *
   * @param {Geometry} geometry Geometry being added to scene
   */
  addGeometry(geometry) {
    this.geometries.push(geometry);
  }

  /**
   * Clears all the geometry within the scene.
   */
  clearGeometry(gl) {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    this.geometries = [];
    this.render(gl, false);
  }

  /**
   * Updates the animation for each geometry in geometries.
   */
  updateAnimation() {
    //
    // YOUR CODE HERE
    //

    // Recomendations: No rendering should be done here. Your Geometry objects
    // in this.geometries should update their animations themselves through
    // their own .updateAnimation() methods.
    for(var i = 0; i < this.geometries.length; i++){
      this.geometries[i].updateAnimation();
    }
  }

  /**
   * Renders all the Geometry within the scene.
   */
  render(gl, check) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: No calls to any of your GLSL functions should be made
    // here. Your Geometry objects in this.geometries should render themselves
    // through their own .render() methods.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //console.log("this.geometries.length: " + this.geometries.length);
    this.updateAnimation();
    if(this.light.position[0] < -2){
      this.moveRight = true;
    }
    else if(this.light.position[0] > 2){
      this.moveRight = false;
    }
    if(this.moveRight)
      this.light.position[0] += .1;
    else
      this.light.position[0] -= .1;
    //console.log("light position: " + this.light.position);
    //console.log("geometries.length: " + this.geometries.length);
    var fogColor = new Float32Array([0.137, 0.231, 0.423]);
    var fogDist = new Float32Array([.01, 1]);
    var eye = new Float32Array([this.camera.position.elements[0], this.camera.position.elements[1], this.camera.position.elements[2], 1.0]);
    for(var i = 0; i < this.geometries.length; i++){
      if(this.normalShading == false){
        useShader(gl, this.geometries[i].shader);
        //console.log(this.camera.viewMatrix.elements);
        //console.log(this.camera.projectionMatrix.elements);
        sendUniformVec4ToGLSL(gl, this.light.position, 'u_LightPosition');
        sendUniformVec4ToGLSL(gl, this.light.color, 'u_LightColor');
        sendUniformVec4ToGLSL(gl, this.light.ambient, 'u_AmbientLight');
        sendUniformMatToGLSL(gl, this.camera.viewMatrix, 'u_ViewMatrix');
        sendUniformMatToGLSL(gl, this.camera.projectionMatrix, 'u_ProjMatrix');
        sendUniformVec4ToGLSL(gl, fogColor, 'u_FogColor');
        sendUniformVec4ToGLSL(gl, fogDist, 'u_FogDist');
        //sendUniformVec4ToGLSL(gl, eye, 'u_Eye');
      }
      else{
        useShader(gl, this.normalShader);
        sendUniformMatToGLSL(gl, this.camera.viewMatrix, 'u_ViewMatrix');
        sendUniformMatToGLSL(gl, this.camera.projectionMatrix, 'u_ProjMatrix');
      }
      
      
      this.geometries[i].render(gl, this.normalShading, check);
    }
  }
}
