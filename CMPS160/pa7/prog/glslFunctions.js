/**
 * Sends a WebGL 2D texture object (created by load2DTexture) and sends it to
 * the shaders.
 *
 * @param val The WebGL 2D texture object being passed
 * @param {Number} textureUnit The texture unit (0 - 7) where the texture will reside
 * @param {String} uniformName The name of the uniform variable where the texture's
 * textureUnit location (0 - 7) will reside
 */
function send2DTextureToGLSL(gl, val, textureUnit, uniformName) {
  //
  // YOUR CODE HERE
  //
  //console.log("in send2DTexture");
  var u_Sampler = gl.getUniformLocation(gl.program, uniformName);
  if (!u_Sampler) {
    console.log('Failed to get the storage location of u_Sampler');
    return false;
  }
  
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, val);
  gl.uniform1i(u_Sampler, textureUnit);

  // Recomendations: Within this funciton, you should:
  //    1. Gather your uniform location
  //    2. Determine the exture unit you will be using (gl.TEXTURE"N")
  //    3. Activate your texture unit using gl.activeTexture
  //    4. Bind your texture using gl.bindTexture
  //    5. Send the texture unit (textureUnit not the one you found) to your
  //       uniform location.
}

/**
 * Creates a WebGl 2D texture object.
 *
 * @param imgPath A file path/data url containing the location of the texture image
 * @param magParam texParameteri for gl.TEXTURE_MAG_FILTER. Can be gl.NEAREST,
 * gl.LINEAR, etc.
 * @param minParam texParameteri for gl.TEXTURE_MIN_FILTER. Can be gl.NEAREST,
 * gl.LINEAR, etc.
 * @param wrapSParam texParameteri for gl.TEXTURE_WRAP_S. Can be gl.REPEAT,
 * gl. MIRRORED_REPEAT, or gl.CLAMP_TO_EDGE.
 * @param wrapTParam texParameteri for gl.TEXTURE_WRAP_S. Can be gl.REPEAT,
 * gl. MIRRORED_REPEAT, or gl.CLAMP_TO_EDGE.
 * @param callback A callback function which executes with the completed texture
 * object passed as a parameter.
 */
function create2DTexture(gl, imgPath, magParam, minParam, wrapSParam, wrapTParam, callback) {
  //
  // YOUR CODE HERE
  //
  //console.log("imagePath: " + imgPath);
  var image = new Image();
  var texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);

  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,1,1,0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([255,0,255,255]));
  image.onload = function(){
    console.log("in onload");
    
    
    //console.log("created texture");
    
    
    if(!texture){
      console.log("Failed to create the texture object");
      return false;
    }
    gl.bindTexture(gl.TEXTURE_2D, texture);
    //console.log("gay");
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
    
    //console.log(texture);
    callback(texture);
    
  };
  
  image.src = imgPath;
  return texture;
  // Recomendations: This function should see you creating an Image object,
  // setting that image object's ".onload" to an anonymous function containing
  // the rest of your code, and setting that image object's ".src" to imgPath.
  //
  // Within the anonymous function:
  //  1. create a texture object by saving the result of gl.createTexture()
  //  2. Flip your image's y-axis and bind your texture object to gl.TEXTURE_2D
  //  3. Using multiple calls to gl.texParameteri, pass magParam, minParam,
  //     wrapSParam, and wrapTParam.
  //  4. Set the texture's image to the loaded image using gl.texImage2D
  //  5. Pass your completed texture object to your callback function
  //
  // NOTE: This function should not return anything.
}

/**
 * Sends data to a uniform variable expecting a matrix value.
 *
 * @private
 * @param {Array} val Value being sent to uniform variable
 * @param {String} uniformName Name of the uniform variable recieving data
 */
 //should take in the gl, the modelMatrix(val) and the uniformName of the GL variable
 function sendUniformMatToGLSL(gl, val, uniformName) {
  var u_ModelMatrix = gl.getUniformLocation(gl.program, uniformName);
  if(!u_ModelMatrix){
    console.log('Failed to get the storage location of ' + uniformName);
    return;
  }
  gl.uniformMatrix4fv(u_ModelMatrix, false, val.elements);
}

/**
 * Sends data to an attribute variable using a buffer.
 *
 * @private
 * @param {Float32Array} data Data being sent to attribute variable
 * @param {Number} dataCount The amount of data to pass per vertex
 * @param {String} attribName The name of the attribute variable
 */
function sendAttributeBufferToGLSL(gl, data, dataCount, attribName, offset, numValues) {
  var vertexBuffer = gl.createBuffer();
  if(!vertexBuffer){
    console.log("Failed to create the buffer object");
    return -1;
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  var a_Position = gl.getAttribLocation(gl.program, attribName);
  var FSIZE = data.BYTES_PER_ELEMENT;
  if(a_Position < 0){
    console.log("Failed to get the storage location of " + attribName);
    return -1;
  }
  //puts the data in for the position, FSIZE * 7 for the x,y,z and r,g,b,A
  gl.vertexAttribPointer(a_Position, numValues, gl.FLOAT, false, FSIZE * dataCount, FSIZE * offset);
  gl.enableVertexAttribArray(a_Position);
  
  //set up variables for color
  // var a_Color = gl.getAttribLocation(gl.program, 'a_Color');
  // if(a_Color < 0){
  //   console.log("Failed to get the storage location of a_Color");
  //   return -1;
  // }
  // gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * dataCount, FSIZE * 3);
  // gl.enableVertexAttribArray(a_Color);
  // gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

/**
 * Draws the current buffer loaded. Buffer was loaded by sendAttributeBufferToGLSL.
 *
 * @param {Integer} pointCount The amount of vertices being drawn from the buffer.
 */
function tellGLSLToDrawCurrentBuffer(gl, pointCount) {
  //console.log("point count: " + pointCount);
  gl.drawArrays(gl.TRIANGLES, 0, pointCount);
}

/**
 * Sends a float value to the specified uniform variable within GLSL shaders.
 * Prints an error message if unsuccessful.
 *
 * @param {float} val The float value being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformFloatToGLSL(gl, val, uniformName) {
  gl.uniform1f(uniformName, val);
}

/**
 * Sends an JavaSript array (vector) to the specified uniform variable within
 * GLSL shaders. Array can be of length 2-4.
 *
 * @param {Array} val Array (vector) being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformVec4ToGLSL(gl, val, uniformName) {
  var uniform = gl.getUniformLocation(gl.program, uniformName);
  if(!uniform){
    console.log("Failed to get the storage location: " + uniformName);
    return;
  }
  if(val.length == 2)
    gl.uniform2f(uniform, val[0], val[1]);
  else if(val.length == 3)
    gl.uniform3f(uniform, val[0], val[1], val[2]);
  else
    gl.uniform4f(uniform, val[0], val[1], val[2], val[3]);
}
