/**
 * Specifies a Square. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Square}
 */
class Square extends Geometry {
  /**
   * Constructor for Square.
   *
   * @constructor
   * @param {Number} size The size of the square drawn
   * @param {Number} centerX The center x-position of the square
   * @param {Number} centerY The center y-position of the square
   */
  constructor(shader, rainbow, size, centerX, centerY, red, green, blue) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Remember that Square is a subclass of Geometry.
    // "super" keyword can come in handy when minimizing code reuse.
    super();
    this.shader = shader;
    this.generateSquareVertices(rainbow, size, centerX, centerY, red, green, blue);
  }

  /**
   * Generates the vertices of the square.
   *
   * @private
   * @param {Number} size The size of the square drawn
   * @param {Number} centerX The center x-position of the square
   * @param {Number} centerY The center y-position of the square
   */
  generateSquareVertices(rainbow, size, centerX, centerY, red, green, blue) {
    var z = 0.0;
    var radians;
    var j = 0;
    for(var i = 45; i <= 405; i+= 360/4){
      var x = (Math.sin(i *(Math.PI/180))*size);// + centerX;
      var y = (Math.cos(i *(Math.PI/180))*size);// + centerY;
      //console.log("X,Y in square: " + centerX + " " + centerY);
      //console.log("rainbow is: " + rainbow);
      if(!rainbow){
        this.vertices[j] = super.createVertex(x, y, z, red, green,blue);
        
      }
        
      else{
        if(j == 0)
          this.vertices[j] = super.createVertex(x,y,z, 1.0, 0, 0);
        if(j==1)
          this.vertices[j] = super.createVertex(x,y,z, 0,1.0,0);
        if(j==2)
          this.vertices[j] = super.createVertex(x,y,z, 0,0,1.0);
        if(j==3)
          this.vertices[j] = super.createVertex(x,y,z, 0,1.0,0);
      }
      j++;
    }
    this.vertices[4] = this.vertices[0];
    this.vertices[5] = this.vertices[2];
    //console.log("created square: " + this.vertices[0].points + this.vertices[0].color);
    //console.log("created square: " + this.vertices[1].points + this.vertices[1].color);
  }
  
}
