/**
 * Specifies a tilted cube which rotates.
 *
 * @author Josh Clouse
 * @this {TiltedCube}
 */
class TiltedCube extends Geometry {
  /**
   * Constructor for TiltedCube.
   *
   * @constructor
   * @returns {TiltedCube} Geometric object created
   */
  constructor(shader, size, centerX, centerY, red, green ,blue) {
    super();
    this.shader = shader;
    this.g_last = Date.now();
    this.offsetX = centerX;
    this.offsetY = centerY;
    this.currentAngle = 0.0;
    this.ANGLE_STEP = 45.0;
    this.generateCubeVertices(size, centerX, centerY, red, green,blue);

    // Recommendations: Might want to tilt your cube at 30 degrees relative to
    // the z-axis here. Pretty good tilt that lets us see that it's a cube.
  }

  /**
   * Generates the vertices of TiltedCube. Just a regular cube.
   *
   * @private
   */
  generateCubeVertices() {
    var z = 0.0;
    var radians;
    var j = 0;
    //this should create the first square in the front of the cube
    for(var i = 0; i <= 360; i+= 360/4){
      var x = (Math.sin(i *(Math.PI/180))*size);// + centerX;
      var y = (Math.cos(i *(Math.PI/180))*size);// + centerY;
      //console.log("X,Y in square: " + centerX + " " + centerY);
      this.vertices[j] = super.createVertex(x, y, .75 * size, red, green, blue);
      j++;
    }
    this.vertices[4] = this.vertices[0];
    this.vertices[5] = this.vertices[2];
    var l = 6;
    //this should create the second square in the back of the first square
    for(i = 0; i < 6; i++){
      var vert = this.vertices[i]; 
      this.vertices[l] = super.createVertex(vert.points[0], vert.points[1], vert.points[2] * -1, red, green, blue);
      l++;
    }
    //this should make the top right side of the cube
    for(var i = 0; i < 2; i++){
      this.vertices.push(this.vertices[i]);
      this.vertices.push(this.vertices[i+6]);
    }
    this.vertices.push(this.vertices[6]);
    this.vertices.push(this.vertices[1]);
    
    //this should make the bottom right side of the cube
    for(var i = 12; i < 18; i++){
      var vert = this.vertices[i];
      this.vertices.push(super.createVertex(vert.points[0], (-1 * vert.points[1]), vert.points[2], red, green, blue));
    }

    //this should make the bottom left side of the cube
    for(var i = 12; i < 18; i++){
      var vert = this.vertices[i];
      this.vertices.push(super.createVertex((-1 * vert.points[0]), (-1 * vert.points[1]), vert.points[2], red, green, blue));
    }

    //this should make the top left side of the cube
    for(var i = 12; i < 18; i ++){
      var vert = this.vertices[i];
      this.vertices.push(super.createVertex((-1 * vert.points[0]), vert.points[1], vert.points[2], red, green, blue));
    }
    //console.log("cube vert length: " + this.vertices.length);
    for(var i = 0; i < this.vertices.length; i++){
      //console.log(i + ": " + this.vertices[i].points[0] + ", " + this.vertices[i].points[1] + ", " +this.vertices[i].points[2]);
      //console.log(i + ": " + this.vertices[i].color[0] + ", " + this.vertices[i].color[1] + ", " +this.vertices[i].color[2]);
    }
    // Recommendations: Might want to generate your cube vertices so that their
    // x-y-z values are combinations of 1.0 and -1.0. Allows you to scale the
    // the cube to your liking better.
  }
  /**
   * Updates the animation of the TiltedCube. Should make it rotate.
   */
  updateAnimation() {
    var now = Date.now();
    var elapsed = now - this.g_last;
    this.g_last = now;
    this.currentAngle = this.currentAngle + (this.ANGLE_STEP * elapsed) / 350.0;
    this.currentAngle %= 360;
    this.modelMatrix.setTranslate(this.offsetX, this.offsetY, 0);
    this.modelMatrix.rotate(this.currentAngle, .25, 1, .5);
    //console.log("in update animation tiltedCube");
    //console.log(this.modelMatrix);
    // Recommendations: Do not simply apply a rotation matrix. Doing so will
    // cause your cube to spin in a circle around the axis you've chosen.
    //
    // Keep in mind that no rendering should be done here. updateAnimation()'s
    // purpose is to update the geometry's modelMatrix and any other variables
    // related to animation. It should be the case that after I call
    // updateAnimation() I should be able to call render() elsewhere and have my
    // geometry complete a frame of animation.
  }
}
