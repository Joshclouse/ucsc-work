I'll write what I write here in the canvas submission notes as well.
Basically this is the same as the last assignment except I added in fog and selectable obj(the grey/white cubes)
When you select a cube it should turn yellow indicating that it has been selected. There is a slight issue with selecting cubes due to the fog and shading it can make the
colors unreadable from too far away so to get the best results stand closer to the cube you want to select. There should be 3 grey cubes each one holding a track from a 
short song I made. When you click on a cube that track will start playing. Click on all three and you will get the complete song or click on individual ones for their own
respective parts. After the first loop of the song it starts to desync so you might want to refresh your page.