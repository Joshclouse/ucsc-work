/**
 * Specifies a Triangle. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Triangle}
 */
class Triangle extends Geometry {
  /**
   * Constructor for Triangle.
   *
   * @constructor
   * @param {Number} size The size of the triangle drawn
   * @param {Number} centerX The center x-position of the triangle
   * @param {Number} centerY The center y-position of the triangle
   */
  constructor(size, centerX, centerY) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Remember that Triangle is a subclass of Geometry.
    // "super" keyword can come in handy when minimizing code reuse.
    super();
    this.generateTriangleVertices(size, centerX, centerY);
  }

  /**
   * Generates the vertices of the Triangle.
   *
   * @private
   * @param {Number} size The size of the triangle drawn
   * @param {Number} centerX The center x-position of the triangle
   * @param {Number} centerY The center y-position of the triangle
   */
  generateTriangleVertices(size, centerX, centerY) {
    var z = 0.0;
    //console.log("centerx and y: " + centerX + " " + centerY);
    //console.log("size in triangle: " + size);
    var x1 = (Math.sin(240 * (Math.PI/180)))*size;
    var y1 = (Math.cos(240 * (Math.PI/180)))* size;
    var x2 = (Math.sin(120 * (Math.PI/180)))*size;
    var y2 = (Math.cos(120 * (Math.PI/180)))*size;
    var x3 = (Math.sin(0 * (Math.PI/180)))*size;
    var y3 = (Math.cos(0 * (Math.PI/180)))*size;
    this.vertices[0] = super.createVertex(centerX + x1, centerY + y1, z);
    this.vertices[1] = super.createVertex(centerX + x2, centerY + y2, z);
    this.vertices[2] = super.createVertex(centerX + x3, centerY + y3, z);
  }
}
