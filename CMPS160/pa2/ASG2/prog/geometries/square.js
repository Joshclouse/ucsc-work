/**
 * Specifies a Square. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Square}
 */
class Square extends Geometry {
  /**
   * Constructor for Square.
   *
   * @constructor
   * @param {Number} size The size of the square drawn
   * @param {Number} centerX The center x-position of the square
   * @param {Number} centerY The center y-position of the square
   */
  constructor(size, centerX, centerY) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Remember that Square is a subclass of Geometry.
    // "super" keyword can come in handy when minimizing code reuse.
    super();
    this.generateSquareVertices(size, centerX, centerY);
  }

  /**
   * Generates the vertices of the square.
   *
   * @private
   * @param {Number} size The size of the square drawn
   * @param {Number} centerX The center x-position of the square
   * @param {Number} centerY The center y-position of the square
   */
  generateSquareVertices(size, centerX, centerY) {
    var z = 0.0;
    var radians;
    var j = 0;
    for(var i = 45; i <= 405; i+= 360/4){
      var x = (Math.sin(i *(Math.PI/180))*size) + centerX;
      var y = (Math.cos(i *(Math.PI/180))*size) + centerY;
      console.log("X,Y in square: " + x + " " + y);
      this.vertices[j] = super.createVertex(x, y, z);
      j++;
    }
    this.vertices[4] = this.vertices[0];
    this.vertices[5] = this.vertices[2];
  }
}
