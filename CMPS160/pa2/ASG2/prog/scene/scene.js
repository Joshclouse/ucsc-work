/**
 * Specifies a WebGL scene.
 *
 * @author "Josh Clouse"
 * @this {Scene}
 */
class Scene {
  /**
   * Constructor for Scene.
   *
   * @constructor
   */
  constructor(gl) {
    this.geometries = []; // Geometries being drawn on canvas
    //
    // YOUR CODE HERE
    //

    // Recommendations: Setting the canvas's clear color and clearing the canvas
    // here is a good idea.
    gl.clearColor(0,0,0,1);
    gl.clear(gl.COLOR_BUFFER_BIT);
  }

  /**
   * Adds the given geometry to the the scene.
   *
   * @param {Geometry} geometry Geometry being added to scene
   */
  addGeometry(geometry) {
    this.geometries.push(geometry);
  }

  /**
   * Clears all the geometry within the scene.
   */
  clearGeometry(gl, u_FragColor) {
    gl.clear(gl.COLOR_BUFFER_BIT);
    this.geometries = [];
    this.render(gl, u_FragColor);
  }

  /**
   * Renders all the Geometry within the scene.
   */
  render(gl, u_FragColor) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: No calls to any of your GLSL functions should be made
    // here. Your Geometry objects in this.geometries should render themselves
    // through their own .render() methods.
    //console.log("lenght of geometries: " + this.geometries.length);
    for(var i = 0; i < this.geometries.length; i++){
      //console.log("inside render loop in scene: " + i);
      this.geometries[i].render(gl, u_FragColor);
    }
  }
}
