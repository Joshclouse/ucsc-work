/**
 * Specifies a Circle. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Circle}
 */
class Circle extends Geometry {
  /**
   * Constructor for Circle.
   *
   * @constructor
   * @param {Number} radius The radius of the circle being constructed
   * @param {Integer} segments The number of segments composing the circle
   * @param {Number} centerX The central x-position of the circle
   * @param {Number} centerY The central y-position of the circle
   */
  constructor(shader, rainbow, radius, segments, centerX, centerY, red, green, blue) {
    super();
    this.shader = shader;
    this.generateCircleVertices(rainbow, radius, segments, centerX, centerY, red, green, blue);
  }

  /**
   * Generates the vertices of the Circle.
   *
   * @private
   * @param {Number} radius The radius of the circle being constructed
   * @param {Integer} segments The number of segments composing the circle
   * @param {Number} centerX The central x-position of the circle
   * @param {Number} centerY The central y-position of the circle
   */
  generateCircleVertices(rainbow, radius, segments, centerX, centerY, red, green, blue) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Might want to call this within your Circle constructor.
    // Keeps your code clean :)
    var z =0.0;
    var j = 0;
    for(var i = 360/segments; i <=360 + (360/segments); i+=(360/segments)){
      if(rainbow)
        this.vertices[j] = super.createVertex(centerX, centerY, z, 1.0, 0, 0);
      else
        this.vertices[j] = super.createVertex(centerX, centerY, z, red, green, blue);
      j++;
      //console.log("prev i: " + (i - (360/segments)));
      var x = ((Math.sin((i - (360/segments)) * (Math.PI/180))) * radius) + centerX;
      var y = ((Math.cos((i - (360/segments)) * (Math.PI/180)))* radius) + centerY;
      if(rainbow)
        this.vertices[j] = super.createVertex(x,y,z, 0, 1.0, 0);
      else
        this.vertices[j] = super.createVertex(x,y,z, red, green, blue);
      j++;
      x = (Math.sin(i * (Math.PI/180)) * radius) + centerX;
      y = (Math.cos(i * (Math.PI/180))* radius) + centerY;
      if(rainbow)
        this.vertices[j] = super.createVertex(x, y, z, 0, 0, 1.0);
      else
        this.vertices[j] = super.createVertex(x,y,z, red,green,blue);
      //console.log("current i: " + i);
      j++;
    }
    for(var p = 0; p < this.vertices.length; p++){
      console.log("circle verts are: " + this.vertices[p].points + "\n" + this.vertices[p].color);
    }
  }
}
