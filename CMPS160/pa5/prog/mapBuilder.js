function buildMap(geomArray, colorArray, shader, shapes){
    console.log("In map builder");
    //Color for white is 255, 251, 251, 255
    //color for black is 0, 0, 0, 255
    //we set size to .125 because we go from -1 to 1 i.e. 2 so 2/16(the number of pixels) = .125
    var size = .125;
    //we will draw starting at the top right corner
    var row = 1;
    var col = -1;
    var z = 0;
    console.log(colorArray.length);
    for(var i = 0; i < geomArray.length; i+=4){
        //console.log(i);
        //if value is white then make a cube that is at position y = .5 aka the wall
        if(geomArray[i] == 255 && geomArray[i+1] == 255 && geomArray[i+2] == 255 && geomArray[i+3] == 255){
            var c = new Cube(shader, size, col, .09, row, colorArray[i]/255, colorArray[i+1]/255, colorArray[i+2]/255);
            //console.log("Building new cube");
            shapes.geometries.push(c);
        }
        //if value is black then make a cube that is at position y = -.5 aka the floor
        // else if(geomArray[i] == 0 && geomArray[i+1] == 0 && geomArray[i+2] == 0 && geomArray[i+3] == 255){
        //     //console.log("building new cube");
        //     var c = new Cube(shader, size, col, -.09, row, colorArray[i], colorArray[i+1], colorArray[i+2]);
        //     shapes.geometries.push(c);
        // }
        //if we have reached the last row in the column we need to start the next column
        if(col >= 1){
            col = -1;
            row -= size;
        }
        else{
            col += size;
        }
        
    }
}