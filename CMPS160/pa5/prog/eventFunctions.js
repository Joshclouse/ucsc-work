
var red = 1.0;
var blue = 0.0;
var green = 0.0;
var size = .1;
var mousePressed = false;
var segments = 3;
var shape = "triangle";
var isRainbow = false;
var textureLoaded = false;
var boyText;
var g_near = 0.01;
var g_far = 5;
var POVAngle = 50;
var ortho = 'ortho';
var movement = .1;
var firstX;
var angle = 0;
var lookAt = [0, -1];
function initEventHandelers(loadMap, textured, nonTextured, loadTexture, load, shapes, canvas, gl, clearButton, POV, nearSlider, farSlider,orthoButton, perspectiveButton) {
  canvas.onmousedown = function(ev){mousePressed = true; firstX = ev.clientX;};
  canvas.onmouseup = function(){mousePressed = false;};
  canvas.onmousemove = function(ev){rotateCamera(ev, shapes, canvas);};
  document.body.onkeydown = function(ev){startMoving(ev, shapes);};
  document.body.onkeypress = function(ev){startMoving(ev, shapes);};
  clearButton.onclick = function(){shapes.clearGeometry(gl);};
  load.onclick = function(){loadFileAsText(shapes, nonTextured)};
  loadTexture.onclick = function(){textureToImage(imageSrc); textureLoaded = true;};
  loadMap.onclick = function(){createMap(nonTextured, shapes);};
  POV.oninput = function(){changePOV(POV, shapes, canvas);};
  nearSlider.oninput = function(){changeNear(nearSlider, shapes, canvas);};
  farSlider.oninput = function(){changeFar(farSlider, shapes, canvas);};
  perspectiveButton.onclick = function(){ortho = 'perspective';changePerspective(shapes, canvas)};
  orthoButton.onclick = function(){ortho = 'ortho';changePerspective(shapes, canvas)};
}

/**
 * Function called upon mouse click or mouse drag. Computes position of cursor,
 * pushes cursor position as GLSL coordinates, and draws.
 *
 * @param {Object} ev The event object containing the mouse's canvas position
 */
function click(imageSrc, textured, nonTextured, ev, gl, canvas, shapes) {
  if(mousePressed == false)
    return;
    var x = ev.clientX;
    var y = ev.clientY;
    var rect = ev.target.getBoundingClientRect();
    x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
    y = (canvas.height/2 - (y-rect.top))/ (canvas.height/2);
    if(shape == "triangle"){
      var t = new FluctuatingTriangle(nonTextured, isRainbow, size, x, y, red, green, blue);
      shapes.geometries.push(t);
    }
    if(shape == "square"){
      var s = new SpinningSquare(nonTextured, isRainbow, size, x, y, red , green, blue);
      shapes.geometries.push(s);
    }
    if(shape == "circle"){
      var c = new RandomCircle(nonTextured, isRainbow, size, segments, x, y, red, green, blue);
      shapes.geometries.push(c);
    }
    if(shape == "cube"){
      if(!textureLoaded){
        var q = new TiltedCube(nonTextured, size, x, y, red, green, blue);
        shapes.geometries.push(q);
      }
      else{
        var qt = new MultiTextureCube(gl, imageSrc, textured, size, x,y);
        shapes.geometries.push(qt);
      }
    }
    clearCanvas(gl);
    shapes.render(gl, shape);
}

/**
 * Clears the HTML canvas.
 */
function clearCanvas(gl) {
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
}

/**
 * Changes the size of the points drawn on HTML canvas.
 *
 * @param {float} size Real value representing the size of the point.
 */
function changePointSize(slider) {
  size = slider.value/100.0;
}

/**
 * Changes the color of the points drawn on HTML canvas.
 *
 * @param {float} color Color value from 0.0 to 1.0.
 */
function changePointColor(slider,color) {
  //if the color is red
  if(color == 1){
    red = (.01 * slider.value);
  }
  else if(color == 2){ //color is green
    green = (.01 * slider.value);
  } 
  else if(color == 3){ //color is blue
    blue = (.01 * slider.value);
  }
}
function changeSegmentCount(slider){
  segments = slider.value;
}
function loadFileAsText(shapes, shade){
  var objFile = document.getElementById('objFile').files[0];
  var fileReader = new FileReader();
  fileReader.onload = function(fileLoadedEvent){
    console.log("in onload");
    var textFromFileLoaded = fileLoadedEvent.target.result;
    console.log(textFromFileLoaded);
    var loadObj = new LoadedOBJ(textFromFileLoaded, shade);

    loadObj.color = [red, green, blue, 1.0];
    console.log(loadObj.vertices.length);
    //console.log("vert 1: " + loadObj.vertices[0].points[0] + ", " + loadObj.vertices[0].points[1] + ", " + loadObj.vertices[0].points[2]);
    shapes.geometries.push(loadObj);
    //shapes.render();
  };
  fileReader.readAsText(objFile, "UTF-8");
}

function textureToImage(boy){
  // let imageReader = new FileReader();
  //  imageReader.addEventListener("load", function(){
  //    imageSrc = imageReader.result;
  //    console.log(imageSrc);
  //  }, false);
  // imageReader.readAsDataURL(document.getElementById('textureFile').files[0]);
  boy = 'checkerboard.png';
  boyText = boy;
  console.log(boy);
}

function resize(canvas){
  var displayWidth = canvas.clientWidth;
  var displayHeight = canvas.clientHeight;
  if(canvas.width != displayWidth || canvas.height != displayHeight){
    canvas.width = displayWidth;
    canvas.height = displayHeight;
  }
}

function createMap(shade, shapes){
  var mapGeometry = 'Map_Geom.png';
  var mapColors = 'Map_Color.png';
  var obj1 = 'cat.obj';
  var obj2 = 'teapot.obj';
  var mapC = new Image();
  var mapG = new Image();
  var mapGeoArray;
  var mapColArray;
  mapG.onload = function(){
    //the array holding the rgb geometry array
    mapGeoArray = sampleImageColor(mapG);
    //once we load the geometry we also want to load the colors
    mapC.onload = function(){
      //the array holding the rgb color array
      mapColArray = sampleImageColor(mapC);
      //We only want to start building the map once we have loaded the geometry and the colors so we put
      //our build map functionality in here
      buildMap(mapGeoArray, mapColArray, shade, shapes);
      //console.log("after map builder");
      //console.log("The color array: \n" + mapColArray);
    };
    //console.log("The geometry array: \n" + mapGeoArray);
  };
  //Color for white is 255, 251, 251, 255
  //color for black is 0, 0, 0, 255
  
  mapG.src = mapGeometry;
  mapC.src = mapColors;
  
  
  
}

function changePOV(slider, shapes, canvas){
  POVAngle = slider.value;
  console.log(POVAngle);
  if(ortho == 'perspective'){
    shapes.camera.perspective = POVAngle;
  }
  else{
    shapes.camera.ortho = [POVAngle * -.01, POVAngle * .01];
  }
  shapes.camera.setProjection(ortho, canvas)
}

function changeNear(slider, shapes, canvas){
  console.log("Change near");
  g_near = slider.value*.1 + .01;
  shapes.camera.near = g_near;
  shapes.camera.setProjection(ortho, canvas);
}
function changeFar(slider, shapes, canvas){
  console.log("Change far");
  g_far = slider.value*.1 + .01;
  shapes.camera.far = g_far;
  shapes.camera.setProjection(ortho, canvas);
}
function changePerspective(shapes, canvas){
  if(ortho == 'perspective'){
    shapes.camera.setProjection(ortho, canvas);
  }
  else{
    shapes.camera.setProjection(ortho, canvas);
  }
}

function startMoving(ev, shapes){
  if(ev.keyCode == 83){
    //console.log("changin posiiong");
    shapes.camera.position.elements[0] -= lookAt[0] * movement;
    shapes.camera.position.elements[2] -= lookAt[1] * movement;
    shapes.camera.center.elements[0] -= lookAt[0] * movement;
    shapes.camera.center.elements[2] -= lookAt[1] * movement;
  }
  if(ev.keyCode == 87){
    //console.log("test");
    shapes.camera.position.elements[0] += lookAt[0] * movement;
    shapes.camera.position.elements[2] += lookAt[1] * movement;
    shapes.camera.center.elements[0] += lookAt[0] * movement;
    shapes.camera.center.elements[2] += lookAt[1] * movement;
  }
  if(ev.keyCode == 65){
    //console.log("test");
    shapes.camera.position.elements[0] -= -1 *lookAt[1] * movement;
    shapes.camera.position.elements[2] -= lookAt[0] * movement;
    shapes.camera.center.elements[0] -= -1 *lookAt[1] * movement;
    shapes.camera.center.elements[2] -= lookAt[0] * movement;
  }
  if(ev.keyCode == 68){
    //console.log("test");
    shapes.camera.position.elements[0] += -1 *lookAt[1] * movement;
    shapes.camera.position.elements[2] += lookAt[0] * movement;
    shapes.camera.center.elements[0] += -1 *lookAt[1] * movement;
    shapes.camera.center.elements[2] += lookAt[0] * movement;
  }
  shapes.camera.updateViewMatrix();
}

function rotateCamera(ev, shapes, canvas){
  //console.log("moving camera");
  if(mousePressed == true){
    var newX = ev.clientX;
    if(firstX != newX){
      angle = (angle + (newX - firstX))%360;
      firstX = newX;
  
    }
    //console.log(angle);
    
    lookAt[0] = Math.sin(angle * (Math.PI / 180));
    lookAt[1] = -Math.cos(angle * (Math.PI/180));
    shapes.camera.center.elements[0] = shapes.camera.position.elements[0] + lookAt[0];
    shapes.camera.center.elements[2] = shapes.camera.position.elements[2] + lookAt[1];
    //console.log(lookAt);
    shapes.camera.updateViewMatrix();
  }
  
}