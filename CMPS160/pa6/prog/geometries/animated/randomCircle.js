/**
 * Specifies a circle which moves randomly.
 *
 * @author Josh Clouse
 * @this {RandomCircle}
 */
class RandomCircle extends Circle {
  /**
   * Constructor for RandomCircle.
   *
   * @constructor
   * @param {Number} radius The radius of the random circle being constructed
   * @param {Integer} segements The number of segments composing the circle
   * @param {Number} centerX The x-position of the circle being constructed
   * @param {Number} centerY The y-position of the circle being constructed
   * @returns {RandomCircle} RandomCircle object created
   */
  constructor(shader, rainbow, radius, segments, centerX, centerY, red, green, blue) {
    super(shader, rainbow, radius, segments, centerX, centerY, red, green, blue);
    this.g_last = Date.now();
    this.offsetX = 0;
    this.offsetY = 0;
    this.centerX = centerX;
    this.centerY = centerY;
    //console.log("centerX: " + centerX);
    // Recomendations: You're going to need a few variables to keep track of
    // information relevant to your animation. For example, a circle is going
    // to need a variable to keep track of the direction the circle is moving.
  }

  /**
   * Updates random circle's animation. Changes modelMatrix into a translation
   * matrix translating into a random direction.
   */
  updateAnimation() {
    
    var now = Date.now();
    var elapsed = now - this.g_last;
    this.g_last = now;
    var x = Math.floor(Math.random() * Math.floor(3));
    var y = Math.floor(Math.random() * Math.floor(3));
    //console.log(x);
    //console.log(y);
    if(x == 0)
      x = -.5;
    else if(x==1)
      x = 0;
    else
      x = .5;
    if(y == 0)
      y = -.5;
    else if(y== 1)
      y= 0;
    else
      y = .5;
    
    var tempX = this.offsetX + (x * elapsed) / 1000.0;
    if(tempX + this.centerX < 1 && tempX + this.centerX > -1)
      this.offsetX = tempX;
    var tempY = this.offsetY + (y * elapsed) / 1000.0;
    if(tempY + this.centerY < 1 && tempY + this.centerY > -1)
      this.offsetY = tempY;
    //this.modelMatrix.setIdentity();
    //console.log(this.currentX);
    this.modelMatrix.setTranslate(this.offsetX, this.offsetY, 0); 
    // Recomendations: Refer to README.txt for more detalied recommendations
    //
    // Keep in mind that no rendering should be done here. updateAnimation()'s
    // purpose is to update the geometry's modelMatrix and any other variables
    // related to animation. It should be the case that after I call
    // updateAnimation() I should be able to call render() elsewhere and have my
    // geometry complete a frame of animation.
  }

}
