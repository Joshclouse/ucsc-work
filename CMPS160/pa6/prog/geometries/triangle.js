/**
 * Specifies a Triangle. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Triangle}
 */
class Triangle extends Geometry {
  /**
   * Constructor for Triangle.
   *
   * @constructor
   * @param {Number} size The size of the triangle drawn
   * @param {Number} centerX The center x-position of the triangle
   * @param {Number} centerY The center y-position of the triangle
   */
  constructor(shader, rainbow, size, centerX, centerY, red, green, blue) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Remember that Triangle is a subclass of Geometry.
    // "super" keyword can come in handy when minimizing code reuse.
    super();
    this.shader = shader;
    this.generateTriangleVertices(rainbow, size, centerX, centerY, red, green, blue);
  }

  /**
   * Generates the vertices of the Triangle.
   *
   * @private
   * @param {Number} size The size of the triangle drawn
   * @param {Number} centerX The center x-position of the triangle
   * @param {Number} centerY The center y-position of the triangle
   */
  generateTriangleVertices(rainbow, size, centerX, centerY, red, green,blue) {
    var z = 0.0;
    //console.log("centerx and y: " + centerX + " " + centerY);
    //console.log("size in triangle: " + size);
    var x1 = (Math.sin(240 * (Math.PI/180)))*size;
    var y1 = (Math.cos(240 * (Math.PI/180)))* size;
    var x2 = (Math.sin(120 * (Math.PI/180)))*size;
    var y2 = (Math.cos(120 * (Math.PI/180)))*size;
    var x3 = (Math.sin(0 * (Math.PI/180)))*size;
    var y3 = (Math.cos(0 * (Math.PI/180)))*size;
    if(!rainbow){
      this.vertices[0] = super.createVertex(x1, y1, z, red, green, blue);
      this.vertices[1] = super.createVertex(x2, y2, z, red, green, blue);
      this.vertices[2] = super.createVertex(x3, y3, z, red, green, blue);
    }
    else{
      this.vertices[0] = super.createVertex(x1, y1, z, 1.0, 0, 0);
      this.vertices[1] = super.createVertex(x2, y2, z, 0, 1.0, 0);
      this.vertices[2] = super.createVertex(x3, y3, z, 0, 0, 1.0);
      console.log("Making rainbow verts");
    }
    //console.log("triangle points: " + this.vertices[0].points + " triangle color: " + this.vertices[0].color);
    
  }
}
