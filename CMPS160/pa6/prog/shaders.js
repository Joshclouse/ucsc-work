var ASSIGN4_VSHADER =
'attribute vec4 a_Position;\n' +
'attribute vec4 a_Color;\n' +
'attribute vec4 a_Normal;\n' +
'uniform mat4 u_ModelMatrix;\n' +
'uniform mat4 u_NormalMatrix;\n' +
'uniform mat4 u_ViewMatrix;\n' +
'uniform mat4 u_ProjMatrix;\n' +
'varying vec4 v_Color;\n' +
'varying vec3 v_Normal;\n' +
'varying vec3 v_Position;\n' +
'void main() {\n' +
'   gl_Position = u_ProjMatrix * u_ViewMatrix * u_ModelMatrix * a_Position;\n'+
'   v_Position = vec3(u_ModelMatrix * a_Position);\n' +
'   v_Normal = normalize(vec3(u_NormalMatrix * a_Normal));\n' +
'   v_Color = a_Color;\n' +
'}\n';

var ASSIGN4_FSHADER =
'precision mediump float;\n'+
'uniform vec3 u_LightColor;\n'+
'uniform vec3 u_LightPosition;\n' +
'uniform vec3 u_AmbientLight;\n' +
'varying vec3 v_Normal;\n' +
'varying vec3 v_Position;\n' +
'varying vec4 v_Color;\n' + 
'void main() {\n' +
'   vec3 normal = normalize(v_Normal);\n' +
'   vec3 lightDirection = normalize(u_LightPosition - v_Position);\n' +
'   vec3 v = normalize(-v_Position.xyz);\n' +
'   vec3 reflection = reflect(-lightDirection, normal);\n' +
'   float nDotL = max(dot(lightDirection, normal), 0.0);\n' +
'   vec3 diffuse = u_LightColor * v_Color.rgb * nDotL;\n' +
'   vec3 ambient = u_AmbientLight * v_Color.rgb;\n' +
'   vec3 spec = vec3(0.0);\n' +
'   spec = u_LightColor * v_Color.rgb * pow(max(dot(reflection, v), 0.0), 1.0);\n' +
'   gl_FragColor = vec4(diffuse + ambient + spec, v_Color.a);\n' +
'}\n';

var ASSIGN4_VSHADER2 = 
'attribute vec4 a_Position;\n' +
'attribute vec4 a_Color;\n' +
'varying vec4 v_Color;\n' +
'uniform mat4 u_ModelMatrix;\n' +
'uniform mat4 u_ViewMatrix;\n' +
'uniform mat4 u_ProjMatrix;\n' +
'void main() {\n' +
' gl_Position = u_ProjMatrix * u_ViewMatrix * u_ModelMatrix * a_Position;\n'+
' v_Color = a_Color;\n' +
'}\n';

var ASSIGN4_FSHADER2 = 
'precision mediump float;\n'+
'varying vec4 v_Color;\n'+
'void main() {\n' +
' gl_FragColor = v_Color;\n'+
'}\n';