/**
 * Function called when the webpage loads.
 */
function main() {
  var canvas = document.getElementById('webgl');
  var clearButton = document.getElementById('Clear');
  var load = document.getElementById('load');
  var loadTexture = document.getElementById('loadTexture');
  var loadMap = document.getElementById('loadMap');
  var gl = getWebGLContext(canvas);
  var POV = document.getElementById('POVSlider');
  var nearSlider = document.getElementById('nearSlider');
  var farSlider = document.getElementById('farSlider');
  var orthoButton = document.getElementById('orthoButton');
  var perspectiveButton = document.getElementById('perspectiveButton');
  var normalShading = document.getElementById('normalShading');
  var shading = document.getElementById('shading');
  var imageSrc;
  if(!gl){
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  let nonTextured = createShader(gl, ASSIGN4_VSHADER, ASSIGN4_FSHADER);
  let textured = createShader(gl, ASSIGN4_VSHADER2, ASSIGN4_FSHADER2);
  var shapes = new Scene(gl, canvas, textured);
  createMap(nonTextured, shapes);
  console.log("shapes.length: " + shapes.geometries.length);
  initEventHandelers(normalShading, shading, loadMap, textured, nonTextured, loadTexture, load, shapes, canvas, gl, clearButton, POV, nearSlider, farSlider, orthoButton,
    perspectiveButton);
  tick(gl, shapes, canvas);
}
