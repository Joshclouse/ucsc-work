/**
 * Specifies a camera in 3D space. Used by scene.
 */
class Camera {
    /**
    * Specifies a camera in 3D space. Used by scene.
    */
    constructor(canvas) {
        console.log("in camera constructor");
        this.position = new Vector3([0, .125, 0]);
        this.center = new Vector3([0, .125, -10]);
        this.up = new Vector3([0, 1, 0]);
        this.ortho = [-.5, .5];
        this.perspective = 50;
        this.near = .01;
        this.far = 5.01;
        this.projectionMatrix = new Matrix4();
        this.viewMatrix = new Matrix4();
        console.log("calling set projection");
        this.setProjection('ortho', canvas);
        console.log("calling updateMatrix");
        this.updateViewMatrix();
    }

    /**
    * Rotates the camera.
    *
    * @param angle The angle of rotation
    * @param x The x-direction of your rotation
    * @param y The x-direction of your rotation
    * @param z The x-direction of your rotation
    */
    rotate(angle, x, y, z) {
      // YOUR CODE HERE
    }

    /**
    * Rotates the camera.
    *
    * @param distance The distance of camera movement
    * @param x The x-direction of your rotation
    * @param y The x-direction of your rotation
    * @param z The x-direction of your rotation
    */
    move(distance, x, y, z) {
      // YOUR CODE HERE
    }

    /**
    * Changes the projection. Can be orthographic or perspective.
    *
    * @param {String} perspectiveType The type of projection
    */
    setProjection(projectionType, canvas) {
      // YOUR CODE HERE
      //console.log("in set projection");
      //console.log("Near: " + this.near);
      //console.log("Far: " + this.far);
      if(projectionType == 'perspective'){
        //console.log("canvas.width: " + canvas.width + " canvas.height: " + canvas.height);
        this.projectionMatrix.setPerspective(this.perspective, canvas.width/canvas.height, this.near, this.far);
        
      }
      else{
        this.projectionMatrix.setOrtho(this.ortho[0], this.ortho[1], -1, 1, this.near, this.far);
      }
      //console.log("Projection matrix: " + this.projectionMatrix.elements);
    }

    /**
    * Updates the view matrix of your camera.
    */
    updateViewMatrix() {
      //console.log("In updatematrix");
      //console.log(this.center.elements);
        this.viewMatrix.setLookAt(
            this.position.elements[0],
            this.position.elements[1],
            this.position.elements[2],
            this.center.elements[0],
            this.center.elements[1],
            this.center.elements[2],
            this.up.elements[0],
            this.up.elements[1],
            this.up.elements[2]);
        //console.log("View matrix: " + this.viewMatrix.elements);
    }
}
