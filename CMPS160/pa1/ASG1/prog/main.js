/**
 * Function called when the webpage loads.
 */
//import eventFuncts from "eventFunctions.js";

function main() {
  //A lot of this code is taken from the example I typed it out tho and didn't just copy paste
  //so that's a good thing I guess
  //Retrieve <canvas> element from html
  
  var canvas = document.getElementById('webgl');
  var clearButton = document.getElementById('Clear');
  var redSlider = document.getElementById('redSlider');
  var blueSlider = document.getElementById('blueSlider');
  var greenSlider = document.getElementById('greenSlider');
  var sizeSlider = document.getElementById('sizeSlider');
  var xcoord = document.getElementById('xcoord');
  var ycoord = document.getElementById('ycoord');
  console.log("boy oh boy");

  //Get rednering context for WebGL
  var gl = getWebGLContext(canvas);
  if(!gl){
    console.log('Failed to get the rednering context for WebGL');
    return;
  }

  //Initialize shaders
  if(!initShaders(gl, ASSIGN1_VSHADER, ASSIGN1_FSHADER)){
    console.log('Failed to initialize shaders.');
    return;
  }

  //Get storage location of a_Position
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  if(a_Position < 0){
    console.log('Failed to get the storage location of a_Position');
    return;
  }

  var a_Size = gl.getAttribLocation(gl.program, 'a_Size');
  if(a_Size < 0){
    console.log('Failed to get the storage location of a_Size');
    return;
  }

  // Get the storage location of u_FragColor
  var u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
  if (!u_FragColor) {
    console.log('Failed to get the storage location of u_FragColor');
    return;
  }

  //Register function (event handler) to be called on a mouse press
  //console.log("right before call to initEventHandelers");
  initEventHandelers(canvas, gl, a_Position, u_FragColor,a_Size, clearButton, redSlider, blueSlider, greenSlider, sizeSlider);

  //Specify the color for clearing the <canvas>
  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  //Clean <canvas>
  gl.clear(gl.COLOR_BUFFER_BIT);
  
  
  
}
