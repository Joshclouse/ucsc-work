
/**
 * Responsible for initializing buttons, sliders, radio buttons, etc. present
 * within your HTML document.
 */
var g_points = []; //The arry for the position of a mouse press
var g_colors = []; //The array for the colors of the squares
var g_size = []; //the arry for the sizes of the squares
var red = 1.0;
var blue = 0.0;
var green = 0.0;
var size = 10.0;
var mousePressed = false;

function initEventHandelers(canvas, gl, a_Position, u_FragColor, a_Size, clearButton, redSlider, blueSlider, greenSlider, sizeSlider) {
  //
  // YOUR CODE HERE
  //
  //console.log("in initEventHandelers");
  //console.log(clearButton);
  canvas.onmousedown = function(ev){mousePressed = true; click(ev, gl, canvas, a_Position, u_FragColor, a_Size);};
  canvas.onmouseup = function(){mousePressed = false;};
  canvas.onmousemove = function(ev){click(ev, gl, canvas, a_Position, u_FragColor, a_Size);};
  clearButton.onclick = function(){reset(gl);};
  redSlider.oninput = function(){changePointColor(redSlider, 1);};
  greenSlider.oninput = function(){changePointColor(greenSlider, 2);};
  blueSlider.oninput = function(){changePointColor(blueSlider, 3);};
  sizeSlider.oninput = function(){changePointSize(sizeSlider)};
  //console.log("initialized clearButton");
}

/**}
 * Function called upon mouse click or mouse drag. Computes position of cursor,
 * pushes cursor position as GLSL coordinates, and draws.
 *
 * @param {Object} ev The event object containing the mouse's canvas position
 */
function click(ev, gl, canvas, a_Position, u_FragColor, a_Size) {
  if(mousePressed == false)
    return;
  var x = ev.clientX; //x coordinate of a mouse pointer
  var y = ev.clientY; //y coordinate of a mouse pointer
  var rect = ev.target.getBoundingClientRect();
  x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
  y = (canvas.height/2 - (y-rect.top))/ (canvas.height/2);
  g_points.push([x,y]);
  sendTextToHTML(x, xcoord);
  sendTextToHTML(y, ycoord);
  console.log("pushing red green blue: "+ red + " " + green + " " + blue);
  g_colors.push([red, green, blue, 1.0]);
  g_size.push([size]);

  //clear <canvas>
  clearCanvas(gl);
  render(a_Position, gl, u_FragColor, a_Size);
  
}

/**
 * Renders the scene on the HTML canvas.
 */
function render(a_Position, gl, u_FragColor, a_Size) {
  //console.log("rendering");
  var len = g_points.length;
  for(var k = 0; k < len; k++){
    //console.log("looping: "+k);
    var rgba = g_colors[k];
    var xy = g_points[k];
    //Pass the position of a point to a_Position variable
    gl.vertexAttrib3f(a_Position, xy[0], xy[1], 0.0);
    gl.vertexAttrib1f(a_Size, g_size[k]);+
    sendUniformVec4ToGLSL(gl, rgba, u_FragColor);

    //Draw
    gl.drawArrays(gl.POINTS, 0, 1);
  }
}

/**
 * Clears the HTML canvas.
 */
function clearCanvas(gl) {
  gl.clear(gl.COLOR_BUFFER_BIT);
}

/**
 * Changes the size of the points drawn on HTML canvas.
 *
 * @param {float} size Real value representing the size of the point.
 */
function changePointSize(slider) {
  size = slider.value * 1.0;
}

/**
 * Changes the color of the points drawn on HTML canvas.
 *
 * @param {float} color Color value from 0.0 to 1.0.
 */
function changePointColor(slider, color) {
  //if the color is red
  if(color == 1){
    red = (.01 * slider.value);
  }
  else if(color == 2){ //color is green
    green = (.01 * slider.value);
  } 
  else if(color == 3){ //color is blue
    blue = (.01 * slider.value);
  }
  //console.log("red = " +red);
  //console.log("blue = "+blue);
  //console.log("green = "+green);
}

function reset(gl){
  //console.log("g_points before deletion " + g_points);
  g_points = [];
  g_colors = [];
  g_size = [];
  //console.log("g_points after deletion " + g_points);
  clearCanvas(gl);
}