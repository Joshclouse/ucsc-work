var ASSIGN4_VSHADER =
'attribute vec4 a_Position;\n' +
'attribute vec4 a_Color;\n' +
'varying vec4 v_Color;\n' +
'uniform mat4 u_ModelMatrix;\n' +
'uniform mat4 u_MvpMatrix;\n' +
'void main() {\n' +
' gl_Position = u_ModelMatrix * a_Position;\n'+
' v_Color = a_Color;\n' +
'}\n';

var ASSIGN4_FSHADER =
'precision mediump float;\n'+
'varying vec4 v_Color;\n'+
'void main() {\n' +
' gl_FragColor = v_Color;\n'+
'}\n';

var ASSIGN4_VSHADER2 = 
'attribute vec4 a_Position;\n' +
'attribute vec2 a_TexCoord;\n' +
'varying vec2 v_TexCoord;\n' +
'uniform mat4 u_ModelMatrix;\n' +
'void main() {\n' +
' gl_Position = u_ModelMatrix * a_Position;\n' +
' v_TexCoord = a_TexCoord;\n' +
'}\n';

var ASSIGN4_FSHADER2 = 
'precision mediump float;\n' +
'uniform sampler2D u_Sampler;\n' +
'varying vec2 v_TexCoord;\n' +
'void main() {\n' +
' gl_FragColor = texture2D(u_Sampler, v_TexCoord);\n' +
'}\n';
