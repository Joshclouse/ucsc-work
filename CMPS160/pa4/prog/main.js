/**
 * Function called when the webpage loads.
 */
function main() {
  var canvas = document.getElementById('webgl');
  var clearButton = document.getElementById('Clear');
  var redSlider = document.getElementById('redSlider');
  var blueSlider = document.getElementById('blueSlider');
  var greenSlider = document.getElementById('greenSlider');
  var sizeSlider = document.getElementById('sizeSlider');
  var segmentSlider = document.getElementById('segmentCount');
  var triangle = document.getElementById('triangle');
  var square = document.getElementById('square');
  var circle = document.getElementById('circle');
  var cube = document.getElementById('cube');
  var load = document.getElementById('load');
  var loadTexture = document.getElementById('loadTexture');
  var solid = document.getElementById('solid');
  var rainbow = document.getElementById('rainbow');
  var gl = getWebGLContext(canvas);
  var imageSrc;
  if(!gl){
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  let nonTextured = createShader(gl, ASSIGN4_VSHADER, ASSIGN4_FSHADER);
  let textured = createShader(gl, ASSIGN4_VSHADER2, ASSIGN4_FSHADER2);
  var texture = gl.createTexture();
  var shapes = new Scene(gl);
  console.log("shapes.length: " + shapes.geometries.length);
  initEventHandelers(texture, imageSrc, textured, nonTextured, solid, rainbow, loadTexture, load, shapes, canvas, gl, clearButton, redSlider, blueSlider, greenSlider, sizeSlider, segmentSlider, cube, triangle, square, circle);
  tick(gl, shapes, canvas);
}
