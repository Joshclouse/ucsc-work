/**
 * Specifies a geometric object.
 *
 * @author "Clouse"
 * @this {Geometry}
 */
class Geometry {
  /**
   * Constructor for Geometry.
   *
   * @constructor
   */
  constructor() {
    //console.log("in constructor for goemetry");
    this.vertices = []; // Vertex objects. Each vertex has x-y-z.
    this.modelMatrix = new Matrix4();
    this.type = null;
    this.shader = null;
  }

  /**
   * Renders this Geometry within your webGL scene.
   */
  render(gl) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: sendUniformVec4ToGLSL(), tellGLSLToDrawCurrentBuffer(),
    // and sendAttributeBufferToGLSL() are going to be useful here.
    //console.log("number of vertices: " + this.vertices.length);
    //console.log("checking to see if there is anything in this.vertices: " + this.vertices[0].points[0] + " " + this.vertices[0].points[1]);
    var data = new Float32Array(this.vertices.length *6);
    var i = 0;
    //console.log("data.length: " + data.length);
    if(this.type == null){
      for(var j = 0; j < this.vertices.length; j++){
        //point will be each vertex in the vertex array associated with each geometric object
        //console.log("j = " + j);
        data[i] = this.vertices[j].points[0];
        //console.log("x: " + data[i]);
        data[i+1] = this.vertices[j].points[1];
        //console.log("y: " + data[i+1]);
        data[i+2] = this.vertices[j].points[2];
        data[i+3] = this.vertices[j].color[0];
        data[i+4] = this.vertices[j].color[1];
        data[i+5] = this.vertices[j].color[2];
        //console.log("Color = " + this.vertices[j].color);
        i+=6;
      }
    }
    else{
      for(var j = 0; j < this.vertices.length; j++){
        data[i] = this.vertices[j].points.elements[0];
        data[i+1] = this.vertices[j].points.elements[1];
        data[i+2] = this.vertices[j].points.elements[2];
        data[i+3] = this.color[0];
        data[i+4] = this.color[1];
        data[i+5] = this.color[2];
        i+=6;
      }
    }
    //console.log("data: " + data);
    useShader(gl, this.shader);
    sendAttributeBufferToGLSL(gl, data, 6, "a_Position", 0, 3);
    sendAttributeBufferToGLSL(gl, data, 6, "a_Color", 3, 3)
    sendUniformMatToGLSL(gl, this.modelMatrix, "u_ModelMatrix");
    tellGLSLToDrawCurrentBuffer(gl, this.vertices.length);
    
  }
  createVertex(x, y, z, red, green, blue){
      var vert = new Vertex();
      //console.log("in create vertex: " + x + " "+ y);
      vert.points[0] = x;
      vert.points[1] = y;
      vert.points[2] = z;
      vert.color = [red,green,blue];
      return vert;
  }
}
