/**
 * Specifies a WebGL scene.
 *
 * @author "Josh Clouse"
 * @this {Scene}
 */
class Scene {
  /**
   * Constructor for Scene.
   *
   * @constructor
   */
  constructor(gl) {
    //console.log("In constructor for shapes");
    this.geometries = []; // Geometries being drawn on canvas
    //
    // YOUR CODE HERE
    //

    // Recommendations: Setting the canvas's clear color and clearing the canvas
    // here is a good idea.
    gl.clearColor(0,0,0,1);
    gl.clear(gl.COLOR_BUFFER_BIT);
  }

  /**
   * Adds the given geometry to the the scene.
   *
   * @param {Geometry} geometry Geometry being added to scene
   */
  addGeometry(geometry) {
    this.geometries.push(geometry);
  }

  /**
   * Clears all the geometry within the scene.
   */
  clearGeometry(gl) {
    gl.clear(gl.COLOR_BUFFER_BIT);
    this.geometries = [];
    this.render(gl);
  }

  /**
   * Updates the animation for each geometry in geometries.
   */
  updateAnimation() {
    //
    // YOUR CODE HERE
    //

    // Recomendations: No rendering should be done here. Your Geometry objects
    // in this.geometries should update their animations themselves through
    // their own .updateAnimation() methods.
    for(var i = 0; i < this.geometries.length; i++){
      this.geometries[i].updateAnimation();
    }
  }

  /**
   * Renders all the Geometry within the scene.
   */
  render(gl) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: No calls to any of your GLSL functions should be made
    // here. Your Geometry objects in this.geometries should render themselves
    // through their own .render() methods.
    gl.clear(gl.COLOR_BUFFER_BIT);
    //console.log("this.geometries.length: " + this.geometries.length);
    this.updateAnimation();
    for(var i = 0; i < this.geometries.length; i++){
      this.geometries[i].render(gl);
    }
  }
}
