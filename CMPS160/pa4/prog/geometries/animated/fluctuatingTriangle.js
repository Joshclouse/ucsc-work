/**
 * Specifies a triangle which fluctuates in size (grows and shrinks).
 *
 * @author "Josh Clouse"
 * @this {FluctuatingTriangle}
 */
class FluctuatingTriangle extends Triangle {
  /**
   * Constructor for FluctuatingTriangle.
   *
   * @constructor
   * @param {Number} size The size of the triangle drawn
   * @param {Number} centerX The center x-position of the triangle
   * @param {Number} centerY The center y-position of the triangle
   */
  constructor(shader, rainbow, size, centerX, centerY, red, green, blue) {
    super(shader, rainbow, size, centerX, centerY, red, green, blue);
    this.currentSize = 1.0;
    this.sizeStep = .5;
    this.g_last = Date.now();
    this.increasing = true;
    this.offsetX = centerX;
    this.offsetY = centerY;
    console.log("this.offsetx, y: " + this.offsetX + ", " + this.offsetY);
  }

  /**
   * Updates the animation for FluctuatingTriangle. Grows and shrinks the
   * triangle in size.
   */
  updateAnimation() {
    //
    // YOUR CODE HERE
    //
    var now = Date.now();
    var elapsed = now-this.g_last;
    this.g_last = now;
    var difference = (this.sizeStep * elapsed)/1000.0
    var iterations = 0;
    while(difference > 0){
      if(this.increasing && this.currentSize + difference > 1.50){
        difference =  difference - (1.50- this.currentSize);
        this.increasing = false;
      }
      else if(!this.increasing && this.currentSize - difference < .5){
        difference = difference - (this.currentsize - .5)
        this.increasing = true;
      }
      else if(iterations == 0 && this.increasing && !(this.currentSize + difference > 1.50)){
        this.currentSize += difference;
        difference = -1;
      }
      else if(iterations == 0 && !this.increasing && !(this.currentSize - difference < .5)){
        this.currentSize -= difference;
        difference = -1;
      }
      else if(iterations > 0 && this.increasing){
        this.currentSize = .5 + difference;
        difference = -1;
      }
      else{
        this.currentSize = 1.5 - difference;
        difference = -1;
      }
      iterations++;
    }
    //console.log("currentSize = " + this.currentSize);
    this.modelMatrix.setTranslate(this.offsetX, this.offsetY, 0);
    this.modelMatrix.scale(this.currentSize, this.currentSize, 1.0);
    //this.modelMatrix.setIdentity();
    // Recomendations: How much the triangle grows an shrinks is up to you.
    // Might want to shrink it to x.50 at it's smallest point and x1.50 at it's
    // largest point.
    //
    // Keep in mind that no rendering should be done here. updateAnimation()'s
    // purpose is to update the geometry's modelMatrix and any other variables
    // related to animation. It should be the case that after I call
    // updateAnimation() I should be able to call render() elsewhere and have my
    // geometry complete a frame of animation.
  }
}
