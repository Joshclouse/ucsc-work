/**
 * Specifies a square which spins realtive to its center.
 *
 * @author Joshua Clouse
 * @this {SpinningSquare}
 */
class SpinningSquare extends Square {
  /**
   * Constructor for SpinningSquare.
   *
   * @constructor
   * @param {Number} size The size of the square drawn
   * @param {Number} centerX The center x-position of the square
   * @param {Number} centerY The center y-position of the square
   * @returns {SpinningSquare} SpinningSquare object created
   */
  constructor(shader, rainbow, size, centerX, centerY, red, green, blue) {
    //
    // YOUR CODE HERE
    //

    // Recomendations: You're going to need a few variables to keep track of
    // information relevant to your animation. For example, a square is going
    // to need a variable to keep track of its centerX and centerY position.
    console.log("creating spinning square");
    super(shader, rainbow, size, centerX, centerY, red, green, blue);
    this.currentAngle = 0.0;
    this.ANGLE_STEP = 45.0; 
    this.g_last = Date.now();
    this.offsetX = centerX;
    this.offsetY = centerY;
    //console.log("offset x and y: " + this.offsetX + " " + this.offsetY);
    //this should set all the points of the square relative to the origin in order to do a correct rotation around the center
    //later I will translate this back to the offset when in updateAnimation()
    // for(var i = 0; i < this.vertices.length; i++){
    //   this.vertices[i].points[0] -= this.offsetX;
    //   this.vertices[i].points[1] -= this.offsetY;
    //   console.log("x, y: " + this.vertices[i].points[0] + ", " +  this.vertices[i].points[1]);
    // }
  }

  /**
   * Updates the animation for spinning square. Rotates the square by spinAngle
   * relative to its center.
   */
  updateAnimation() {
    //
    // YOUR CODE HERE
    //

    // Recomendations: Do not simply apply a rotation matrix. Doing so will
    // cause your square to spin in a circle on screen.
    //
    // Keep in mind that no rendering should be done here. updateAnimation()'s
    // purpose is to update the geometry's modelMatrix and any other variables
    // related to animation. It should be the case that after I call
    // updateAnimation() I should be able to call render() elsewhere and have my
    // geometry complete a frame of animation.
    var now = Date.now();
    var elapsed = now - this.g_last;
    this.g_last = now;
    this.currentAngle = this.currentAngle + (this.ANGLE_STEP * elapsed) / 1000.0;
    this.currentAngle %= 360;
    //Right now I'm centered around the origin and I want to rotate around the origin and then translate back to the correct offset
    //but I think I'm supposed to do it in reverse order.
    //this.modelMatrix.setIdentity();
    this.modelMatrix.setTranslate(this.offsetX, this.offsetY, 0);
    this.modelMatrix.rotate(this.currentAngle, 0, 0, 1);
  }
}
