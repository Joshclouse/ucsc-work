/**
 * A cube with a single textured applied in multiple different ways. A subclass
 * of TiltedCube.
 *
 * @author Joshua Clouse
 * @this {MultiTextureCube}
 */
class MultiTextureCube extends TiltedCube {
  /**
   * Constructor for MultiTextureCube
   *
   * @constructor
   * @param {String} texturePath The filepath/URL of the image used as a texture
   */
  constructor(gl, texturePath, shader, size, centerX, centerY) {
    //
    // YOUR CODE HERE
    //
    super(shader, size, centerX, centerY, 1,1,1);
    this.texturePath = texturePath;
    this.text;
    let self = this;
    this.generateUVCoordinates();
    //console.log("Creating 2d Tex");
    this.text = create2DTexture(gl, this.texturePath, "gl.LINEAR", "gl.LINEAR", "gl.REPEAT", "gl.REPEAT", function(texture){
      //console.log(texture);
      //self.text = texture;
      //gl.bindTexture(gl.TEXTURE_2D, text);
    });
    //console.log(this.modelMatrix);
    //console.log("in const for textcube");
    //console.log("texturepath: " + texturePath);
    // Recomendations: Might want to call generateUVCoordinates here.
    
  }

  /**
   * Generates the texture coordinates of CheckerCube.
   *
   * @private
   */
  generateUVCoordinates() {
    //
    // YOUR CODE HERE
    //
    //just straight going to hardcode this shit
    //uv coordinates for the front of the cube
    this.vertices[0].uv = [1,1];
    this.vertices[1].uv = [1,0];
    this.vertices[2].uv = [0,0];
    this.vertices[3].uv = [0,1];
    this.vertices[4].uv = [1,1];
    this.vertices[5].uv = [0,0];
    //uv coords for the back
    this.vertices[6].uv = [1,1];
    this.vertices[7].uv = [1,0];
    this.vertices[8].uv = [0,0];
    this.vertices[9].uv = [0,1];
    this.vertices[10].uv = [1,1];
    this.vertices[11].uv = [0,0];
    //uv coords for the top right
    this.vertices[12].uv = [0,1];
    this.vertices[13].uv = [1,1];
    this.vertices[14].uv = [0,0];
    this.vertices[15].uv = [1,0];
    this.vertices[16].uv = [1,1];
    this.vertices[17].uv = [0,0];
    //uv coords for the bottom right
    this.vertices[18].uv = [0,1];
    this.vertices[19].uv = [1,1];
    this.vertices[20].uv = [0,0];
    this.vertices[21].uv = [1,0];
    this.vertices[22].uv = [1,1];
    this.vertices[23].uv = [0,0];
    //uv coords for bottom left
    this.vertices[24].uv = [0,1];
    this.vertices[25].uv = [1,1];
    this.vertices[26].uv = [0,0];
    this.vertices[27].uv = [1,0];
    this.vertices[28].uv = [1,1];
    this.vertices[29].uv = [0,0];
    //uv coords for top left
    this.vertices[30].uv = [0,1];
    this.vertices[31].uv = [1,1];
    this.vertices[32].uv = [0,0];
    this.vertices[33].uv = [1,0];
    this.vertices[34].uv = [1,1];
    this.vertices[35].uv = [0,0];
    //console.log(this.vertices);
    // Recomendations: Remember uv coordinates are defined from 0.0 to 1.0.
  }

  /**
   * Renders MultiTextureCube.
   */
  render(gl) {
    //
    // YOUR NAME HERE
    //
    //this should get the texture warmed up I hope
    useShader(gl, this.shader);
    
    var data = new Float32Array(this.vertices.length *5);
    var i = 0;
    //console.log("data.length: " + data.length);
    for(var j = 0; j < this.vertices.length; j++){
      //point will be each vertex in the vertex array associated with each geometric object
      //console.log("j = " + j);
      data[i] = this.vertices[j].points[0];
      //console.log("x: " + data[i]);
      data[i+1] = this.vertices[j].points[1];
      //console.log("y: " + data[i+1]);
      data[i+2] = this.vertices[j].points[2];
      data[i+3] = this.vertices[j].uv[0];
      data[i+4] = this.vertices[j].uv[1];
      //console.log("Color = " + this.vertices[j].color);
      i+=5;
    }
    
    //console.log(data);
    
    //console.log("after sending");
    sendAttributeBufferToGLSL(gl, data, 5, "a_Position", 0, 3);
    sendAttributeBufferToGLSL(gl, data, 5, "a_TexCoord", 3, 2);
    //console.log(this.text);
    send2DTextureToGLSL(gl, this.text, 0, 'u_Sampler');
    //console.log(this.modelMatrix);
    sendUniformMatToGLSL(gl, this.modelMatrix, "u_ModelMatrix");
    //console.log("drawing triangles");
    tellGLSLToDrawCurrentBuffer(gl, this.vertices.length);
    //console.log("trying to render this");
    
    // Recomendations: This will be the first time render will need to be
    // overloaded. Why? Because this is a textured geometry, not a geometry
    // which relies on a color value. Might want to use
  }
  updateAnimation(){
    super.updateAnimation();
    //console.log(this.modelMatrix);
  }

}
