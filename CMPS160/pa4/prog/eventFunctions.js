
var red = 1.0;
var blue = 0.0;
var green = 0.0;
var size = .1;
var mousePressed = false;
var segments = 3;
var shape = "triangle";
var isRainbow = false;
var textureLoaded = false;
var boyText;
function initEventHandelers(texture, imageSrc, textured, nonTextured, solid,rainbow, loadTexture, load, shapes, canvas, gl, clearButton, redSlider, blueSlider, greenSlider, sizeSlider, segmentSlider, cube, triangle, square, circle) {
  canvas.onmousedown = function(ev){mousePressed = true; click(boyText, textured, nonTextured, ev, gl, canvas, shapes);};
  canvas.onmouseup = function(){mousePressed = false;};
  canvas.onmousemove = function(ev){click(boyText, textured, nonTextured, ev, gl, canvas, shapes);};
  clearButton.onclick = function(){shapes.clearGeometry(gl);};
  redSlider.oninput = function(){changePointColor(redSlider, 1);};
  greenSlider.oninput = function(){changePointColor(greenSlider, 2);};
  blueSlider.oninput = function(){changePointColor(blueSlider, 3);};
  sizeSlider.oninput = function(){changePointSize(sizeSlider);};
  segmentSlider.oninput = function(){changeSegmentCount(segmentSlider);};
  triangle.onclick = function(){shape = "triangle"};
  square.onclick = function(){shape = "square"};
  circle.onclick = function(){shape = "circle"};
  cube.onclick = function(){shape = "cube"};
  load.onclick = function(){loadFileAsText(shapes, nonTextured)};
  loadTexture.onclick = function(){textureToImage(imageSrc, texture); textureLoaded = true;};
  solid.onclick = function(){isRainbow = false;};
  rainbow.onclick = function(){isRainbow = true; console.log("rainbow activated")};
}

/**
 * Function called upon mouse click or mouse drag. Computes position of cursor,
 * pushes cursor position as GLSL coordinates, and draws.
 *
 * @param {Object} ev The event object containing the mouse's canvas position
 */
function click(imageSrc, textured, nonTextured, ev, gl, canvas, shapes) {
  if(mousePressed == false)
    return;
    var x = ev.clientX;
    var y = ev.clientY;
    var rect = ev.target.getBoundingClientRect();
    x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
    y = (canvas.height/2 - (y-rect.top))/ (canvas.height/2);
    if(shape == "triangle"){
      var t = new FluctuatingTriangle(nonTextured, isRainbow, size, x, y, red, green, blue);
      shapes.geometries.push(t);
    }
    if(shape == "square"){
      var s = new SpinningSquare(nonTextured, isRainbow, size, x, y, red , green, blue);
      shapes.geometries.push(s);
    }
    if(shape == "circle"){
      var c = new RandomCircle(nonTextured, isRainbow, size, segments, x, y, red, green, blue);
      shapes.geometries.push(c);
    }
    if(shape == "cube"){
      if(!textureLoaded){
        var q = new TiltedCube(nonTextured, size, x, y, red, green, blue);
        shapes.geometries.push(q);
      }
      else{
        var qt = new MultiTextureCube(gl, imageSrc, textured, size, x,y);
        shapes.geometries.push(qt);
      }
    }
    clearCanvas(gl);
    shapes.render(gl, shape);
}

/**
 * Clears the HTML canvas.
 */
function clearCanvas(gl) {
  gl.clear(gl.COLOR_BUFFER_BIT);
}

/**
 * Changes the size of the points drawn on HTML canvas.
 *
 * @param {float} size Real value representing the size of the point.
 */
function changePointSize(slider) {
  size = slider.value/100.0;
}

/**
 * Changes the color of the points drawn on HTML canvas.
 *
 * @param {float} color Color value from 0.0 to 1.0.
 */
function changePointColor(slider,color) {
  //if the color is red
  if(color == 1){
    red = (.01 * slider.value);
  }
  else if(color == 2){ //color is green
    green = (.01 * slider.value);
  } 
  else if(color == 3){ //color is blue
    blue = (.01 * slider.value);
  }
}
function changeSegmentCount(slider){
  segments = slider.value;
}
function loadFileAsText(shapes, shade){
  var objFile = document.getElementById('objFile').files[0];
  var fileReader = new FileReader();
  fileReader.onload = function(fileLoadedEvent){
    console.log("in onload");
    var textFromFileLoaded = fileLoadedEvent.target.result;
    //console.log(textFromFileLoaded);
    var loadObj = new LoadedOBJ(textFromFileLoaded, shade);

    loadObj.color = [red, green, blue, 1.0];
    console.log(loadObj.vertices.length);
    //console.log("vert 1: " + loadObj.vertices[0].points[0] + ", " + loadObj.vertices[0].points[1] + ", " + loadObj.vertices[0].points[2]);
    shapes.geometries.push(loadObj);
    //shapes.render();
  };
  fileReader.readAsText(objFile, "UTF-8");
}

function textureToImage(boy, texture){
  // let imageReader = new FileReader();
  //  imageReader.addEventListener("load", function(){
  //    imageSrc = imageReader.result;
  //    console.log(imageSrc);
  //  }, false);
  // imageReader.readAsDataURL(document.getElementById('textureFile').files[0]);
  boy = 'checkerboard.png';
  boyText = boy;
  console.log(boy);
}