/**
 * Responsible for animating the Scene.
 */
function tick(gl, shapes, u_FragColor, canvas, shape) {
  //
  // YOUR CODE HERE
  //
  //console.log("in tick");

  // Recomendations: You're going to want to call this at the end of your main()
  // in main.js. requestAnimationFrame() needs to be used here (read the book).
  //console.log("vapes length in tick: " + shapes.geometries.length);
  var tock = function(){
    //console.log("calling tock");
    shapes.render(gl, u_FragColor);
    requestAnimationFrame(tock, canvas);
  };
  tock();
}
