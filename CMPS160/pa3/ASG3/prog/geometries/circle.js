/**
 * Specifies a Circle. A subclass of Geometry.
 *
 * @author "Josh Clouse"
 * @this {Circle}
 */
class Circle extends Geometry {
  /**
   * Constructor for Circle.
   *
   * @constructor
   * @param {Number} radius The radius of the circle being constructed
   * @param {Integer} segments The number of segments composing the circle
   * @param {Number} centerX The central x-position of the circle
   * @param {Number} centerY The central y-position of the circle
   */
  constructor(radius, segments, centerX, centerY) {
    super();
    this.generateCircleVertices(radius, segments, centerX, centerY);
  }

  /**
   * Generates the vertices of the Circle.
   *
   * @private
   * @param {Number} radius The radius of the circle being constructed
   * @param {Integer} segments The number of segments composing the circle
   * @param {Number} centerX The central x-position of the circle
   * @param {Number} centerY The central y-position of the circle
   */
  generateCircleVertices(radius, segments, centerX, centerY) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: Might want to call this within your Circle constructor.
    // Keeps your code clean :)
    var z =0.0;
    var j = 0;
    for(var i = 360/segments; i <=360 + (360/segments); i+=(360/segments)){
      this.vertices[j] = super.createVertex(centerX, centerY, z);
      j++;
      //console.log("prev i: " + (i - (360/segments)));
      var x = ((Math.sin((i - (360/segments)) * (Math.PI/180))) * radius) + centerX;
      var y = ((Math.cos((i - (360/segments)) * (Math.PI/180)))* radius) + centerY;
      this.vertices[j] = super.createVertex(x,y,z);
      j++;
      x = (Math.sin(i * (Math.PI/180)) * radius) + centerX;
      y = (Math.cos(i * (Math.PI/180))* radius) + centerY;
      this.vertices[j] = super.createVertex(x, y, z);
      //console.log("current i: " + i);
      j++;
    }
  }
}
