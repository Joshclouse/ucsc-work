/**
 * Function called when the webpage loads.
 */
function main() {
  var canvas = document.getElementById('webgl');
  var clearButton = document.getElementById('Clear');
  var redSlider = document.getElementById('redSlider');
  var blueSlider = document.getElementById('blueSlider');
  var greenSlider = document.getElementById('greenSlider');
  var sizeSlider = document.getElementById('sizeSlider');
  var segmentSlider = document.getElementById('segmentCount');
  var triangle = document.getElementById('triangle');
  var square = document.getElementById('square');
  var circle = document.getElementById('circle');
  var cube = document.getElementById('cube');
  var load = document.getElementById('load');

  var gl = getWebGLContext(canvas);
  if(!gl){
    console.log('Failed to get the rendering context for WebGL');
    return;
  }

  if(!initShaders(gl, ASSIGN3_VSHADER, ASSIGN3_FSHADER)){
    console.log('Failed to initialize shaders.');
    return;
  }

  var u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');
  if(!u_FragColor){
    console.log("Failed to get the storage location of u_FragColor");
    return;
  }
  var shapes = new Scene(gl);
  console.log("shapes.length: " + shapes.geometries.length);
  initEventHandelers(load, shapes, canvas, gl, u_FragColor, clearButton, redSlider, blueSlider, greenSlider, sizeSlider, segmentSlider, cube, triangle, square, circle);
  tick(gl, shapes, u_FragColor, canvas);
}
