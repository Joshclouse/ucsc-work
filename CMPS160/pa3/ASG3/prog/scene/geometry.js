/**
 * Specifies a geometric object.
 *
 * @author "Clouse"
 * @this {Geometry}
 */
class Geometry {
  /**
   * Constructor for Geometry.
   *
   * @constructor
   */
  constructor() {
    //console.log("in constructor for goemetry");
    this.vertices = []; // Vertex objects. Each vertex has x-y-z.
    this.color = [];  // The color of your geometric object
    this.modelMatrix = new Matrix4();
    this.type = null;
  }

  /**
   * Renders this Geometry within your webGL scene.
   */
  render(gl, u_FragColor) {
    //
    // YOUR CODE HERE
    //

    // Recommendations: sendUniformVec4ToGLSL(), tellGLSLToDrawCurrentBuffer(),
    // and sendAttributeBufferToGLSL() are going to be useful here.
    //console.log("number of vertices: " + this.vertices.length);
    //console.log("checking to see if there is anything in this.vertices: " + this.vertices[0].points[0] + " " + this.vertices[0].points[1]);
    var data = new Float32Array(this.vertices.length *3);
    var i = 0;
    //console.log("data.length: " + data.length);
    if(this.type == null){
      for(var j = 0; j < this.vertices.length; j++){
        //point will be each vertex in the vertex array associated with each geometric object
        //console.log("j = " + j);
        data[i] = this.vertices[j].points[0];
        //console.log("x: " + data[i]);
        data[i+1] = this.vertices[j].points[1];
        //console.log("y: " + data[i+1]);
        data[i+2] = this.vertices[j].points[2];
        i+=3;
      }
    }
    else{
      for(var j = 0; j < this.vertices.length; j++){
        data[i] = this.vertices[j].points.elements[0];
        data[i+1] = this.vertices[j].points.elements[1];
        data[i+2] = this.vertices[j].points.elements[2];
        i+=3;
      }
    }
    sendAttributeBufferToGLSL(gl, data, this.vertices.length, "a_Position");
    sendUniformMatToGLSL(gl, this.modelMatrix, "u_ModelMatrix");
    sendUniformVec4ToGLSL(gl, this.color, u_FragColor);
    tellGLSLToDrawCurrentBuffer(gl, this.vertices.length);
    
  }
  createVertex(x, y, z){
      var vert = new Vertex();
      //console.log("in create vertex: " + x + " "+ y);
      vert.points[0] = x;
      vert.points[1] = y;
      vert.points[2] = z;
      return vert;
  }
}
