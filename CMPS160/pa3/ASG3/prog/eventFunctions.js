
/**
 * Responsible for initializing buttons, sliders, radio buttons, etc. present
 * within your HTML document.
 */
var red = 1.0;
var blue = 0.0;
var green = 0.0;
var size = .1;
var mousePressed = false;
var segments = 3;
var shape = "triangle";
function initEventHandelers(load, shapes, canvas, gl, u_FragColor, clearButton, redSlider, blueSlider, greenSlider, sizeSlider, segmentSlider, cube, triangle, square, circle) {
  canvas.onmousedown = function(ev){mousePressed = true; click(ev, gl, canvas, u_FragColor, shapes);};
  canvas.onmouseup = function(){mousePressed = false;};
  canvas.onmousemove = function(ev){click(ev, gl, canvas, u_FragColor, shapes);};
  clearButton.onclick = function(){shapes.clearGeometry(gl);};
  redSlider.oninput = function(){changePointColor(redSlider, 1);};
  greenSlider.oninput = function(){changePointColor(greenSlider, 2);};
  blueSlider.oninput = function(){changePointColor(blueSlider, 3);};
  sizeSlider.oninput = function(){changePointSize(sizeSlider);};
  segmentSlider.oninput = function(){changeSegmentCount(segmentSlider);};
  triangle.onclick = function(){shape = "triangle"};
  square.onclick = function(){shape = "square"};
  circle.onclick = function(){shape = "circle"};
  cube.onclick = function(){shape = "cube"};
  load.onclick = function(){loadFileAsText(shapes)};
}

/**
 * Function called upon mouse click or mouse drag. Computes position of cursor,
 * pushes cursor position as GLSL coordinates, and draws.
 *
 * @param {Object} ev The event object containing the mouse's canvas position
 */
function click(ev, gl, canvas, u_FragColor, shapes) {
  if(mousePressed == false)
    return;
    var x = ev.clientX;
    var y = ev.clientY;
    var rect = ev.target.getBoundingClientRect();
    x = ((x - rect.left) - canvas.width/2)/(canvas.width/2);
    y = (canvas.height/2 - (y-rect.top))/ (canvas.height/2);
    if(shape == "triangle"){
      var t = new FluctuatingTriangle(size, x, y);
      t.color = [red, green, blue, 1.0];
      shapes.geometries.push(t);
    }
    if(shape == "square"){
      var s = new SpinningSquare(size, x, y);
      s.color = [red,green,blue, 1.0];
      shapes.geometries.push(s);
    }
    if(shape == "circle"){
      var c = new RandomCircle(size, segments, x, y);
      c.color = [red,green,blue, 1.0];
      shapes.geometries.push(c);
    }
    if(shape == "cube"){
      var q = new TiltedCube(size, x, y);
      q.color = [red,green,blue, 1.0];
      shapes.geometries.push(q);
    }
    clearCanvas(gl);
    shapes.render(gl, u_FragColor, shape);
}

/**
 * Clears the HTML canvas.
 */
function clearCanvas(gl) {
  gl.clear(gl.COLOR_BUFFER_BIT);
}

/**
 * Changes the size of the points drawn on HTML canvas.
 *
 * @param {float} size Real value representing the size of the point.
 */
function changePointSize(slider) {
  size = slider.value/100.0;
}

/**
 * Changes the color of the points drawn on HTML canvas.
 *
 * @param {float} color Color value from 0.0 to 1.0.
 */
function changePointColor(slider,color) {
  //if the color is red
  if(color == 1){
    red = (.01 * slider.value);
  }
  else if(color == 2){ //color is green
    green = (.01 * slider.value);
  } 
  else if(color == 3){ //color is blue
    blue = (.01 * slider.value);
  }
}
function changeSegmentCount(slider){
  segments = slider.value;
}
function loadFileAsText(shapes){
  var objFile = document.getElementById('objFile').files[0];
  loadFile(objFile.name, (file_string) =>{
    var loadObj = new LoadedOBJ(file_string);
    loadObj.color = [red, green, blue, 1.0];
    shapes.geometries.push(loadObj);
    //console.log(loadObj);
    //console.log("vert 1: " + loadObj.vertices[0].points[0] + ", " + loadObj.vertices[0].points[1] + ", " + loadObj.vertices[0].points[2]);
  });
  //var fileReader = new FileReader();
  // fileReader.onload = function(fileLoadedEvent){
  //   console.log("in onload");
  //   var textFromFileLoaded = fileLoadedEvent.target.result;
  //   //console.log(textFromFileLoaded);
  //   var loadObj = new LoadedOBJ(textFromFileLoaded);

  //   loadObj.color = [red, green, blue, 1.0];
  //   console.log(loadObj.vertices.length);
  //   console.log("vert 1: " + loadObj.vertices[0].points[0] + ", " + loadObj.vertices[0].points[1] + ", " + loadObj.vertices[0].points[2]);
  //   shapes.geometries.push(loadObj);
  //   //shapes.render();
  // };
  //fileReader.readAsText(objFile, "UTF-8");
}