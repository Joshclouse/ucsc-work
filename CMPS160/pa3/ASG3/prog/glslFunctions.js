/**
 * Sends data to a uniform variable expecting a matrix value.
 *
 * @private
 * @param {Array} val Value being sent to uniform variable
 * @param {String} uniformName Name of the uniform variable recieving data
 */

 //should take in the gl, the modelMatrix(val) and the uniformName of the GL variable
 function sendUniformMatToGLSL(gl, val, uniformName) {
  var u_ModelMatrix = gl.getUniformLocation(gl.program, uniformName);
  if(!u_ModelMatrix){
    console.log('Failed to get the storage location of ' + u_ModelMatrix);
    return;
  }
  gl.uniformMatrix4fv(u_ModelMatrix, false, val.elements);
}

/**
 * Sends data to an attribute variable using a buffer.
 *
 * @private
 * @param {Float32Array} data Data being sent to attribute variable
 * @param {Number} dataCount The amount of data to pass per vertex
 * @param {String} attribName The name of the attribute variable
 */
function sendAttributeBufferToGLSL(gl, data, dataCount, attribName, shape) {
  var vertexBuffer = gl.createBuffer();
  if(!vertexBuffer){
    console.log("Failed to create the buffer object");
    return -1;
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  var a_Position = gl.getAttribLocation(gl.program, attribName);
  if(a_Position < 0){
    console.log("Failed to get the storage location of a_Position");
    return -1;
  }
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, 0, 0);
  
  gl.enableVertexAttribArray(a_Position);
  //I have a feeling I might need to disable this at some point.
}

/**
 * Draws the current buffer loaded. Buffer was loaded by sendAttributeBufferToGLSL.
 *
 * @param {Integer} pointCount The amount of vertices being drawn from the buffer.
 */
function tellGLSLToDrawCurrentBuffer(gl, pointCount) {
  //console.log("point count: " + pointCount);
  gl.drawArrays(gl.TRIANGLES, 0, pointCount);
}

/**
 * Sends a float value to the specified uniform variable within GLSL shaders.
 * Prints an error message if unsuccessful.
 *
 * @param {float} val The float value being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformFloatToGLSL(gl, val, uniformName) {
  gl.uniform1f(uniformName, val);
}

/**
 * Sends an JavaSript array (vector) to the specified uniform variable within
 * GLSL shaders. Array can be of length 2-4.
 *
 * @param {Array} val Array (vector) being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformVec4ToGLSL(gl, val, uniformName) {
  if(val.length == 2)
    gl.uniform2f(uniformName, val[0], val[1]);
  else if(val.length == 3)
    gl.uniform3f(uniformName, val[0], val[1], val[2]);
  else
    gl.uniform4f(uniformName, val[0], val[1], val[2], val[3]);
}
