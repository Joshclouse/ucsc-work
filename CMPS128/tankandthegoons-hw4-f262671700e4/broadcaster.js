module.exports.broadcast = function(url, method, nodes, my_ip_port, body, on_response, on_err, sendToMe) {
	for(var i=0; i<nodes.length; i++) {
		if((nodes[i].ip + ':' + nodes[i].port != my_ip_port) || (sendToMe)) {
			var bodyString = JSON.stringify(body);
			var options = {
				host: nodes[i].ip,
				port: nodes[i].port,
				path: url,
				method: method,
				headers: {
					'Content-Type': 'application/json',
					'Content-Length': bodyString.length
				}
			};
			var httpRequest = http.request(options, function(response) {
				response.on('data', on_response);
			});

			httpRequest.on('socket', function (socket) {
				socket.setTimeout(400);  
				socket.on('timeout', function() {
					httpRequest.abort();
				});
			});

			httpRequest.on('error', function(err) {
				if(err.code === "ECONNRESET") {
					on_err();
				}
			});

			httpRequest.write(bodyString);
			httpRequest.end();
		}
	}
};

module.exports.message = function(url, ip, port, method, body, on_response, on_err) {
	var bodyString = JSON.stringify(body);
	var options = {
		host: ip,
		port: port,
		path: url,
		method: method,
		headers: {
			'Content-Type': 'application/json',
			'Content-Length': bodyString.length
		}
	};

	var httpRequest = http.request(options, function(response) {
		response.on('data', on_response);
	});

	httpRequest.on('socket', function (socket) {
		socket.setTimeout(400);  
		socket.on('timeout', function() {
			httpRequest.abort();
		});
	});

	httpRequest.on('error', function(err) {
		if(err.code === "ECONNRESET") {
			on_err()
		}
	});
	httpRequest.write(bodyString);
	httpRequest.end();
};

