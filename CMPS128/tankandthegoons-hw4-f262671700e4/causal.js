var strToNums = function(str) {
	var vc = str.split('.');
	var nums = [];
	for(var i=0; i<vc.length; i++) {
		nums.push(Number(vc[i]));
	}
	return nums;
};

var numsToStr = function(nums) {
	var str = '';
	for(var i=0; i<nums.length; i++) {
		if(i < nums.length -1) {
			str += String(nums[i]) + '.';
		} else {
			str += String(nums[i]);
		}
	}
	return str;
};

module.exports = {
	startup: function(view) {
		var str = '';
		for(var i=0; i<view.length; i++) {
			if(i<view.length-1) {
				str += '0.';
			} else {
				str += '0';
			}
		}
		return str;
	},
	myVcIndex: function(view, ip) {
		for(var i=0; i<view.length; i++) {
			if(view[i].ip == ip) {
				return i;
			}
		}
		return -1;
	},
	increment: function(vc, index) {
		var nums = strToNums(vc);
		nums[index]++;
		return numsToStr(nums);
	},
	compare: function(myVc, incomingVc) {
		myNums = strToNums(myVc);
		incomingNums = strToNums(incomingVc);
		for(var i=0; i<myNums.length; i++) {
			if(incomingNums[i] > myNums[i]) {
				return 1;
			}
		}
		for(var i=0; i<myNums.length; i++) {
			if(incomingNums[i] < myNums[i]) {
				return -1;
			}
		}
		return 0;
	}
};