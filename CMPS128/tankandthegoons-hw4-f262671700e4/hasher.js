var maxHash = 2049;

var search = function(nodes, target) {
	for(var i=0; i<nodes.length; i++) {
		if(nodes[i].hash == target) {
			return true;
		}
	}
	return false;
};

var matchingNode = function(nodes, target) {
	for(var i=0; i<nodes.length; i++) {
		if(nodes[i].hash == target) {
			return nodes[i];
		}
	}
};

module.exports.hash = function(str) {
	var hex = crypto.createHash('md5').update(str).digest('hex');
	hex = '0x' + hex;
	return parseInt(hex) % maxHash;
};
 
module.exports.advance = function(num) {
	if(num == maxHash) {
		return 0;
	} else {
		return (num + 1);
	}
};

module.exports.retreat = function(num) {
	if(num == 0) {
		return maxHash;
	} else {
		return (num - 1);
	}
};

module.exports.assemblePartitionView = function(nodes) {
	var id = 0;
	var partIndex = -1;

	var part_view = [];

	for(var i=0; i<nodes.length; i++) {
		if(i % k == 0) {
			part_view.push({
				partition_id: id,
				hash: nodes[i].hash,
				primary: {
					ip: nodes[i].ip,
					port: nodes[i].port
				},
				backups: []
			});
			partIndex++;
			id++;
		} else {
			part_view[partIndex].backups.push({
				ip: nodes[i].ip,
				port: nodes[i].port
			});
		}
	}
	return part_view;
};

module.exports.findHashPartition = function(str, partitions) {
	var hash = this.hash(str);
	while(!search(partitions, hash)) {
		hash = hasher.retreat(hash);
	}
	return matchingNode(partitions, hash);
};