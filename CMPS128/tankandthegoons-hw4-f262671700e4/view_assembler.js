
broadcaster = require('./broadcaster.js');
hasher = require('./hasher.js');

var search = function(nodes, target) {
	for(var i=0; i<nodes.length; i++) {
		if(nodes[i].hash == target) {
			return true;
		}
	}
	return false;
};

var matchingNode = function(nodes, target) {
	for(var i=0; i<nodes.length; i++) {
		if(nodes[i].hash == target) {
			return nodes[i];
		}
	}
};

module.exports.startupView = function(ip_port) {
	var viewString;
	if (process.env.VIEW != undefined) {
		viewString = process.env.VIEW;
	} else {
		viewString = ip_port;
		console.log("UNDEFINED");
	}

	if(viewString.search(', ') != -1) {
		var viewIPPort = viewString.split(', ');
	} else {
		var viewIPPort = viewString.split(',');
	}

	var view = [];
	for(var i=0; i<viewIPPort.length; i++) {
		var node = {
			ip: viewIPPort[i].split(':')[0],
			port: viewIPPort[i].split(':')[1],
			hash: hasher.hash(viewIPPort[i])
		}
		view.push(node);
	}
	console.log(view);
	return view;
};


module.exports.assemblePartition = function(part_view, ip) {
	var partition = [];
	for(var i=0; i<part_view.length; i++) {
		for(var j=0; j<part_view[i].backups.length; j++) {
			if(part_view[i].backups[j].ip == ip) {
				for(var k=0; k<part_view[i].backups.length; k++) {
					partition.push(part_view[i].backups[k]);
				}
				partition.push(part_view[i].primary);
			}
		}
		if(part_view[i].primary.ip == ip) {
			for(var l=0; l<part_view[i].backups.length; l++) {
				partition.push(part_view[i].backups[l]);
			}
			partition.push(part_view[i].primary);
		}
	}
	return partition;
};


module.exports.my_partition = function(part_view, ip) {
	for(var i=0; i<part_view.length; i++) {
		if(part_view[i].primary == ip) return part_view[i].partition_id;
		for(var j=0; j<part_view[i].backups.length; j++) {
			if(part_view[i].backups[j].ip == ip) return part_view[i].partition_id;
		}
	}
};

// Finds the partition ID that the ip/port belongs to
module.exports.find_partition_id = function(partition_view, ip, port) {
	var my_partition = 0;
	if (partition_view.length > 1) {
		var found = false;
		for (var i = 0; i < partition_view.length; i++) {
			if (ip == partition_view[i].primary.ip && port == partition_view[i].primary.port) {
				my_partition = partition_view[i].partition_id;
				//console.log("PRIMARY");
				break;
			}
			for (var j = 0; j < partition_view[i].backups.length; j++) {
				if (ip == partition_view[i].backups[j].ip && port == partition_view[i].backups[j].port) {
					my_partition = partition_view[i].partition_id;
					found = true;
					//console.log("BACKUP "+j);
					break;
				}
			}
			if (found) break;
		}
	}
	//console.log("In partition "+my_partition);
	return my_partition;
}; 

