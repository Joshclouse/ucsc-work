express = require('express');
comp_ip = require('ip');
bodyParser = require('body-parser');
http = require('http');
uuid = require('uuid/v4');
broadcaster = require('./broadcaster.js');
viewAssembler = require('./view_assembler.js');
url_mod = require('url');
crypto = require('crypto');
async = require('async');
querystring = require('querystring');
hasher = require('./hasher.js');
causal = require('./causal.js');

app = express();

app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({
  extended: true
})); 

ip_port = process.env.IPPORT;

if(ip_port != undefined) {
	ip = ip_port.split(':')[0];
	port = ip_port.split(':')[1];
} else {
	ip = comp_ip.address();
	port = '8080';
	ip_port = ip + ':' + port;
}

var countActiveNodes = function(ip_port, view, callback) {
	var count = 0;
	console.log("Checking "+view.length+" nodes...");
	broadcaster.broadcast('/heartbeat', 'GET', view, ip_port, {}, function(chunk) { //on response
		count++;
		var res_obj = JSON.parse(''+chunk);
		console.log("    Active: "+res_obj.ip+":"+res_obj.port);
		//console.log("Count increased: "+count);
		if (count >= view.length) {
			//console.log("Returned "+count);
			callback(count);
		}
	}, function() { //on timeout
	
	}, true);
	setTimeout(function() {
		//console.log("Returned "+count);
		callback(count);
	}, 200);
}

k = process.env.K;
view = viewAssembler.startupView(ip_port);
partition_view = hasher.assemblePartitionView(view);
partition = viewAssembler.assemblePartition(partition_view, ip);
var my_partition = viewAssembler.find_partition_id(partition_view, ip, port);
var activeNodes = 0;
my_vc = causal.startup(view);

var kvs = {};
var log = [];
var viewLog = [];

var timeoutCap = 400;

//send heartbeats every second to all nodes in my partition
setInterval(function() {
	broadcaster.broadcast('/compare-kvs', 'POST', partition, ip_port, {kvs: kvs, log: log, vc: my_vc}, function(chunk) { // on response
		//do nothing for now
	}, 
	function() { // on timeout
		console.log('someone in the partition is down');
	}, false);
}, 1000);

//send heartbeats every second to everyone in my view
setInterval(function() {
	//console.log("Active nodes: "+activeNodes);
	countActiveNodes(ip_port, view, function(count) {
		activeNodes = count;
		//my_partition = viewAssembler.find_partition_id(partition_view, ip, port);
		broadcaster.broadcast('/compare-view', 'POST', view, ip_port, {view: view, viewLog: viewLog}, function(chunk) { // on response
		//do nothing for now
		}, 
		function() { // on timeout
			console.log('someone in the view is down');
		}, false);
		});
}, 2000);

var find_key = function(store, target) {
	if(store['target'] == undefined) {
		return false;
	}
	else {
		return true;
	}
};

var find_node = function(view, target) {
	if (view.indexOf(target) == null) {
		return false;
	} else {
		return true;
	}
};

var find_latest_update = function(updates, k) {
	for(var i=updates.length-1; i>=0; i--) {
		if(updates[i].key == k) {
			return updates[i];
		}
	}
	return -1;
};

var find_latest_update_node = function(updates, node) {
	for (var i = updates.length-1; i >= 0; i--) {
		if (updates[i].node == node) {
			return updates[i];
		}
	}
	return -1;
} 



app.post('/compare-kvs', function(req, res) {
	// this is where we will compare the 2 kvs and look for inconsistencies. 
	// Use the log for timestamps and operation types
	var your_kvs = req.body.kvs;
	var your_log = req.body.log;
	var altered = false;
	for(my_key in kvs) {
		if(find_key(your_kvs, my_key)) {
			if(kvs[my_key] != your_kvs[my_key]) { //if we both have the key, but they don't match
				var my_last_update = find_latest_update(log, my_key);
				var your_last_update = find_latest_update(your_log, my_key);
				if(causal.compare(my_last_update.clock, your_last_update.clock) == 1) {
					kvs[my_key] = your_kvs[my_key];
					altered = true;
					log.push({
						type: 'insert',
						time: your_last_update.clock,
						key: my_key,
						value: kvs[my_key]
					});
				}
			}
		} else { // I have a key that you don't
				 // This block of code does not apply right now because we aren't doing deletes
			var my_last_update = find_latest_update(log, my_key);
			var your_last_update = find_latest_update(your_log, my_key);
			if(your_last_update != -1) {
				if(your_last_update.type == 'delete') { // you have a delete in your log that I don't have
					if(causal.compare(my_last_update.clock, your_last_update.clock) == 1) {
						delete kvs[my_key];
						altered = true;
						log.push({
							type: 'delete',
							clock: your_last_update.clock,
							key: my_key
						});

					}
				}
			}
		}
	}
	for(your_key in your_kvs) {
		if(!find_key(kvs, your_key)) { //you have a key that I don't
			var my_last_update = find_latest_update(log, your_key);
			var your_last_update = find_latest_update(your_log, your_key);
			if(my_last_update == -1 || (causal.compare(my_last_update.clock, your_last_update.clock) == 1)) {
				kvs[your_key] = your_kvs[your_key];
				altered = true;
				log.push({
					type: 'insert',
					clock: your_last_update.clock,
					key: your_key,
					value: kvs[your_key]
				});
			}
		}
	}
	if(altered) {
		my_vc = req.body.vc;
	}
	//console.log(kvs);
	res.send('ack');
});

app.post('/compare-view', function(req, res) {
	var your_view = req.body.view;
	var your_log = req.body.viewLog;
	var altered = false;
	for(my_node in view) {
		if(!find_node(your_view, my_node)) { // I have a node that you don't
			var my_last_update = find_latest_update_node(viewLog, my_node);
			var your_last_update = find_latest_update_node(your_log, my_node);
			if(your_last_update != -1) {
				if(your_last_update.type == 'remove') { // you have a remove in your log that I don't have
					if(my_last_update.time < your_last_update.time) {
						view.splice(view.indexOf(my_node), 1);
						altered = true;
						viewLog.push({
							type: 'remove',
							time: your_last_update.time,
							node: my_node
						});
					}
				}
			}
		}
	}
	for(your_node in your_view) {
		if(!find_node(view, your_node)) { //you have a node that I don't
			var my_last_update = find_latest_update_node(viewLog, your_node);
			var your_last_update = find_latest_update_node(your_log, your_node);
			if(my_last_update == -1 || (my_last_update.time < your_last_update.time)) {
				view.push(your_node);
				altered = true;
				viewLog.push({
					type: 'add',
					time: your_last_update.time,
					node: your_node,
				});
			}
		}
	}
	if(altered) {
		//my_vc = req.body.vc;
		console.log("    VIEW ALTERED");
	}
	//console.log(view); // Prints entire view. Un-comment at your discretion
	res.send('ack');
});

var broadcastIncrement = function(nodes, index) {
	broadcaster.broadcast('/increment', 'PUT', nodes, ip_port, {index: index}, function(chunk) {
		// do nothing
	}, function() {
		// do nothing
	}, false);
};

app.put('/kvs', function(req, res) {
	var key = req.body.key;
	var value = req.body.value;
	var payload = req.body.causal_payload;
	if(payload == '') {
		payload = '0.0.0.0';
	}
	var responded = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			var hashed_partition = hasher.findHashPartition(key, partition_view);
			var nodes = [];
			nodes.push(hashed_partition.primary);
			for(var i=0; i<hashed_partition.backups.length; i++) {
				nodes.push(hashed_partition.backups[i]);
			}
			var time = Math.floor(new Date().getTime() / 1000);
			broadcaster.broadcast('/new-key', 'PUT', nodes, ip_port, {
				key: key,
				value: value,
				payload: payload,
				time: time
			}, function(chunk) {
				if(!responded) {
					responded = true;
					var res_obj = JSON.parse(String(chunk));
					my_vc = causal.increment(my_vc, causal.myVcIndex(view, ip));
					res_obj['causal_payload'] = my_vc;
					res_obj['partition_id'] = hashed_partition.partition_id;
					res_obj['timestamp'] = time;
					broadcastIncrement(view, causal.myVcIndex(view, ip));
					res.status(res_obj.status);
					delete res_obj['status'];
					res.json(res_obj);
					clearInterval(buffer);
				}
			}, function() {
				console.log('Timeout!')
			}, true);
		}
	}, 50);
	setTimeout(function() {
		if(!responded) {
			clearInterval(buffer);
			res.status(400);
			res.json({
				msg: 'error',
				error: 'key value store is not available'
			});
		}
	}, timeoutCap);
});

app.get('/kvs', function(req, res) {
	var url_parts = url_mod.parse(req.url, true);
	var query = url_parts.query;
	var key = String(query.key);
	var payload = query.causal_payload;
	if(payload == '') {
		payload = '0.0.0.0';
	} 
	var responded = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			var hashed_partition = hasher.findHashPartition(key, partition_view);
			var nodes = [];
			nodes.push(hashed_partition.primary);
			for(var i=0; i<hashed_partition.backups.length; i++) {
				nodes.push(hashed_partition.backups[i]);
			}
			var time = Math.floor(new Date().getTime() / 1000);
			broadcaster.broadcast('/get-key', 'POST', nodes, ip_port, {
				key: key,
				payload: payload,
				time: time
			}, function(chunk) {
				if(!responded) {
					responded = true;
					var res_obj = JSON.parse(String(chunk));
					my_vc = causal.increment(my_vc, causal.myVcIndex(view, ip));
					res_obj['causal_payload'] = my_vc;
					res_obj['partition_id'] = hashed_partition.partition_id;
					res_obj['timestamp'] = time;
					broadcastIncrement(view, causal.myVcIndex(view, ip));
					res.status(res_obj.status);
					delete res_obj['status'];
					res.json(res_obj);
					clearInterval(buffer);
				}
			}, function() {
				console.log('Timeout!')
			}, true);
		}
	}, 50);
	setTimeout(function() {
		if(!responded) {
			clearInterval(buffer);
			res.status(400);
			res.json({
				msg: 'error',
				error: 'key value store is not available'
			});
		}
	}, timeoutCap);
});

app.delete('/kvs', function(req, res) {
	var url_parts = url_mod.parse(req.url, true);
	var query = url_parts.query;
	var key = String(query.key);
	var payload = query.causal_payload;
	if(payload == '') {
		payload = '0.0.0.0';
	} 
	var responded = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			var hashed_partition = hasher.findHashPartition(key, partition_view);
			var nodes = [];
			nodes.push(hashed_partition.primary);
			for(var i=0; i<hashed_partition.backups.length; i++) {
				nodes.push(hashed_partition.backups[i]);
			}
			var time = Math.floor(new Date().getTime() / 1000);
			broadcaster.broadcast('/delete-key', 'DELETE', nodes, ip_port, {
				key: key,
				payload: payload,
				time: time
			}, function(chunk) {
				if(!responded) {
					responded = true;
					var res_obj = JSON.parse(String(chunk));
					my_vc = causal.increment(my_vc, causal.myVcIndex(view, ip));
					res_obj['causal_payload'] = my_vc;
					res_obj['partition_id'] = hashed_partition.partition_id;
					res_obj['timestamp'] = time;
					broadcastIncrement(view, causal.myVcIndex(view, ip));
					res.status(res_obj.status);
					delete res_obj['status'];
					res.json(res_obj);
					clearInterval(buffer);
				}
			}, function() {
				console.log('Timeout!')
			}, true);
		}
	}, 50);
	setTimeout(function() {
		if(!responded) {
			clearInterval(buffer);
			res.status(400);
			res.json({
				msg: 'error',
				error: 'key value store is not available'
			});
		}
	}, timeoutCap);
});


app.put('/new-key', function(req, res) {
	var key = req.body.key;
	var value = req.body.value;
	var payload = req.body.payload;
	var time = req.body.time;
	var cleared = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			if(key == undefined || value == undefined) { //key without value or value without key
				res.json({
					msg: 'error',
					status: 400
				});
			} else if(key.length > 250 || key.length < 1) { //key is too long
				res.json({
					msg: 'error',
					status: 400
				});
			} else if(kvs[key] != undefined) { //value replaced
				kvs[key] = value;
				log.push({
					type: 'insert',
					clock: my_vc,
					key: key,
					value: value
				});
				res.json({
					replaced: 1,
					msg: 'success',
					status: 200
				});
			} else { //value created
				kvs[key] = value;
				log.push({
					type: 'insert',
					clock: my_vc,
					key: key,
					value: value
				});
				res.json({
					replaced: 0,
					msg: 'success',
					status: 201
				});
			}
			cleared = true;
			clearInterval(buffer);
		}
	}, 50);
	setTimeout(function() {
		if(!cleared) {
			clearInterval(buffer);
		}
	}, timeoutCap);
});

app.post('/get-key', function(req, res) {
	var key = req.body.key;
	var payload = req.body.payload;
	var time = req.body.time;
	var cleared = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			if(key == undefined) { //no key passed
				res.json({
					msg: 'error',
					error: 'no key passed',
					status: 400
				});
			} else if(kvs[key] == undefined) { //key does not exist in kvs
				res.json({
					msg: 'error',
					error: 'key does not exist',
					status: 400
				});
			} else {
				res.json({
					msg: 'success',
					value: kvs[key],
					status: 200
				});
			}
			cleared = true;
			clearInterval(buffer);
		}
	}, 50);
	setTimeout(function() {
		if(!cleared) {
			clearInterval(buffer);
		}
	}, timeoutCap);
});

app.delete('/delete-key', function(req, res) {
	var key = req.body.key;
	var payload = req.body.payload;
	var time = req.body.time;
	var cleared = false;
	var buffer = setInterval(function() {
		if(causal.compare(my_vc, payload) != 1) {
			if(key == undefined) { // no key passed
				res.json({
					msg: 'error',
					error: 'no key passed',
					status: 400
				});
			} else if(kvs[key] == undefined) { // key does not exist in kvs
				res.json({
					msg: 'error',
					error: 'key does not exist',
					status:404
				});
			} else {
				delete kvs[key];
				log.push({
					type: 'delete',
					clock: my_vc,
					key: key
				});
				res.json({
					msg: 'success',
					status: 200
				});
			}
			cleared = true;
			clearInterval(buffer);
		}
	}, 50);
	setTimeout(function() {
		if(!cleared) {
			clearInterval(buffer);
		}
	}, timeoutCap);
});

app.put('/increment', function(req, res) {
	var index = req.body.index;
	my_vc = causal.increment(my_vc, index);
	res.send('ack');
});


app.put('/kvs/view_update', function(req, res) {
	var currentNodes = activeNodes;
	if(req.body.type == 'add') {
		var responded = false;
		var new_ip = req.body.ip_port.split(':')[0];
		var new_port = req.body.ip_port.split(':')[1];
		var new_part = true;
		var time = Math.floor(new Date().getTime() / 1000);
		if(view.length > 1 && currentNodes > 1) {
			console.log("right before broadcasting add node");
			//console.log("Counting active nodes...");
			//var activeNodes = countActiveNodes(ip_port, view);
			var commit_responses = [];
			broadcaster.broadcast('/add_node', 'PUT', view, ip_port, {ip_port: req.body.ip_port, time: time}, function(node_response) {
				var allCommitted = true;
				console.log("inside broadcast for /add_node");
				var commit_obj = JSON.parse(''+node_response);
				commit_responses.push(commit_obj.response);
				console.log("    Commit responses: "+commit_responses.length);
				console.log("    Active nodes: "+currentNodes);
				if (commit_responses.length >= currentNodes-1) {
					for (var i = 0; i < commit_responses.length; i++) {
						if (commit_responses[i] == 2) {
							console.log("new_part = false in /add_node callback");
							new_part = false;
						}
					}
					if(allCommitted) {
						//commit the change on this node
						console.log("in all comitted");
						var new_node = {
							ip: new_ip,
							port: new_port,
							hash: hasher.hash(req.body.ip_port)
						};
						view.push(new_node);
						currentNodes++;
						viewLog.push({
							type: 'add',
							time: time,
							node: new_node
						});
						console.log("Pushed the new node in caller");
						var new_partInd = 0;
						for(i=0; i < partition_view.length; i++)
						{
							new_partInd = i;
							if(partition_view[i].backups.length < k-1 || partition_view[i].primary.ip==null){
								//new_partInd = i;
								break;
							}
						}
						if(new_part == false && partition_view[new_partInd].primary.ip != null){
							partition_view[new_partInd].backups.push(new_node);
						}
						else if(new_part == false && partition_view[new_partInd].primary.ip == null){
							partition_view[new_partInd].primary.ip = new_node.ip;
							partition_view[new_partInd].primary.port = new_node.port;
						}
						else{
							console.log("new partition is pushed: "+new_partInd);
							partition_view.push({
								partition_id: partition_view.length,
								hash: new_node.hash,
								primary:{
									ip: new_node.ip,
									port: new_node.port
								},
								backups:[]
							});
						}
						partition = viewAssembler.assemblePartition(partition_view, ip);
						console.log("before update view");
						console.log("Node caller view len "+ view.length);
						console.log("Node caller partlen "+ partition_view.length);
						broadcaster.message('/update_view', new_node.ip, new_node.port, 'PUT',
							{view: view,
							 partition_view: partition_view,
							 activeNodes: currentNodes
							}, function(chunk){
								console.log("in message callback update view");
							var view_updated = JSON.parse(''+chunk);
							if (view_updated.response) {
								console.log("view_updated.response is true");
							//console.log("pushed the new node with hash: " + new_node.hash);
							//console.log("Elements in view[] after addition: "+view.length);
							//console.log("pushed the new node with hash: " + new_node.hash);
								if(new_part==true){
									console.log("new_part is true");
									var hash_brown = hasher.retreat(partition_view[partition_view.length-1].hash);
									var generous_part = 0;
									for(var j = 0; j < 2049; j++){
										for(var tank = 0; tank < partition_view.length; tank++){
											if(partition_view[tank].hash == hash_brown){
												generous_part = tank;
												break;
											}
										}
										if(partition_view[generous_part].hash == hash_brown)
											break;
										hash_brown = hasher.retreat(hash_brown);
									}
									var part_boy = [];
									part_boy.push(partition_view[generous_part].primary)
									for(var webber = 0; webber < partition_view[generous_part].backups.length; webber++){
										part_boy.push(partition_view[generous_part].backups[webber]);
									}
									broadcaster.broadcast('/redistribute', 'PUT', part_boy, ip, {node: new_node},function(hunk){
										var balanced = [];
										var good_boy = true;
										var resp = JSON.parse(''+hunk);
										balanced.push(resp.response);
										for(var b = 0; b < balanced.length; b++){
											if(balanced[b] != true) {
												good_boy = false;
												console.log("second error");
												if(!responded){
													res.json({msg:'error'});
													responded = true;
												}
											}
												
										}
										if(good_boy == true){
											console.log("first success");
											if(!responded){
												responded = true;
												broadcaster.message('/countActiveParts', new_node.ip, new_node.port, 'PUT', {activeNodes: currentNodes}, function(bunk){
													var number_of_partitions_obj = JSON.parse(''+bunk);
													var number_of_partitions = number_of_partitions_obj.count;
													console.log('number of partition obj.count: '+number_of_partitions_obj.count);
													var part = number_of_partitions_obj.partition;
													activeNodes = currentNodes;
													res.json({msg:'success', partition_id: part, number_of_partitions: number_of_partitions});
												}, function(){
												});
											}
										}
									},
									function(){
									},false);
								}
								else{
									console.log("new part is false");
									
									
									console.log("second success");
									if(!responded){
										responded = true;
										broadcaster.message('/countActiveParts', new_node.ip, new_node.port, 'PUT', {activeNodes: currentNodes}, function(bunk){
											var number_of_partitions_obj = JSON.parse(''+bunk);
											var number_of_partitions = number_of_partitions_obj.count;
											var part = number_of_partitions_obj.partition;
											activeNodes = currentNodes;
											res.json({msg:'success',
											partition_id: part,
											number_of_partitions: number_of_partitions})
											if (my_partition == part)
												partition.push(new_node);
										}, function(){
										
										});
									}
						
								}
							}
							else{
								console.log("last error");
								if(!responded){
									responded = true;
									res.json({msg:'error'})
								}
							}
						}, function(){
						});					
					}
				}
			}, function() {
			
			}, false);
		}
		else {
			console.log("made it to this ballsy ass else");
			var new_node = {
				ip: new_ip,
				port: new_port,
				hash: hasher.hash(req.body.ip_port)
			};
			view.push(new_node);
			currentNodes++;
			viewLog.push({
				type: 'add',
				time: time,
				node: new_node
			});
			console.log(viewLog);
			console.log("Pushed the new node in caller");
			var new_partInd = 0;
			for(i=0; i < partition_view.length; i++)
			{
				if(partition_view[i].backups.length < k-1 || partition_view[i].primary.ip==null){
					new_part = false;
					new_partInd = i;
					break;
				}
			}
			if(new_part == false && partition_view[new_partInd].primary.ip != null){
				partition_view[new_partInd].backups.push(new_node);
			}
			else if(new_part == false && partition_view[new_partInd].primary.ip == null){
				partition_view[new_partInd].primary.ip = new_node.ip;
				partition_view[new_partInd].primary.port = new_node.port;
			}
			else{
				console.log("new partition is pushed: "+new_partInd);
				partition_view.push({
					partition_id: partition_view.length,
					hash: new_node.hash,
					primary:{
						ip: new_node.ip,
						port: new_node.port
					},
					backups:[]
				});
			}
			partition = viewAssembler.assemblePartition(partition_view, ip);
			console.log("before update view");
			console.log("Node called view len "+ view.length);
			console.log("Node caller partlen "+ partition_view.length);
			broadcaster.message('/update_view', new_node.ip, new_node.port, 'PUT',
				{view: view,
				 partition_view: partition_view,
				 activeNodes: currentNodes
				}, function(chunk){
					console.log("in message callback update view");
				var view_updated = JSON.parse(''+chunk);
				if (view_updated.response) {
					console.log("view_updated.response is true");

				//console.log("pushed the new node with hash: " + new_node.hash);
				//console.log("Elements in view[] after addition: "+view.length);

				//console.log("pushed the new node with hash: " + new_node.hash);
					if(new_part==true){
						console.log("new_part is true");
						var hash_brown = hasher.retreat(partition_view[partition_view.length-1].hash);
						var generous_part = 0;
						for(var j = 0; j < 2049; j++){
							for(var tank = 0; tank < partition_view.length; tank++){
								if(partition_view[tank].hash == hash_brown){
									generous_part = tank;
									break;
								}
							}
							if(partition_view[generous_part].hash == hash_brown)
								break;
							hash_brown = hasher.retreat(hash_brown);
						}
						var part_boy = [];
						part_boy.push(partition_view[generous_part].primary)
						for(var webber = 0; webber < partition_view[generous_part].backups.length; webber++){
							part_boy.push(partition_view[generous_part].backups[webber]);
						}
						broadcaster.broadcast('/redistribute', 'PUT', part_boy, ip, {node: new_node},function(hunk){
							var balanced = [];
							var good_boy = true;
							var resp = JSON.parse(''+hunk);
							balanced.push(resp.response);
							for(var b = 0; b < balanced.length; b++){
								if(balanced[b] != true){
									good_boy = false;
									console.log("second error");
									if(!responded){
										res.json({msg:'error'});
										responded = true;
									}
								}
									
									
									
							}
							if(good_boy == true){
								console.log("first success");
								console.log("responded: " + responded);
								if(!responded) {
									responded = true;
									broadcaster.message('/countActiveParts', new_node.ip, new_node.port, 'PUT', {activeNodes: currentNodes}, function(funk){
										var number_of_partitions_obj = JSON.parse(''+funk);
										var number_of_partitions = number_of_partitions_obj.count;
										var part = number_of_partitions_obj.partition;
										activeNodes = currentNodes;
										res.json({msg:'success', partition_id: part, number_of_partitions: number_of_partitions});
									}, function(){
										res.json({error: 'unable to countActiveParts'});
									});
								}
							}

						},
						function(){

						},false);
					}
					else{
						console.log("new part is false");
						broadcaster.message('/countActiveParts', new_node.ip, new_node.port, 'PUT', {activeNodes: currentNodes}, function(funk){
							if(!responded){
								responded = true;
								var number_of_partitions_obj = JSON.parse(''+funk);
								var number_of_partitions = number_of_partitions_obj.count;
								var part = number_of_partitions_obj.partition;
								res.json({msg:'success',
								partition_id: part,
								number_of_partitions: number_of_partitions});
								if (my_partition == part)
									partition.push(new_node);
							}	
						}, function(){
						
						});
						console.log("second success");
						//if(!responded){
						//	responded = true;
						//	res.json({msg:'success'});
						//}
			
					}
				}
				else{
					console.log("last error");
					if(!responded){
						responded = true;
						res.json({msg:'error'})
					}
				}
			}, function(){

			});
		}
	}

	//Start of delete---------------------------------------------------------------------------------------------------------------
	//2 Lines for readability-------------------------------------------------------------------------------------------------------
	if(req.body.type == 'remove') {
		console.log("Starting node removal...");
		var rem_ip = req.body.ip_port.split(':')[0];
		var rem_port = req.body.ip_port.split(':')[1];
		if (ip == rem_ip && port == rem_port) {
			if (!responded) {
				responded = true;
				res.status(400);
				res.json({
					msg: 'error',
					error: 'cannot remove myself'
				});
			}
		}
		var commit_responses = [];
		var viewIndex = null;
		var node_to_remove;
		var time = Math.floor(new Date().getTime() / 1000);
		var partIndex = viewAssembler.find_partition_id(partition_view, rem_ip, rem_port); // find partition id of node to remove
		// Check if node to remove exists in view
		for (var i = 0; i < view.length; i++) {
			if (rem_ip == view[i].ip && rem_port == view[i].port) {
				node_to_remove = view[i];
				viewIndex = i;
				break;
			}
		}
		if (viewIndex != null) {
			var node_part = partition_view[partIndex]; // partition the node to remove belongs to
			console.log("Current Primary in node_part: "+node_part.primary);
			console.log("Current number of Backups in node_part: "+node_part.backups.length); 
			var isPrimary;
			console.log("My partition: "+my_partition);
			// Check if node is a primary
			if (rem_ip == node_part.primary.ip && rem_port == node_part.primary.port) {
				console.log("Node to remove is a PRIMARY in partition "+node_part.partition_id);
				isPrimary = true;
			} else { // Otherwise, check which backup it is
				for (var i = 0; i < node_part.backups.length; i++) {
					if (rem_ip == node_part.backups[i].ip && rem_port == node_part.backups[i].port) {
						var backupIndex = i;
					}
				}
				console.log("Node to remove is a BACKUP in partition "+node_part.partition_id);
				isPrimary = false;
			}
			// Check if the node to remove is up or not
			broadcaster.message('/heartbeat', rem_ip, rem_port, 'PUT', {}, function(chunk) { // on response
				// If the node to remove is up -----------------------------------------------------------------------------------------
				console.log("Node to remove is alive");
				var new_node_part = []; // Nodes in the node_to_remove's partition 
				if (node_part.primary.ip != null)
					new_node_part.push(node_part.primary);
				for (var i = 0; i < node_part.backups.length; i++) {
					new_node_part.push(node_part.backups[i]);
				}
				console.log("Found "+new_node_part.length+" nodes in its partition:");
				console.log(new_node_part);
				console.log("Broadcasting redistribution to partition...");
				var acks = 0;
				var commit_responses = [];
				var sendToMe = false;
				var redistributed = false;
				if (node_part.partition_id == my_partition) {
					sendToMe = true;
				}
				// Redistribution for removal
				// This broadcast will give the node to remove one more chance to share its kvs with its partition
				// in case it has any keys that the others don't. This only works if the node to remove is
				// up and running, which is what the /heartbeat message above is for. 
				broadcaster.broadcast('/compare-kvs', 'POST', new_node_part, ip_port,
					{kvs: kvs, log: log}, function(chunk){ //on response
					if (chunk == 'ack') {
						acks++;
					}
					console.log("    Redis. acknowledgements: "+acks);
					if (!redistributed && ((new_node_part.length > 1 && acks >= 1) || (new_node_part.length == 1))) {
						redistributed = true;
						console.log("Redistribution successful.");
						// Tell nodes to commit delete
						console.log("Elements in view[] before removal: "+view.length);
						console.log("Broadcasting remove to all nodes...");
						broadcaster.broadcast('/remove_node', 'DELETE', view, ip_port, {isPrimary: isPrimary, partIndex: partIndex, 
						rem_ip: rem_ip, rem_port: rem_port, viewIndex: viewIndex, backupIndex: backupIndex, time: time}, 
						function(node_response) { //on response
							var commit_obj = JSON.parse(''+node_response);
							commit_responses.push(commit_obj.removed);
							console.log("    Removes committed: "+commit_responses.length);
							if(commit_responses.length >= currentNodes-1) {
								var allCommitted = true;
								for(var i=0; i<commit_responses.length; i++) {
									if(commit_responses[i] == false) {
										allCommitted = false;
										console.log("Remove error: not all committed.");
										if (!responded) {
											res.json({
												msg: 'error: not all committed'
											});
											responded = true;
										}
									}
								}
								// Commit change in this node
								if (allCommitted) {
									viewLog.push({
										type: 'remove',
										time: time,
										node: view[viewIndex]
									});
									console.log(viewLog);
									view.splice(viewIndex, 1); // deletes 1 element starting at viewIndex from view[]
									currentNodes--;
									if (my_partition == node_part.partition_id) {
										for (var i = 0; i < partition.length; i++) {
											if (partition[i].ip == rem_ip && partition[i].port == rem_port) {
												partition.splice(i, 1);
												break;
											}
										}
									}
									if (isPrimary) {
										node_part.primary.ip = null;
									} else {
										node_part.backups.splice(backupIndex, 1);
									}
									console.log("Remove successful.");
									console.log("Elements in view[] after removal: "+view.length);
									console.log("Primary in partition after removal: "+node_part.primary.ip);
									console.log("Elements in backups[] after removal: "+node_part.backups.length);
									broadcaster.message('/countActiveParts', ip, port, 'PUT', {activeNodes: currentNodes}, function(chunk) { // on response
										var response = JSON.parse(''+chunk);
										if (!responded) {
											var numParts = Math.ceil(currentNodes/k);
											activeNodes = currentNodes;
											res.status(200);
											res.json({
												msg: 'success',
												number_of_partitions: numParts
											});
											responded = true;
										}
									}, function() { // on timeout
										if (!responded) {
											res.json({
												msg: 'error',
												error: 'could not count active parts'
											});
											responded = true;
										}
									});
								}
							}
						}, function() {
							console.log("Timeout!");
						}, false);
					} 
				}, function() { //on timeout
					console.log("Timeout!");
				}, sendToMe);
			// If the node to remove is down ------------------------------------------------------------------------------------------------
			}, function() { //on timeout
				console.log("Node to remove is dead");
				console.log("Elements in view[] before removal: "+view.length);
				var newView = []; // View without the node to remove
				for (var i =0; i < view.length; i++) {
					if (rem_ip != view[i].ip || rem_port != view[i].port)
						newView.push(view[i]);
				}
				// If the node to remove is down, there is no need for the /compare-kvs used for redistribution above,
				// since it cannot share any of its keys.
				if (newView.length > 1 && currentNodes > 1) {
					console.log("Broadcasting remove to "+newView.length+" nodes...");
					broadcaster.broadcast('/remove_node', 'DELETE', newView, ip_port, {isPrimary: isPrimary, partIndex: partIndex, 
					rem_ip: rem_ip, rem_port: rem_port, viewIndex: viewIndex, backupIndex: backupIndex, time: time}, 
					function(node_response) { //on response
						var commit_obj = JSON.parse(''+node_response);
						commit_responses.push(commit_obj.removed);
						console.log("    Removes committed: "+commit_responses.length);
						console.log("    Checking if "+commit_responses.length+" >= "+(currentNodes-2));
						if(commit_responses.length >= currentNodes-2) {
							var allCommitted = true;
							if (commit_responses.length >= 1) {
								for(var i=0; i<commit_responses.length; i++) {
									if(commit_responses[i] == false) {
										allCommitted = false;
										console.log("Remove error: not all committed.");
										if (!responded) {	
											res.json({
												msg: 'error: not all committed'
											});
											responded = true;
										}
									}
								}
							}
							// Commit change in this node
							if (allCommitted) {
								viewLog.push({
									type: 'remove',
									time: time,
									node: view[viewIndex]
								});
								console.log(viewLog);
								view.splice(viewIndex, 1); // deletes 1 element starting at viewIndex from view[]
								currentNodes--;
								if (my_partition == node_part.partition_id) {
									for (var i = 0; i < partition.length; i++) {
										if (partition[i].ip == rem_ip && partition[i].port == rem_port) {
											partition.splice(i, 1);
											break;
										}
									}
								}
								if (isPrimary) {
									node_part.primary.ip = null;
								} else {
									node_part.backups.splice(backupIndex, 1);
								}
								console.log("Remove successful.");
								console.log("Elements in view[] after removal: "+view.length);
								console.log("Primary in partition after removal: "+node_part.primary);
								console.log("Elements in backups[] after removal: "+node_part.backups.length);
								broadcaster.message('/countActiveParts', ip, port, 'PUT', {activeNodes: currentNodes}, function(chunk) { // on response
									var response = JSON.parse(''+chunk);
									if (!responded) {
										var numParts = Math.ceil(currentNodes/k);
										activeNodes = currentNodes;
										res.status(200);
										res.json({
											msg: 'success',
											number_of_partitions: numParts
										});
										responded = true;
									}
								}, function() { // on timeout
									if (!responded) {
										res.json({
											msg: 'error'
										});
										responded = true;
									}
								});
							}
						}
					}, function() {
						console.log("Timeout!");
					}, false); 
				} else {
					viewLog.push({
						type: 'remove',
						time: time,
						node: view[viewIndex]
					});
					console.log(viewLog);
					view.splice(viewIndex, 1); // deletes 1 element starting at viewIndex from view[]
					currentNodes--;
					if (my_partition == node_part.partition_id) {
						for (var i = 0; i < partition.length; i++) {
							if (partition[i].ip == rem_ip && partition[i].port == rem_port) {
								partition.splice(i, 1);
								break;
							}
						}
					}
					if (isPrimary) {
						node_part.primary.ip = null;
					} else {
						node_part.backups.splice(backupIndex, 1);
					}
					console.log("Remove successful.");
					console.log("Elements in view[] after removal: "+view.length);
					console.log("Primary in partition after removal: "+node_part.primary);
					console.log("Elements in backups[] after removal: "+node_part.backups.length);
					broadcaster.message('/countActiveParts', ip, port, 'PUT', {activeNodes: currentNodes}, function(chunk) { // on response
						var response = JSON.parse(''+chunk);
						if (!responded) {
							var numParts = Math.ceil(currentNodes/k);
							activeNodes = currentNodes;
							res.status(200);
							res.json({
								msg: 'success',
								number_of_partitions: numParts
							});
							responded = true;
						}
					}, function() { // on timeout
						if (!responded) {
							res.json({
								msg: 'error'
							});
							responded = true;
						}
					});
				}
			});
		} else {
			res.status(404);
			console.log("Remove error: node not found.");
			if (!responded) {
				res.json({
					msg: 'error: node not found'
				});
				responded = true;
			}
		}
	}
	setTimeout(function() {
		if (!responded) {
			res.status(400);
			res.json({
				msg: 'error',
				error: 'service is unavailable'
			});
		}
	}, 1000);
});

app.get('/kvs/get_number_of_keys', function(req, res){
	res.json({
		count: Object.keys(kvs).length
	});
});

app.put('/add_node', function(req, res) {
	console.log("In add_node");
	var new_ip = req.body.ip_port.split(':')[0];
	var new_port = req.body.ip_port.split(':')[1];
	var new_node = {
		ip: new_ip, 
		port: new_port,
		hash: hasher.hash(req.body.ip_port)
	};
	var new_part = true;
	var new_partInd = 0;
	view.push(new_node);
	activeNodes++;
	viewLog.push({
		type: 'add',
		time: req.body.time,
		node: new_node
	});
	console.log("Added the new node");
	for(i=0; i < partition_view.length; i++)
	{
		console.log("Length of partition "+i+" backups: "+partition_view[i].backups.length);
		if(partition_view[i].backups.length < k-1 || partition_view[i].primary.ip==null){
			new_part = false;
			new_partInd = i;
			break;
		}
	}
	if(new_part == false && partition_view[new_partInd].primary.ip != null){
		partition_view[new_partInd].backups.push(new_node);
	}
	else if(new_part == false && partition_view[new_partInd].primary.ip == null){
		partition_view[new_partInd].primary.ip = new_node.ip;
		partition_view[new_partInd].primary.port = new_node.port;
	}
	else{
		console.log("new partition is pushed: " + new_partInd);
		partition_view.push({
			partition_id: partition_view.length,
			hash: new_node.hash,
			primary:{
				ip: new_node.ip,
				port: new_node.port
			},
			backups:[]
		});
	}
	console.log(viewLog);
	partition = viewAssembler.assemblePartition(partition_view, ip);
	console.log("My new viewlen is: " + view.length);
	console.log("My new partlen is: " + partition_view.length);
	console.log("what is new part set to? "+ new_part);
	if(new_part == true)
		res.json({response: 1});
	else if(new_part == false)
		res.json({response: 2});
	else
		res.json({response: 0});
});

app.put('/redistribute', function(req, res){
	var redistributed = false;
	var new_node = req.body.node;
	var hash_boy;
	var key_transfers = [];
	var transfered = true;
	var transfer_num = 0;
	var mash;
	for(var key in kvs){
		mash = (hasher.findHashPartition(key, partition_view)).hash;
		if(mash != partition_view[my_partition].hash) {
			transfer_num++;
		}
	}
	//console.log("in redistribute");
	for(var key in kvs){
		//console.log("in red loop" + key);
		hash_boy = (hasher.findHashPartition(key, view)).hash;
		if(hash_boy != partition_view[my_partition].hash){
			//transfer_num++;
			broadcaster.message('/insert', new_node.ip, new_node.port, 'PUT', {key: key, value: kvs[key]}, function(hunk){
				var transfer = JSON.parse(''+hunk);
				key_transfers.push(transfer.response);
				if(key_transfers.length == transfer_num){
					//console.log("in new if statement")
					redistributed = true;
					for(var i = 0; i < key_transfers.length; i++){
						if(key_transfers[i] == false){
							redistributed = false;
						}
					}
					res.json({response: redistributed});
				}
				//delete kvs[key];
			});
			delete kvs[key];
		}
	}
	if(transfer_num == 0)
		res.json({response: true});
});

app.put('/insert', function(req, res){
	if(kvs[req.body.key] != req.body.value)
		kvs[req.body.key] = req.body.value;
	//console.log("in insert");
	if(kvs[req.body.key] == req.body.value){
		//console.log("insert is good");
		res.json({response: true});
	}
	else{
		//console.log("error message in insert");
		res.json({response: false});
	}
});

app.delete('/remove_node', function(req, res) {
	viewLog.push({
		type: 'remove',
		time: req.body.time,
		node: view[req.body.viewIndex]
	});
	console.log(viewLog);
	view.splice(req.body.viewIndex, 1); // deletes 1 element starting at viewIndex from view[]
	activeNodes--;
	console.log("My partition_view in remove:");
	console.log(partition_view);
	if (my_partition == partition_view[req.body.partIndex].partition_id) {
		for (var i = 0; i < partition.length; i++) {
			if (partition[i].ip == req.body.rem_ip && partition[i].port == req.body.rem_port) {
				partition.splice(i, 1);
				break;
			}
		}
	}
	if (req.body.isPrimary) {
		partition_view[req.body.partIndex].primary.ip = null;
	} else {
		partition_view[req.body.partIndex].backups.splice(req.body.backupIndex, 1);
	}
	console.log("Node removed.");
	console.log("Elements in view[] after removal: "+view.length);
	console.log("Primary in partition after removal: "+partition_view[req.body.partIndex].primary.ip);
	console.log("Elements in backups[] after removal: "+partition_view[req.body.partIndex].backups.length);
	res.json({
		removed: true
	});
});

// Update the view of the new node
app.put('/update_view', function(req, res) {
	console.log("made it to update view");
	view = req.body.view;
	console.log("Updated view: ");
	console.log(view);
	for(var i = 0; i < req.body.partition_view.length; i++){
		console.log("Index in first loop of update_view: "+ i);
		partition_view[i] = {partition_id: req.body.partition_view[i].partition_id,
		                     hash: req.body.partition_view[i].hash,
		                     primary: {ip: req.body.partition_view[i].primary.ip,
		                               port: req.body.partition_view[i].primary.port
		                              },
		                     backups:[]};
		//console.log("inside first loop of update_view: "+ i);
		if(req.body.partition_view[i].primary.ip == ip && req.body.partition_view[i].primary.port == port)
			my_partition = i;
		for(j = 0; j < req.body.partition_view[i].backups.length; j++){
			partition_view[i].backups.push(req.body.partition_view[i].backups[j]);
			//console.log("inside second loop of update_view: " + j);
			if(req.body.partition_view[i].backups[j].ip == ip && req.body.partition_view[i].backups[j].port == port)
				my_partition = i;
		}
	}
	console.log("length of new partition_view: "+ partition_view.length);
	console.log("length of old partition_view: "+ req.body.partition_view.length);
	console.log("length of new view: "+ view.length);
	console.log("length of old view: "+ req.body.view.length);
	partition = viewAssembler.assemblePartition(partition_view, ip);
	//console.log("new node added with partition "+ my_partition);
	console.log("    Active nodes updateview: "+req.body.activeNodes);
	activeNodes = req.body.activeNodes;
	if (view.length == req.body.view.length && partition_view.length == req.body.partition_view.length)
		res.json({response: true});
	else 
		res.json({response: false});
});

//just sends a response back to the node that called
app.get('/heartbeat', function(req,res){
	console.log("my_partition in heartbeat: "+ my_partition);
	res.json({response: my_partition, ip: ip, port: port});
});

//counts the number of available partitions and returns it
app.put('/countActiveParts',  function(req, res){
	console.log("In countactiveaparts");
	var count = 0;
	var responses = [];
	var activeParts = [];
	var counted = false;
	var responded = false;
	var temp = 0;
	var k = 0;
	console.log("activeNodes = "+ req.body.activeNodes)
	broadcaster.broadcast('/heartbeat', 'GET', view, ip_port, {}, function(hunk) { // on response	
		var res_obj = JSON.parse(''+hunk);	
		responses.push(res_obj.response);
		console.log(responses);
		console.log("Is "+responses.length+" >= "+req.body.activeNodes+"?");
		if(responses.length >= req.body.activeNodes){

			for(var i = 0; i < partition_view.length; i++)
			{
				for(var j = 0; j < responses.length; j++){
					if(i == responses[j] && counted == false){
						count++;
						activeParts.push(i);
						break;
					}
				}
				//counted = false;
			}
			if (!responded) {
				responded = true;
				console.log("Count is: "+ count);
				res.json({count: count, partition: my_partition, activeParts: activeParts});
			}
		}
	}, function() { // on timeout
		//responses.push(-1);
		//console.log("TIMEOUT PUSHED");
		//console.log(responses);
	}, true);
	
});

app.get('/kvs/get_partition_id', function(req, res) {
	var part = viewAssembler.find_partition_id(partition_view, ip, port);
	res.status(200);
	res.json({
		msg: 'success',
		partition_id: part
	});
});

app.get('/kvs/get_all_partition_ids', function(req, res) {
	broadcaster.message('/countActiveParts', ip, port, 'PUT', {activeNodes: activeNodes}, function(chunk) { //on response
		var res_obj = JSON.parse(''+chunk);
		var activeParts = res_obj.activeParts;
		res.status(200);
		res.json({
			msg: 'success',
			partition_id_list: activeParts
		});
	}, function() { // on timeout
		res.status(400);
		res.json({
			error: 'service is unavailable'
		});
	});
});

app.get('/kvs/get_partition_members', function(req, res) {
	var url_parts = url_mod.parse(req.url, true);
	var query = url_parts.query;
	var part = query.partition_id;
	var part_members = [];
	if (partition_view[part].primary.ip != null)
		part_members.push(partition_view[part].primary.ip+':'+partition_view[part].primary.port);
	for (var i = partition_view[part].backups.length-1; i >= 0; i--) {
		part_members.push(partition_view[part].backups[i].ip+':'+partition_view[part].backups[i].port);
	}
	res.status(200);
	res.json({
		msg: 'success',
		partition_members: part_members
	});
});

app.listen(port, function() {
	console.log('Listening on port ' + port);
});