/*
 * Node.js / JavaScript Reverse Polish Notation (RPN) Calculator
 *
 * Accepts an single argument in RPN, evaluates it and prints the answer to stdout.
 * 
 */
var stack = [256];
var args = process.argv[2].split(" ");
//console.log("args.length: " + args.length);
var end = 0;
var first;
var second;
var sum;

for(var i = 0; i <args.length - 1; i++){
	//console.log(args[i]);
	var argument = args[i];
	//console.log(argument + "\n");
	if(argument == '+')
	{
		first = parseInt(stack[end-2]);
		second = parseInt(stack[end -1]);
		end -= 2;
		sum = first + second;
		stack[end] = sum;
		//console.log("sum is: " + sum + "\n")
		end++;
	}
	else if(argument == '-')
	{
		first = parseInt(stack[end-2]);
		second = parseInt(stack[end -1]);
		end -= 2;
		sum = first - second;
		stack[end] = sum;
		end++;
	}
	else if(argument == '*')
	{
		first = parseInt(stack[end-2]);
		second = parseInt(stack[end -1]);
		end -= 2;
		sum = first * second;
		stack[end] = sum;
		end++;
	}
	else if(argument == '/')
	{
		first = parseInt(stack[end-2]);
		second = parseInt(stack[end -1]);
		end -= 2;
		sum = first / second;
		stack[end] = sum;
		end++;
	}
	else if(argument == '^')
	{
		first = parseInt(stack[end-2]);
		second = parseInt(stack[end -1]);
		end -= 2;
		sum = 1;
		for(var j = 0; j < second; j++)
		{
			sum = sum * first;
		}
		stack[end] = sum;
		end++;
	}
	else
	{
		//console.log("stack end is: " + argument + "\n");
		stack[end] = argument;
		end++;
	}
	//	console.log(sum);
	
}
console.log(stack[end -1]);