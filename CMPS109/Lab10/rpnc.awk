#
# Awk Reverse Polish Notation (RPN) Calculator
#
# Accepts an single argument in RPN, evaluates it and prints the answer to stdout.
# 
BEGIN {
    argument = ARGV[1]
    n =split(argument, args, " ")
    end = 1
    sum = 0
    for(i = 1; i < n+1; i++){
    	argument = args[i]
    	#print argument
    	if(argument == "+"){
    		first = stack[end-2]
    		second = stack[end-1]
    		end -= 2
    		sum = first + second
    		stack[end] = sum
    		end = end+1
    	}
    	else if(argument == "-"){
    		first = stack[end-2]
    		second = stack[end-1]
    		end -= 2
    		sum = first - second
    		stack[end] = sum
    		end = end+1
    	}
    	else if(argument == "*"){
    		first = stack[end-2]
    		second = stack[end-1]
    		end -= 2
    		sum = first * second
    		# print sum;
    		stack[end] = sum
    		end = end+1
    	}
    	else if(argument == "/"){
    		first = stack[end-2]
    		second = stack[end-1]
    		end -= 2
    		sum = first / second
    		stack[end] = sum
    		end = end+1
    	}
    	else if(argument == "^"){
    		first = stack[end-2]
    		second = stack[end-1]
    		end -= 2
    		sum = 1
    		for(j = 0; j < second; j++){
    			sum = sum * first
    		}
    		stack[end] = sum
    		end = end+1
    	}
    	else{
    		stack[end] = argument;
    		end = end +1
    	}
    	#print end
    }
    print stack[end-1]
}
