//jclouse@ucsc.edu
//Josh Clouse
//CMPS109
//Lab4

//reuleauxtetrahedron.h

#ifndef _REULEAUX_TETRAHEDRON_H_
#define _REULEAUX_TETRAHEDRON_H_

#include "containable.h"
#include "point.h"

class ReuleauxTetrahedron: public Containable3D{
	private:
		Point3D vertices_[4];
	public:
		ReuleauxTetrahedron(Point3D vertices[4]);
		Point3D *vertices();
		double radius();

		bool containedWithin(Sphere &sphere);
		bool containedWithin(Cube &cube);
		bool containedWithin(ReuleauxTetrahedron &rt);
};

#endif