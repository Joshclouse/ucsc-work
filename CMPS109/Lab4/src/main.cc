/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "circle.h"
#include "polygon.h"
#include "reuleauxtriangle.h"
#include "sphere.h"
#include "cube.h"
#include "reuleauxtetrahedron.h"

#define USAGE "USAGE %s test-file\n"


//code in main, getPoint2D, parse, and getRadius taken from lecture
//https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=f1b2136b-19ef-4971-b4c6-504eadc46eac
//https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=2be08498-8350-4514-b8fe-06e265820e39


//code for determinging double found here https://stackoverflow.com/questions/29169153/how-do-i-verify-a-string-is-valid-double-even-if-it-has-a-point-in-it
static bool is_number(const::std::string& s){
	try{
		std::stod(s);
	}
	catch(...){
		return false;
	}
	return true;
}

static double getRadius(std::string &token){
	return std::stod(token);
}

static Point2D get2DPoint(std::string &token){
	double x,y;
	std::stringstream vstream(token);
	std::string value;
	if(std::getline(vstream, value, ',')){
		//std::cout<<"token in getpoint: "<< token<<std::endl;
		if(is_number(value)){
				std::stringstream(value) >> x;
			if(std::getline(vstream, value, ',')){
				//std::cout<<"token in getpoint: "<< token<<std::endl;
				if(is_number(value)){
					std::stringstream(value) >> y;
				
				//std::cout<< "x: " << x << "y: " << y << std::endl;
					return Point2D(x,y);
				}
			}
		}	
	}
	throw "invalid point" + token;
}

static Point3D get3DPoint(std::string &token){
	double x,y,z;
	std::stringstream vstream(token);
	std::string value;
	if(std::getline(vstream, value, ',')){
		if(is_number(value)){
			std::stringstream(value)>>x;
			if(std::getline(vstream, value, ',')){
				if(is_number(value)){
					std::stringstream(value) >> y;
					if(std::getline(vstream, value, ',')){
						if(is_number(value)){
							std::stringstream(value) >> z;
							return Point3D(x,y,z);
						}
					}
				}
			}
		}
	}
	throw "invalid point" + token;
}


static void parse(const std::string line)
{
	std::stringstream stream(line);
	std::string token;
	std::size_t found;
	int boy = 0;
	//creating all possible pointers to shapes ugly but it works and there's not much else I can do
	Circle *ci = NULL;
	Circle *co = NULL;
	RegularConvexPolygon *pi = NULL;
	RegularConvexPolygon *po = NULL;
	ReuleauxTriangle *ri = NULL;
	ReuleauxTriangle *ro = NULL;
	Sphere *si = NULL;
	Sphere *so = NULL;
	Cube *cui = NULL;
	Cube *cuo = NULL;
	ReuleauxTetrahedron *rui = NULL;
	ReuleauxTetrahedron *ruo = NULL;

	bool arena = false;
	bool comment = false;
	bool expected = false;

	while(stream >> token){
		//if the line is a comment just break out of the loop
		found = token.find('#');
		if(found == 0 && boy == 0){
			//std::cout<<"comment"<<std::endl;
			comment = true;
			break;
		}
		//if the token is a circle
		//2D shapes ---------------------------------------------------------------------------------------
		//std::cout<<"token before circle: "<< token<<std::endl;
		if(token == "Circle"){
			//std::cout<<"in circle"<<std::endl;
			stream >> token;
			Point2D center = get2DPoint(token);
			stream >> token;
			double radius = getRadius(token);
			if(arena == false){
				arena = true;
				ci = new Circle(center, radius);
			}
			else{
				//std::cout<<"assigning co"<<std::endl;
				co = new Circle(center, radius);
			}
		}
		//if the token is a polygon
		if(token == "RegularConvexPolygon"){
			//std::cout<<"In regular poly"<<std::endl;
			stream >> token;
			std::vector<Point2D> vertex;
			int position;
			found = token.find(',');
			while(found != std::string::npos){
				//std::cout<<"in the while loop"<<std::endl;
				Point2D vert = get2DPoint(token);
				vertex.push_back(vert);
				position = stream.tellg();
				stream >> token;
				//std::cout<<"token: "<< token<< std::endl;
				found = token.find(',');
				//std::cout<<"found: "<< found<< std::endl;
				//std::cout<<"token2: "<< token<< std::endl;
			}
			//std::cout<<"right after while loop: "<<token <<std::endl;
			//std::cout<<"position: "<< position<<std::endl;
			stream.seekg(position);
			if(arena == false)
			{
				arena = true;
				//std::cout<<"assinging pi"<< std::endl;
				pi = new RegularConvexPolygon(vertex);
				//std::cout<<"token in assign: "<< token<< std::endl;
			}
			else{
				po = new RegularConvexPolygon(vertex);
			}
		}
		//if the token is a ReuleauxTriangle
		if(token == "ReuleauxTriangle"){
			//std::cout<<"in RT"<<std::endl;
			//std::cout<<"first token: "<< token <<std::endl;
			stream >> token;
			if(pi != NULL)
				stream >> token;
			//std::cout<<"first token: "<< token <<std::endl;
			Point2D verts[3];
			int position;
			for(int i = 0; i < 3; i++){
				Point2D vert = get2DPoint(token);
				verts[i] = vert;
				position = stream.tellg();
				stream >> token;
				//std::cout<<"token after loop: "<< token<<std::endl;
			}
			stream.seekg(position);
			if(arena == false){
				arena = true;
				ri = new ReuleauxTriangle(verts);
			}
			else{
				ro = new ReuleauxTriangle(verts);
			}
		}

		//3D shapes -------------------------------------------------------------------------
		//if the token is a sphere
		//std::cout<<"token before sphere "<<token<<std::endl;
		if(token == "Sphere"){
			//std::cout<< "in sphere"<<std::endl;
			stream>>token;
			Point3D center = get3DPoint(token);
			//std::cout<<"after center"<<std::endl;
			stream >> token;
			double radius = getRadius(token);
			//std::cout<<"After radius"<<std::endl;
			if(arena == false){
				//std::cout<<"arena is false"<<std::endl;
				arena = true;
				si = new Sphere(center, radius);
			}
			else{
				//std::cout<<"arena is true"<<std::endl;
				so = new Sphere(center, radius);
				//std::cout<<"arena is true"<<std::endl;
			}
		}
		//If token is a square
		if(token == "Cube"){
			//std::cout<<"in cube"<<std::endl;
			stream >> token;
			Point3D upper[4];
			Point3D lower[4];
			int position;
			for(int i = 0; i < 4; i++){
				Point3D vert = get3DPoint(token);
				//std::cout<<"Z coordinate upper: "<<vert.z<<std::endl;
				upper[i] = vert;
				stream >> token;
			}
			for(int i = 0; i < 4; i++){
				Point3D vert = get3DPoint(token);
				//std::cout<<"Z coordinate lower: "<<vert.z<<std::endl;
				lower[i] = vert;
				//std::cout<<"position1: "<<position<<std::endl;
				//std::cout<<"token0 "<<token<<std::endl;
				position = stream.tellg();
				//std::cout<<"position2: "<<position<<std::endl;
				stream >> token;
				//std::cout<<"Position of token "<<stream.tellg()<<std::endl;
				//std::cout<<token<<std::endl;
				
			}
			//std::cout<<"token1: "<<token<<std::endl;
			stream.seekg(position);
			//std::cout <<"Position of token2 "<<stream.tellg()<<std::endl;
			//std::cout<<"token2: "<<token<<std::endl;
			//position = stream.tellg();
			//std::cout<<"position3: "<<position<<std::endl;
			if(arena == false){
				arena = true;
				cui = new Cube(upper, lower);
			}
			else{
				cuo = new Cube(upper, lower);
			}
			//std::cout<<"token at the end of cube: "<< token<<std::endl;
		}
		if(token == "ReuleauxTetrahedron"){
			stream>>token;
			Point3D verts[4];
			int position;
			for(int i = 0; i < 4; i++){
				verts[i] = get3DPoint(token);
				position = stream.tellg();
				stream >> token;
			}
			stream.seekg(position);
			if(arena == false){
				arena = true;
				rui = new ReuleauxTetrahedron(verts);
			}
			else
				ruo = new ReuleauxTetrahedron(verts);
		}
		
		//after you have taken in both shapes look for expected result
		if(token == "true"){
			if(co == NULL && so == NULL)
				stream >> token;
			//std::cout<<"hit the true"<<std::endl;
			expected = true;
			break;
		}
		if(token == "false"){
			if(co == NULL && so == NULL)
				stream>>token;
			//std::cout<<"hit false"<<std::endl;
			break;
		}
		//std::cout<<"token at the end of loop: "<<token<<std::endl;
		boy++;
		
	}
	
	if(comment == false){
		while(stream >> token){
			//std::cout<<"tripping"<<std::endl;
			std::cout << token<< " ";
		}
		//2D Shapes-------------------------------------------------------------------------------
		//If circle is inside
		if(ci != NULL){
			if(co != NULL){
				bool got = ci->containedWithin(*co);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			else if(po != NULL){
				bool got = ci->containedWithin(*po);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			else if(ro != NULL){
				bool got = ci->containedWithin(*ro);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}
		//If polygon is inside
		else if(pi != NULL){
			if(co != NULL){
				bool got = pi->containedWithin(*co);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			else if(po != NULL){
				bool got = pi->containedWithin(*po);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(ro != NULL){
				bool got = pi->containedWithin(*ro);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}
		//If RT is inside
		else if(ri != NULL){
			if(co != NULL){
				bool got = ri->containedWithin(*co);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			else if(po != NULL){
				bool got = ri->containedWithin(*po);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			else if(ro != NULL){
				bool got = ri->containedWithin(*ro);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}

		//3D Shapes -------------------------------------------------------------------------------
		if(si != NULL){
			if(so != NULL){
				bool got = si->containedWithin(*so);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(cuo != NULL){
				bool got = si->containedWithin(*cuo);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(ruo != NULL){
				bool got = si->containedWithin(*ruo);
				std::cout<< (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}
		if(cui != NULL){
			//std::cout<<"in cui conditional"<<std::endl;
			if(so != NULL){
				//std::cout<<"in so conditional"<<std::endl;
				bool got = cui->containedWithin(*so);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(cuo != NULL){
				bool got = cui->containedWithin(*cuo);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(ruo != NULL){
				bool got = cui->containedWithin(*ruo);
				std::cout<< (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}
		
		if(rui != NULL){
			if(so != NULL){
				bool got = rui->containedWithin(*so);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(cuo != NULL){
				bool got = rui->containedWithin(*cuo);
				std::cout << (got == expected ? "PASS" : "FAIL") << std::endl;
			}
			if(ruo != NULL){
				bool got = rui->containedWithin(*ruo);
				std::cout<< (got == expected ? "PASS" : "FAIL") << std::endl;
			}
		}
		
	}
}

int main(int argc, char *argv[]) 
{

    if (argc < 2) {
        printf(USAGE, argv[0]);
        return -1;
    }
    
    std::cout << "Test file: " << argv[1] << std::endl;
    std::ifstream ifs(argv[1]);
    if(ifs){
    	std::string line;
    	while(std::getline(ifs, line)){
    		parse(line);
    	}
    }
}
