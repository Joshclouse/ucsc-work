//jclouse@ucsc.edu
//Josh Clouse
//CMPS109
//Lab4

//reuleauxtetrahedron.cc

#include<iostream>

#include "cube.h"
#include "sphere.h"
#include "reuleauxtetrahedron.h"
#include "geom.h"

ReuleauxTetrahedron::ReuleauxTetrahedron(Point3D vertices[4]){
	for(int i = 0; i < 4; i++){
		vertices_[i] = vertices[i];
	}
}

Point3D *ReuleauxTetrahedron::vertices(){
	return vertices_;
}

double ReuleauxTetrahedron::radius(){
	return Geom::distance(vertices_[0], vertices_[1]);
}
bool ReuleauxTetrahedron::containedWithin(Sphere &sphere){
	for(int i = 0; i < 4; i++){
		if(Geom::distance(vertices_[i], sphere.center()) > sphere.radius())
			return false;
	}
	if(sphere.radius() <= radius())
		return true;
	else
		for(int i = 0; i < 4; i++){
			Sphere s = Sphere(vertices_[i], radius());
			if(s.containedWithin(sphere))
				return true;
		}
	return false;
}
bool ReuleauxTetrahedron::containedWithin(Cube &cube){
	return true;
}
bool ReuleauxTetrahedron::containedWithin(ReuleauxTetrahedron &rt){
	//std::cout<<"in RT"<<std::endl;
	Point3D *verts = rt.vertices();
	for(int i = 0; i < 4; i++){
		//std::cout<<"first loop"<<std::endl;
		for(int j = 0; j < 4; j++){
			std::cout<<"second loop"<<std::endl;
			if(Geom::distance(verts[i], vertices_[i]) > rt.radius())
				return false;
		}
	}
	for(int i = 0; i < 4; i++){
		//std::cout<<"thirdloop"<<std::endl;
		for(int j = 0; j < 4; j++){
			//std::cout<<"fourth loop"<<std::endl;
			Sphere big = Sphere(verts[i], rt.radius());
			Sphere small = Sphere(vertices_[i], radius());
			if(small.containedWithin(big))
				break;
			if(j==3)
				return false;
		}
	}
	return true;
}