//jclouse@ucsc.edu
//Josh Clouse
//CMPS109
//Lab 4

//cube.cc

#include <iostream>

#include "cube.h"
#include "sphere.h"
#include "reuleauxtetrahedron.h"
#include "geom.h"

Cube::Cube(Point3D upperFace[4], Point3D lowerFace[4]){
	for(int i = 0; i < 4; i++){
		upperFace_[i] = upperFace[i];
		lowerFace_[i] = lowerFace[i];
	}
}

Point3D *Cube::upperFace(){
	return upperFace_;
}

Point3D *Cube::lowerFace(){
	return lowerFace_;
}

bool Cube::containedWithin(Sphere &sphere){
	//std::cout<<"in contained in cube"<<std::endl;
	Point3D center = sphere.center();
	double radius = sphere.radius();
	bool xz = false;
	std::vector<Point2D> verts;
	for(int i = 0; i < 4; i++){
		verts.push_back(Point2D(upperFace_[i].x, upperFace_[i].y));
	}
	RegularConvexPolygon square = RegularConvexPolygon(verts);
	Circle circle = Circle(Point2D(center.x, center.y), radius);
	if(square.containedWithin(circle)== false){
		//std::cout<<"got fucked here"<<std::endl;
		return false;
	}
	//checks to see if coordinates were given clockwise or counter clockwise
	//std::cout<<"upper.x: "<<upper[0].x<< " "<<upper[1].x<<std::endl;
	if(upperFace_[0].y == upperFace_[1].y){

		xz = true;
	}
	//sets the top two points of the square
	for(int i = 0; i < 2; i++)
	{
		//if we want to get rid of the Y and only use x and z
		if(xz)
			verts[i] = Point2D(upperFace_[i].x, upperFace_[i].z);
		//if we cant to get rid of the x and only use y and z
		else
			verts[i] = Point2D(upperFace_[i].y, upperFace_[i].z);
	}
	//sets the bottom two points of the sqaure
	int j = 0;
	for(int i = 1; i >=0; i--){
		//if we want to get rid of the Y and only use x and z
		if(xz)
			verts[j+2] = Point2D(lowerFace_[i].x, lowerFace_[i].z);
		//if we want to get rid of the x and only use y and z
		else
			verts[j+2] = Point2D(lowerFace_[i].y, lowerFace_[i].z);
		j++;
	}
	
	for(int i = 0; i < 4; i++){
		//std::cout<<"coordinate: "<<verts[i].x<<","<<verts[i].y<<std::endl;
	}
	
	//sets the new center of the circle squished on either the x or y axis
	if(xz)
		circle.setCenter(Point2D(center.x, center.z));
	else
		circle.setCenter(Point2D(center.y, center.z));
	//std::cout<<"circle center: "<<circle.center().x<<","<<circle.center().y<<std::endl;
	square = RegularConvexPolygon(verts);
	if(square.containedWithin(circle)== false){
		//std::cout<<"got fucked here 2"<<std::endl;
		return false;
	}

	return true;
}

bool Cube::containedWithin(Cube &cube){
	Point3D *upper = cube.upperFace();
	Point3D *lower = cube.lowerFace();
	bool xzBig = false;
	bool xzSmall = false;
	std::vector<Point2D> arenaVerts;
	std::vector<Point2D> shapeVerts;
	for(int i= 0; i < 4; i++){
		arenaVerts.push_back(Point2D(upper[i].x, upper[i].y));
		shapeVerts.push_back(Point2D(upperFace_[i].x, upperFace_[i].y));
	}
	RegularConvexPolygon arena = RegularConvexPolygon(arenaVerts);
	RegularConvexPolygon shape = RegularConvexPolygon(shapeVerts);
	if(shape.containedWithin(arena)== false)
		return false;
	if(upper[0].y == upper[1].y){

		xzBig = true;
	}
	if(upperFace_[0].y == upperFace_[1].y){
		xzSmall = true;
	}
	for(int i = 0; i < 2; i++)
	{
		//if we want to get rid of the Y and only use x and z
		if(xzBig)
			arenaVerts[i] = Point2D(upper[i].x, upper[i].z);
		//if we cant to get rid of the x and only use y and z
		else
			arenaVerts[i] = Point2D(upper[i].y, upper[i].z);

		if(xzSmall)
			shapeVerts[i] = Point2D(upperFace_[i].x, upperFace_[i].z);
		else
			shapeVerts[i] = Point2D(upperFace_[i].y, upperFace_[i].z);
	}
	//sets the bottom two points of the sqaure
	int j = 0;
	for(int i = 1; i >=0; i--){
		//if we want to get rid of the Y and only use x and z
		if(xzBig)
			arenaVerts[j+2] = Point2D(lower[i].x, lower[i].z);
		//if we want to get rid of the x and only use y and z
		else
			arenaVerts[j+2] = Point2D(lower[i].y, lower[i].z);
		if(xzSmall)
			shapeVerts[j+2] = Point2D(lowerFace_[i].x, lowerFace_[i].z);
		else
			shapeVerts[j+2] = Point2D(lowerFace_[i].y, lowerFace_[i].z);
		j++;
	}
	arena = RegularConvexPolygon(arenaVerts);
	shape = RegularConvexPolygon(shapeVerts);
	if(shape.containedWithin(arena)== false)
		return false;
	return true;
}

bool Cube::containedWithin(ReuleauxTetrahedron &rt){
	//not yet implemented
	return false;
}