//jclouse@ucsc.edu
//Josh clouse
//CMPS109
//Lab4
//sphere.cc


#include <iostream>
#include <vector>

#include "sphere.h"
#include "cube.h"
#include "geom.h"
#include "circle.h"
#include "polygon.h"
#include "reuleauxtetrahedron.h"

Sphere::Sphere(Point3D center, double radius){
	setCenter(center);
	setRadius(radius);
}

Point3D Sphere::center(){
	return center_;
}

void Sphere::setCenter(Point3D center){
	center_.x = center.x;
	center_.y = center.y;
	center_.z = center.z;
}

double Sphere::radius(){
	return radius_;
}

void Sphere::setRadius(double radius){
	radius_= radius;
}

bool Sphere::containedWithin(Sphere &sphere){
	double distance = Geom::distance(center(), sphere.center());
	return (distance <= (sphere.radius() - radius()));
}

bool Sphere::containedWithin(Cube &cube){
	Point3D *upper = cube.upperFace();
	Point3D *lower = cube.lowerFace();
	bool xz = false;
	std::vector<Point2D> verts;
	for(int i = 0; i < 4; i++){
		verts.push_back(Point2D(upper[i].x, upper[i].y));
	}
	RegularConvexPolygon square = RegularConvexPolygon(verts);
	Circle circle = Circle(Point2D(center_.x, center_.y), radius_);
	if(circle.containedWithin(square)== false){
		//std::cout<<"got fucked here"<<std::endl;
		return false;
	}
	//checks to see if coordinates were given clockwise or counter clockwise
	//std::cout<<"upper.x: "<<upper[0].x<< " "<<upper[1].x<<std::endl;
	if(upper[0].y == upper[1].y){

		xz = true;
	}
	//sets the top two points of the square
	for(int i = 0; i < 2; i++)
	{
		//if we want to get rid of the Y and only use x and z
		if(xz)
			verts[i] = Point2D(upper[i].x, upper[i].z);
		//if we cant to get rid of the x and only use y and z
		else
			verts[i] = Point2D(upper[i].y, upper[i].z);
	}
	//sets the bottom two points of the sqaure
	int j = 0;
	for(int i = 1; i >=0; i--){
		//if we want to get rid of the Y and only use x and z
		if(xz)
			verts[j+2] = Point2D(lower[i].x, lower[i].z);
		//if we want to get rid of the x and only use y and z
		else
			verts[j+2] = Point2D(lower[i].y, lower[i].z);
		j++;
	}
	
	for(int i = 0; i < 4; i++){
		//std::cout<<"coordinate: "<<verts[i].x<<","<<verts[i].y<<std::endl;
	}
	
	//sets the new center of the circle squished on either the x or y axis
	if(xz)
		circle.setCenter(Point2D(center_.x, center_.z));
	else
		circle.setCenter(Point2D(center_.y, center_.z));
	//std::cout<<"circle center: "<<circle.center().x<<","<<circle.center().y<<std::endl;
	square = RegularConvexPolygon(verts);
	if(circle.containedWithin(square)== false){
		//std::cout<<"got fucked here 2"<<std::endl;
		return false;
	}

	return true;
}

bool Sphere::containedWithin(ReuleauxTetrahedron &rt){
	Point3D *verts = rt.vertices();
	for(int i = 0; i < 4; i++){
		if(Geom::distance(center(), verts[i]) > rt.radius() - radius())
			return false;
	}
	return true;
}