//jclouse@ucsc.edu
//CMPS109
//LAB 3
//reuleaxtraingle.h

#ifndef _REULEAUX_TRIANGLE_H_
#define _REULEAUX_TRIANGLE_H_


#include <vector>


#include "containable.h"
#include "point.h"
#include "circle.h"
#include "line.h"



class ReuleauxTriangle : public Containable2D{
	private:
		Point2D vertices_[3];
	public:
		ReuleauxTriangle(Point2D vertices[3]);
		Point2D *vertices();
		double radius();
		Point2D Center();

		bool containedWithin(Circle &circle);
        bool containedWithin(RegularConvexPolygon &polygon);
        bool containedWithin(ReuleauxTriangle &rt);		
};

#endif