/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include <iostream>
#include <string>

#include "circle.h"
#include "polygon.h"
#include "reuleauxtriangle.h"



//code for the basic layout of tests and ouput taken from lecture: https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=956436f9-b748-4ab7-b7b7-2e27605f1667
static void banana(const char *msg, bool expected, bool got)
{
	std::cout << msg;
	if (expected == got) 
        std::cout << "PASS\n";
    else 
        std::cout << "FAIL\n";
}

//SHAPE IN CIRCLE TESTS----------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
static void testCircleInCircle()
{
 	Circle inner = Circle(Point2D(0.0,0.0), 4.0);
    Circle outer = Circle(Point2D(0.0,0.0), 8.0); 
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer)); 
    inner = Circle(Point2D(2.0,0.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));
    inner = Circle(Point2D(2.0,2.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));
    inner = Circle(Point2D(-2.0,0.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));    
    inner = Circle(Point2D(2.0,-2.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));  
    inner = Circle(Point2D(-2.0,2.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));  
    inner = Circle(Point2D(-2.0,-2.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));
    inner = Circle(Point2D(4.0,0.0), 4.0);
    banana("Circle-in-Circle: ", true, inner.containedWithin(outer));    
    inner = Circle(Point2D(4.0001,0.0), 4.0);
    banana("Circle-in-Circle: ", false, inner.containedWithin(outer));  
    inner = Circle(Point2D(30.0,0.0), 4.0);
    banana("Circle-in-Circle: ", false, inner.containedWithin(outer)); 
    inner = Circle(Point2D(9.0,0.0), 4.0);
    banana("Circle-in-Circle: ", false, inner.containedWithin(outer));   
        
}

static void testTriangleInCircle()
{
	Circle outer = Circle(Point2D(0.0,0.0), 8.0); 
	RegularConvexPolygon inner = 
    	RegularConvexPolygon({Point2D(-1,-1), Point2D(0,2), Point2D(1,-1)});
	 banana("Triangle-in-Circle: ", true, inner.containedWithin(outer)); 

	inner = 
    	RegularConvexPolygon({Point2D(-8,0), Point2D(0,2), Point2D(1,-1)});
    banana("Triangle-in-Circle: ", true, inner.containedWithin(outer)); 

    inner = 
    	RegularConvexPolygon({Point2D(-8,0), Point2D(8,0), Point2D(0,8)});
    banana("Triangle-in-Circle: ", true, inner.containedWithin(outer)); 

    inner = 
    	RegularConvexPolygon({Point2D(-9,0), Point2D(0,2), Point2D(1,-1)});
    banana("Triangle-in-Circle: ", false, inner.containedWithin(outer)); 

    inner = 
    	RegularConvexPolygon({Point2D(-9,-10), Point2D(-20,-9), Point2D(-12,-12)});
    banana("Triangle-in-Circle: ", false, inner.containedWithin(outer)); 

	inner = 
	    	RegularConvexPolygon({Point2D(-8.0001,0), Point2D(8,0), Point2D(0,8)});
	    banana("Triangle-in-Circle: ", false, inner.containedWithin(outer)); 

}

static void testRectangleInCircle()
{
	Circle outer = Circle(Point2D(0.0,0.0), 8.0); 
	RegularConvexPolygon inner = 
		RegularConvexPolygon({Point2D(2,2), Point2D(2,-2), Point2D(-2,-2), Point2D(-2,2)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));
	

	inner = 
		RegularConvexPolygon({Point2D(4,4), Point2D(4,0), Point2D(0,0), Point2D(0,4)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(4,-4), Point2D(4,0), Point2D(0,0), Point2D(0,-4)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(-4,4), Point2D(-4,0), Point2D(0,0), Point2D(0,4)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(-4,-4), Point2D(-4,0), Point2D(0,0), Point2D(0,-4)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(8,0), Point2D(0,8), Point2D(-8,0), Point2D(0,-8)});
	banana("Rectangle-in-Circle: ", true, inner.containedWithin(outer));
	

	inner = 
		RegularConvexPolygon({Point2D(8.001,0), Point2D(0,8.001), Point2D(-8.001,0), Point2D(0,-8.001)});
	banana("Rectangle-in-Circle: ", false, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(2,4), Point2D(6,4), Point2D(6,8), Point2D(4,8)});
	banana("Rectangle-in-Circle: ", false, inner.containedWithin(outer));

	inner = 
			RegularConvexPolygon({Point2D(8,4), Point2D(12,4), Point2D(12,8), Point2D(8,8)});
		banana("Rectangle-in-Circle: ", false, inner.containedWithin(outer));

}
static void testRTInCircle(){
	Circle outer = Circle(Point2D(0.0,0.0), 8.0); 

	Point2D points[3] = {Point2D(0,6.928204), Point2D(6,-3.46410161514), Point2D(-6,-3.46410161514)};
	ReuleauxTriangle inner =
	ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
	points[0] = Point2D(0,4.928204);
	points[1] = Point2D(6,-5.46410161514);
	points[2] = Point2D(-6,-5.46410161514); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", false, inner.containedWithin(outer));
	points[0] = Point2D(0,7.928204);
	points[1] = Point2D(6,-2.46410161514);
	points[2] = Point2D(-6,-2.46410161514); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
	points[0] = Point2D(0,8);
	points[1] = Point2D(6,-2.39230561514);
	points[2] = Point2D(-6,-2.39230561514); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
	points[0] = Point2D(0,20);
	points[1] = Point2D(6,10.39230561514);
	points[2] = Point2D(-6,10.39230561514); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", false, inner.containedWithin(outer));
	points[0] = Point2D(0,6.928204);
	points[1] = Point2D(6,-3.46410161514);
	points[2] = Point2D(-6,-3.46410161514); 
	inner =
		ReuleauxTriangle(points);
	outer = Circle(Point2D(0.0,0.0), 2);
	banana("RT-in-Circle: ", false, inner.containedWithin(outer));
}

//SHAPE IN POLYGON TESTS------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------
static void testCircleInRectangle()
{
	RegularConvexPolygon outer = 
		RegularConvexPolygon({Point2D(-8,8), Point2D(8,8), Point2D(8,-8), Point2D(-8,-8)});

	Circle inner = Circle(Point2D(0.0,0.0), 8.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer)); 

	inner = Circle(Point2D(0.0,0.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(2.0,2.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(2.0,-2.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(-2.0,2.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(-2.0,-2.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(2.0,2.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(4.0,4.0), 4.0);
	banana("Circle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = Circle(Point2D(5.0,4.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(4.0,5.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(-4.0,5.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(-5.0,4.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(-4.0,-5.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(20.0,20.0), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = Circle(Point2D(4.0,4.0001), 4.0);
	banana("Circle-in-Rectangle: ", false, inner.containedWithin(outer));
}

static void testRectangleInRectangle(){
	RegularConvexPolygon outer = 
		RegularConvexPolygon({Point2D(-8,8), Point2D(8,8), Point2D(8,-8), Point2D(-8,-8)});

	RegularConvexPolygon inner = 
		RegularConvexPolygon({Point2D(2,2), Point2D(2,-2), Point2D(-2,-2), Point2D(-2,2)});
	banana("Rectangle-in-Rectangle: ", true, inner.containedWithin(outer));

	inner = 
		RegularConvexPolygon({Point2D(6,6), Point2D(8,6), Point2D(8,4), Point2D(6,4)});
	banana("Rectangle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = 
		RegularConvexPolygon({Point2D(7,6), Point2D(9,6), Point2D(9,4), Point2D(7,4)});
	banana("Rectangle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = 
		RegularConvexPolygon({Point2D(6,12), Point2D(8,12), Point2D(8,10), Point2D(6,10)});
	banana("Rectangle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = 
		RegularConvexPolygon({Point2D(-9,9), Point2D(9,9), Point2D(9,-9), Point2D(-9,-9)});
	banana("Rectangle-in-Rectangle: ", false, inner.containedWithin(outer));
	inner = 
		RegularConvexPolygon({Point2D(-8,8), Point2D(8,8), Point2D(8,-8), Point2D(-8,-8)});
	banana("Rectangle-in-Rectangle: ", true, inner.containedWithin(outer));
	inner = 
		RegularConvexPolygon({Point2D(2,10), Point2D(6,10), Point2D(6,6), Point2D(2,6)});
	banana("Rectangle-in-Rectangle: ", false, inner.containedWithin(outer));


}
static void testRTInRectangle(){
	RegularConvexPolygon outer = 
		RegularConvexPolygon({Point2D(-8,8), Point2D(8,8), Point2D(8,-8), Point2D(-8,-8)});

	Point2D points[3] = {Point2D(0,6.928204), Point2D(6,-3.464102), Point2D(-6,-3.464102)};
	ReuleauxTriangle inner =
	ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
	points[0] = Point2D(0,4.928204);
	points[1] = Point2D(6,-5.464102);
	points[2] = Point2D(-6,-5.464102); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
	points[0] = Point2D(0,8.928204);
	points[1] = Point2D(6,-1.464102);
	points[2] = Point2D(-6,-1.464102); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", false, inner.containedWithin(outer));
	points[0] = Point2D(0,8);
	points[1] = Point2D(6,-2.392306);
	points[2] = Point2D(-6,-2.392306); 
	inner =
		ReuleauxTriangle(points);
	banana("RT-in-Circle: ", true, inner.containedWithin(outer));
}

//SHAPE IN RT TESTS----------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------
static void testCircleInRT(){
	Point2D points[3] = {Point2D(0,6.928204), Point2D(6,-3.46410161514), Point2D(-6,-3.46410161514)};
	ReuleauxTriangle outer =
		ReuleauxTriangle(points);

	Circle inner =
		Circle(Point2D(0.0,0), 2);
	banana("Circle-in-RT: ", true, inner.containedWithin(outer));
	inner =
		Circle(Point2D(0.0,0), 3);
	banana("Circle-in-RT: ", true, inner.containedWithin(outer));
	inner =
		Circle(Point2D(0.0,0), 4);
	banana("Circle-in-RT: ", true, inner.containedWithin(outer));
	inner =
		Circle(Point2D(0.0,0), 5);
	banana("Circle-in-RT: ", true, inner.containedWithin(outer));
	inner =
		Circle(Point2D(0.0,0), 6);
	banana("Circle-in-RT: ", false, inner.containedWithin(outer));
	inner =
		Circle(Point2D(2,2), 3);
	banana("Circle-in-RT: ", false, inner.containedWithin(outer));
	inner =
		Circle(Point2D(20,20), 3);
	banana("Circle-in-RT: ", false, inner.containedWithin(outer));
	inner =
			Circle(Point2D(20,20), 3);
		banana("Circle-in-RT: ", false, inner.containedWithin(outer));

}
static void testPolyInRT(){
	Point2D points[3] = {Point2D(0,6.928204), Point2D(6,-3.46410161514), Point2D(-6,-3.46410161514)};
	ReuleauxTriangle outer =
		ReuleauxTriangle(points);

	RegularConvexPolygon inner =
		RegularConvexPolygon({Point2D(2,2), Point2D(2,-2), Point2D(-2,-2), Point2D(-2,2)});
	banana("Poly-in-RT: ", true, inner.containedWithin(outer));
	inner =
		RegularConvexPolygon({Point2D(2,4), Point2D(2,-2), Point2D(-2,-2), Point2D(-2,4)});
	banana("Poly-in-RT: ", true, inner.containedWithin(outer));
	inner =
		RegularConvexPolygon({Point2D(2,6), Point2D(2,-2), Point2D(-2,-2), Point2D(-2,6)});
	banana("Poly-in-RT: ", false, inner.containedWithin(outer));
	inner =
		RegularConvexPolygon({Point2D(2,0), Point2D(2,-4), Point2D(-2,-4), Point2D(-2,0)});
	banana("Poly-in-RT: ", true, inner.containedWithin(outer));
	inner =
		RegularConvexPolygon({Point2D(8,6), Point2D(8,2), Point2D(4,2), Point2D(4,6)});
	banana("Poly-in-RT: ", false, inner.containedWithin(outer));
}
static void testRTInRT(){
	Point2D points[3] = {Point2D(0,6.928204), Point2D(6,-3.46410161514), Point2D(-6,-3.46410161514)};
	ReuleauxTriangle outer =
		ReuleauxTriangle(points);

	Point2D joints[3] = {Point2D(0,3.47), Point2D(-2,0), Point2D(2,0)};
	ReuleauxTriangle inner =
		ReuleauxTriangle(joints);
	banana("RT-in-RT: ", true, inner.containedWithin(outer));
	joints[0] = Point2D(0,4.47);
	joints[1] = Point2D(-2,1);
	joints[2] = Point2D(2,1); 
	inner =
		ReuleauxTriangle(joints);
	banana("RT-in-RT: ", true, inner.containedWithin(outer));
	joints[0] = Point2D(4,4.47);
	joints[1] = Point2D(2,1);
	joints[2] = Point2D(6,1); 
	inner =
		ReuleauxTriangle(joints);
	banana("RT-in-RT: ", false, inner.containedWithin(outer));
	joints[0] = Point2D(40,4.47);
	joints[1] = Point2D(38,1);
	joints[2] = Point2D(42,1); 
	inner =
		ReuleauxTriangle(joints);
	banana("RT-in-RT: ", false, inner.containedWithin(outer));
}
int main(int argc, char *argv[]) 
{
    //testCircleInCircle();
    //testTriangleInCircle();
    //testRectangleInCircle();
    //testCircleInRectangle();
    //testRectangleInRectangle();
    //testCircleInRT();
    //testPolyInRT();
    //testRTInRT();
    //testRTInCircle();
    testRTInRectangle();
}
