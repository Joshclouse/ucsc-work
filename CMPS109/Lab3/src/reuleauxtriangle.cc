//jclouse@ucsc.edu
//CMPS109
//LAB 2
//reuleauxtriangle.cc


#include <iostream>

#include "reuleauxtriangle.h"
#include "polygon.h"
#include "circle.h"
#include "geom.h"


ReuleauxTriangle::ReuleauxTriangle(Point2D vertices[3]){
	for(int i = 0; i < 3; i++){
		vertices_[i] = vertices[i];
	}
}

Point2D *ReuleauxTriangle::vertices(){
	return vertices_;
}

double ReuleauxTriangle::radius(){
	return Geom::distance(vertices_[0], vertices_[1]);
}
Point2D ReuleauxTriangle::Center(){
	double x=0.0;
	double y=0.0;
	for(int i = 0; i < 3; i++){
		x += vertices_[i].x;
		y += vertices_[i].y;
	}
	return Point2D((x/3.0),(y/3.0));
}

bool ReuleauxTriangle::containedWithin(Circle &circle){
	for(int i = 0; i < 3; i++)
	{
		if(Geom::distance(vertices_[i], circle.center()) > circle.radius())
			return false;
	}
	if(circle.radius() <= radius())
		return true;
	else
		for(int i =0; i < 3; i++){
			Circle rt = Circle(vertices_[i], radius());
			if(rt.containedWithin(circle))
				return true;
		}
	return false;
}

bool ReuleauxTriangle::containedWithin(RegularConvexPolygon &polygon){
	std::vector<Line> arenaEdges = polygon.edges();
	std::vector<Point2D> areanVerts = polygon.vertices();
	int count = 0;
	Point2D center = Center();
	std::cout << center.x << "x " << center.y << "y\n";
	Circle circle = Circle(Point2D(0.0,0.0),0);
	for(Line const &line: arenaEdges){
		count = 0;
		for(int i = 0; i < 3; i++){
			circle = Circle(Point2D(vertices_[i]), radius());
			if(Geom::intersects(line, circle))
				count++;
		}
		if(count == 3){
			if((abs((center.y - line.a.y) * (line.b.x - line.a.x) - 
        		(center.x -  line.a.x) * (line.b.y - line.a.y)) / 
            	Geom::distance(line.a, line.b)) < Geom::distance(center, vertices_[0])){
            	return false;
        	}
		}
	}
	return true;
}

bool ReuleauxTriangle::containedWithin(ReuleauxTriangle &rt){
	Point2D *RTVert = rt.vertices();
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			if(Geom::distance(RTVert[i], vertices_[j]) > rt.radius())
				return false;
		}
	}
	for(int i = 0; i < 3; i++){
		for(int j = 0; i < 3; j++){
			Circle big = Circle(RTVert[i], rt.radius());
			Circle small = Circle(vertices_[j], radius());
			if(small.containedWithin(big))
				break;
			if(j == 3)
				return false;
		}
	}
	return true;
}
