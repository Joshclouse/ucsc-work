/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include <iostream>

#include "polygon.h"
#include "circle.h"
#include "reuleauxtriangle.h"
#include "geom.h"


RegularConvexPolygon::RegularConvexPolygon(std::vector<Point2D> vertices) {
    vertices_ = vertices;
}

std::vector<Point2D> RegularConvexPolygon::vertices() {
    return vertices_;
}

std::vector<Line> RegularConvexPolygon::edges() {
    std::vector<Line> edges;
    for (unsigned int i = 0; i < vertices_.size()-1; i++) 
        edges.push_back(Line(vertices_[i],vertices_[i+1]));
    edges.push_back(Line(vertices_[vertices_.size()-1],vertices_[0]));
    return edges;
}

int RegularConvexPolygon::vertexCount() {
    return vertices_.size();
}

bool RegularConvexPolygon::containedWithin(Circle &circle) {
    for(Point2D const &vertex : vertices()){
        if(Geom::distance(vertex, circle.center()) > circle.radius())
            return false;
    }
    return true;
}

bool RegularConvexPolygon::containedWithin(RegularConvexPolygon &polygon) {
    std::vector<Point2D> arenaVert = polygon.vertices();
    int i, j, c = 0;
    int n = polygon.vertexCount();
    bool duplicate = false;
    //code for calculating point inside a polygon taken from here https://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
    for(Point2D const &point: vertices()){
        duplicate = false;
        i = 0;
        j = 0;
        c = 0;
        for(i = 0, j = n-1; i < n; j=i++){
            if(((arenaVert[i].y >= point.y) != (arenaVert[j].y >= point.y))
                && (point.x <= (arenaVert[j].x - arenaVert[i].x) * (arenaVert[i].y)/(arenaVert[j].y - arenaVert[i].y)+ arenaVert[i].x))
                c = !c;
            //std::cout<< c << "\n" ;
            if(arenaVert[i].y == point.y && arenaVert[i].x == point.x)
                duplicate = true;
        }
        if(c %2 == 0 && duplicate == false){
            //std::cout <<"You fucked up\n";
            return false;
        }
    }
    return true;
    
}

bool RegularConvexPolygon::containedWithin(ReuleauxTriangle &rt) {
    Point2D *RTVert = rt.vertices();
    for(Point2D const &point: vertices()){
        for(int i = 0; i < 3; i++){
            if(Geom::distance(point, RTVert[i]) > rt.radius())
                return false;
        }
    }
    return true;
}
