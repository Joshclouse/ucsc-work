/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

//Josh clouse
//jclouse@ucsc.edu
//CMPS109
//Lab 8

#include "radix.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <string>
#include <iostream>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <strings.h>
#include <string>

/*
 * Start a UDP listener on PORT and accept lists of unsiged integers from
 * clients to be MSD RAdix sorted using no more that CORES cpu cores. After
 * sorting the lists are to be retiurned to the client.
 */


//psuedo code taken from lecture for server and client https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=53213100-10ad-49a3-9ff5-c3bcf3d3d478
void RadixServer::start(const int port, const unsigned int cores) {
	struct timeval tv;
	fd_set readfds;
	tv.tv_sec = 2;
	tv.tv_usec = 0;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0) exit(-1);

    struct sockaddr_in server_addr;
    bzero((char*)&server_addr, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);

    if(bind(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
    	exit(-1);

    struct sockaddr_in remote_addr;
    socklen_t len = sizeof(remote_addr);

    std::vector<unsigned int> list;
    Message received;
    Message send;
    Message temp;
    unsigned int amountReceived = 0;
    //bool done = false;
    int n;

    for(;;){
    	FD_SET(sockfd, &readfds);
    	tv.tv_sec = 2;
		tv.tv_usec = 0;
    	int rc = select(sockfd + 1, &readfds, 0, 0, &tv);
    	if(rc == 0){
			send.flag = RESEND;
			send.sequence = 0;
			send.num_values = received.sequence+1;
			for(unsigned int i = 0; i < send.num_values; i++){
				send.values[i] = i;
			}
			temp.sequence = htonl(send.sequence);
			temp.flag = htonl(send.flag);
			temp.num_values = htonl(send.num_values);
			for(unsigned int j = 0; j < send.num_values; j++){
				temp.values[j] = htonl(send.values[j]);
			}
			//std::cout<<"sending resend message to server"<<std::endl;
			n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
			list.clear();
			amountReceived = 0;
			continue;
    	}
    	n = recvfrom(sockfd, &received, sizeof(Message), 0, (struct sockaddr *)&remote_addr, &len);
    	if(n < 0) exit(-1);
    	received.sequence = ntohl(received.sequence);
		received.num_values = ntohl(received.num_values);
		received.flag = ntohl(received.flag);
		for(unsigned int i = 0; i < received.num_values; i++){
			received.values[i] = ntohl(received.values[i]);
			//std::cout<<"recieved value from client: "<<received.values[i]<<"at index: "<<i<<std::endl;
		}
		if(received.flag != RESEND){
			amountReceived++;
			for(unsigned int i = 0; i < received.num_values; i++){
				//std::cout<<"pushing back: "<<i<<std::endl;
				list.push_back(received.values[i]);
			}
		}
		//std::cout<<"list.size()1: "<<list.size()<<std::endl;
		if(received.flag == LAST){
			//std::cout<<"flag == last"<<std::endl;
			//amountReceived++;
			//std::cout<<"amountReceived-1: "<<amountReceived-1<<" received.sequence: "<<received.sequence<<std::endl;
			if(amountReceived-1 < received.sequence){
				//std::cout<<"need to request for resend"<<std::endl;
				send.flag = RESEND;
				send.sequence = 0;
				send.num_values = received.sequence+1;
				for(unsigned int i = 0; i < send.num_values; i++){
					send.values[i] = i;
				}
				temp.sequence = htonl(send.sequence);
				temp.flag = htonl(send.flag);
				temp.num_values = htonl(send.num_values);
				for(unsigned int j = 0; j < send.num_values; j++){
					temp.values[j] = htonl(send.values[j]);
				}
				//std::cout<<"sending resend message to server"<<std::endl;
				n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
				list.clear();
				amountReceived = 0;
			}
			else{
				//std::cout<<"sorting list"<<std::endl;
				//code for sort taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=1a952c7b-5ebd-4215-a514-78fb6adfdb29
				std::sort(list.begin(), list.end(), [](const unsigned int& a, const unsigned int& b){
						return std::to_string(a).compare(std::to_string(b))<0;
					});
				//std::cout<<"list sorted"<<std::endl;
				send.num_values = 0;
				send.sequence = 0;
				send.flag = NONE;
				// for(unsigned int k = 0; k < list.size(); k++){
				// 	//std::cout<<"sorted list: "<<list[k]<<std::endl;
				// }
				//std::cout<<"list.size(): "<<list.size()<<std::endl;
				for(unsigned int i = 0; i < list.size(); i++){
					//std::cout<<"send.num_values: "<<send.num_values<<std::endl;
					//std::cout<<"i in for loop: "<<i<<std::endl;
					send.values[send.num_values] = list[i];
					send.num_values++;
					if(send.num_values == MAX_VALUES){
						for(unsigned int j = 0; j < send.num_values; j++){
							temp.values[j] = htonl(send.values[j]);
						}
						temp.num_values = htonl(send.num_values);
						temp.sequence = htonl(send.sequence);
						temp.flag = htonl(send.flag);
						//std::cout<<"sending list back"<<std::endl;
						sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
						send.sequence++;
						send.num_values = 0;
					}
				}
				send.flag = LAST;
				for(unsigned int j = 0; j < send.num_values; j++){
					temp.values[j] = htonl(send.values[j]);
				}
				temp.num_values = htonl(send.num_values);
				temp.sequence = htonl(send.sequence);
				temp.flag = htonl(send.flag);
				sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr*)&remote_addr, len);
				list.clear();
			}
		}
		
    }

}

/*
 * Shutdown the server. Typically this will involve closing the server socket.
 */
void RadixServer::stop() {
    //close(this);
}

/*
 * Send the contents of the lists contained with LISTS to the server on HIOSTNAME
 * listening on PORT in datagrams containing batches of unsigned integers. These
 * will be returned to you MSD Radix sorted and should be retied to the caller
 * via LISTS.
 */
void RadixClient::msd(const char *hostname, const int port, std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists) { 
	//code taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=e8d75c88-e18b-45fc-b5b8-ddae7fcc3093
	//more code take from lecture for timeout related things
	// https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=b07033c4-f623-4907-9ab4-85e9fb3d79cb
    //sendto(sockid, msg, msglen, flags, remoteAddr, addrlen);
    //recvfrom(sockid, recvBuf, bufLen, flags, &remoteAddr, addrlen);
    //std::cout<<"in method RadixClient"<<std::endl;
    struct timeval tv;
    fd_set readfds;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0) exit(-1);

    struct hostent *server = gethostbyname(hostname);
    if(server == NULL) exit(-1);

    struct sockaddr_in remote_addr;
    bzero((char*) &remote_addr, sizeof(remote_addr));
    remote_addr.sin_family = AF_INET;
    bcopy((char*)server->h_addr, (char*)&remote_addr.sin_addr.s_addr, server->h_length);
    remote_addr.sin_port = htons(port);

    socklen_t len = sizeof(remote_addr);

    Message msg;
    Message temp;
    FD_SET(sockfd, &readfds);
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    //unsigned int temp[MAX_VALUES];
    int n = 0;
    //std::cout<<"before looping through lists"<<std::endl;
	for(std::vector<unsigned int> &list: lists){
		bool endDropped = false;
		bool endReceived = false;
		int listSize = list.size();
		msg.num_values = 0;
		msg.sequence = 0;
		msg.flag = NONE;

		for(unsigned int i = 0; i < list.size(); i++){

			msg.values[msg.num_values] = list[i];
			msg.num_values++;
			//std::cout<<"in second loop msg.num_values= "<<msg.num_values<<std::endl;
			if(msg.num_values == MAX_VALUES){
				//temp = htonl(msg.values);
				temp.sequence = htonl(msg.sequence);
				temp.flag = htonl(msg.flag);
				temp.num_values = htonl(msg.num_values);
				for(unsigned int j = 0; j < msg.num_values; j++){
					temp.values[j] = htonl(msg.values[j]);
				}
				

				n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
				//std::cout<<"After first send, n = "<<n<<std::endl;
				if(n < 0)
					exit(-1);
				msg.sequence++;
				msg.num_values = 0;
			}
		}
		msg.flag = LAST;
		temp.sequence = htonl(msg.sequence);
		temp.flag = htonl(msg.flag);
		temp.num_values = htonl(msg.num_values);
		for(unsigned int j = 0; j < msg.num_values; j++){
			temp.values[j] = htonl(msg.values[j]);
		}
		n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
		//std::cout<<"sending the last, n = "<<n<<std::endl;
		if(n < 0)
			exit(-1);
		//list.clear();
		Message received;
		received.flag = NONE;
		unsigned int amountReceived = 0;
		bool done = false;
		bool receiving = false;
		//std::cout<<"before do while"<<std::endl;
		FD_SET(sockfd, &readfds);

		do{
			
			//std::cout<<"inside do while"<<std::endl;
			int rc = select(sockfd+1, &readfds, 0, 0, &tv);
			FD_SET(sockfd, &readfds);
			tv.tv_sec = 2;
    		tv.tv_usec = 0;
			//if it doesn't have anything to receive after 2 seconds assume that the last packet was dropped and ask for a resend
			//std::cout<<"rc= "<<rc<<std::endl;
			//std::cout<<"endReceived before "<<endReceived<<std::endl;
			if(rc == 0 && endDropped == false && endReceived == false){
				msg.flag = RESEND;
				msg.sequence = 0;
				if(listSize%128 == 0)
					msg.num_values = listSize/128;
				else
					msg.num_values = listSize/128 +1;
				for(unsigned int i = 0; i < msg.num_values; i++){
					msg.values[i] = i;
				}
				temp.sequence = htonl(msg.sequence);
				temp.flag = htonl(msg.flag);
				temp.num_values = htonl(msg.num_values);
				for(unsigned int j = 0; j < msg.num_values; j++){
					temp.values[j] = htonl(msg.values[j]);
				}
				//std::cout<<"sending resend message to server in timeout"<<std::endl;
				n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
				list.clear();
				amountReceived = 0;
				endDropped = true;
				continue;
			}
			n = recvfrom(sockfd, &received, sizeof(Message), 0, (struct sockaddr *)&remote_addr, &len);
	    	if(n < 0) exit(-1);
	    	received.sequence = ntohl(received.sequence);
			received.num_values = ntohl(received.num_values);
			received.flag = ntohl(received.flag);
			//std::cout<<"received.flag = "<<received.flag<<std::endl;
			for(unsigned int i = 0; i < received.num_values; i++){
				received.values[i] = ntohl(received.values[i]);
				//std::cout<<"recieved value from client: "<<received.values[i]<<"at index: "<<i<<std::endl;
			}
			
			//IF YOU NEED TO DO ANY RESENDS----------------------------------------------------------------------------

			if(received.flag == RESEND){
				//std::cout<<"in resend"<<std::endl;
				std::vector<unsigned int> resendSequence;
				for(unsigned int i = 0; i < received.num_values;i++){
					resendSequence.push_back(received.values[i]);
				}
				msg.num_values = 0;
				msg.sequence = 0;
				msg.flag = NONE;
				unsigned int resendIndex = 0;
				for(unsigned int i = 0; i < list.size(); i++){
					if(resendSequence.size() == resendIndex)
						break;
					msg.values[msg.num_values] = list[i];
					msg.num_values++;
					//std::cout<<"in second loop msg.num_values= "<<msg.num_values<<std::endl;
					if(msg.num_values == MAX_VALUES){
						//temp = htonl(msg.values);
						temp.sequence = htonl(msg.sequence);
						temp.flag = htonl(msg.flag);
						temp.num_values = htonl(msg.num_values);
						for(unsigned int j = 0; j < msg.num_values; j++){
							temp.values[j] = htonl(msg.values[j]);
						}
						
						if(msg.sequence == resendSequence[resendIndex]){
							n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
							//std::cout<<"After first send, n = "<<n<<std::endl;
							if(n < 0)
								exit(-1);
							resendIndex++;
						}
						msg.sequence++;
						msg.num_values = 0;
					}
				}
				msg.flag = LAST;
				temp.sequence = htonl(msg.sequence);
				temp.flag = htonl(msg.flag);
				temp.num_values = htonl(msg.num_values);
				for(unsigned int j = 0; j < msg.num_values; j++){
					temp.values[j] = htonl(msg.values[j]);
				}
				n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
				//std::cout<<"sending the last, n = "<<n<<std::endl;
				if(n < 0)
					exit(-1);
				//list.clear();
			}

			//IF IT'S NOT A RESEND -------------------------------------------------------------------------------
			else{
				//std::cout<<"inside the not resend"<<std::endl;
				if(receiving == false){
					//std::cout<<"clearing list"<<std::endl;
					list.clear();
					receiving = true;
				}
				amountReceived++;
				for(unsigned int i = 0; i < received.num_values; i++){
					//std::cout<<"pushing on numbers to list"<<std::endl;
					list.push_back(received.values[i]);
				}
				//std::cout<<"check to see if flag is last"<<std::endl;
				//std::cout<<"flag for received = "<<received.flag<<std::endl;
				if(received.flag == LAST){
					endReceived = true;
					//std::cout<<"endReceived is true"<<std::endl;
					//std::cout<<"amountReceived = "<<amountReceived-1<<"\nreceived.sequence = "<<received.sequence<<std::endl;
					if(amountReceived-1 < received.sequence){
						//std::cout<<"need to request for resend"<<std::endl;
						msg.flag = RESEND;
						msg.sequence = 0;
						msg.num_values = received.sequence+1;
						for(unsigned int i = 0; i < msg.num_values; i++){
							msg.values[i] = i;
						}
						temp.sequence = htonl(msg.sequence);
						temp.flag = htonl(msg.flag);
						temp.num_values = htonl(msg.num_values);
						for(unsigned int j = 0; j < msg.num_values; j++){
							temp.values[j] = htonl(msg.values[j]);
						}
						//std::cout<<"sending resend message to server in LAST"<<std::endl;
						n = sendto(sockfd, &temp, sizeof(Message), 0, (struct sockaddr *)&remote_addr, len);
						list.clear();
						amountReceived = 0;
					}
					else{
						//std::cout<<"setting done to true"<<std::endl;
						done = true;
					}
				}
				//std::cout<<"after the last if statement"<<std::endl;
			}
		}while(done == false);
		//std::cout<<"done with everything"<<std::endl;
		
	}
	close(sockfd);
	//RadixServer::stop();    
}
