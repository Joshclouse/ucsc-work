import sys
# Python3 Reverse Polish Notation (RPN) Calculator
#
# Accepts an single argument in RPN, evaluates it and prints the answer to stdout.
# 
#jclouse@ucsc.edu
#CMPS109
#LAB1 RPN calculator


#For passing in args I got it from this website
#https://www.saltycrane.com/blog/2007/12/how-to-pass-command-line-arguments-to/
#for splitting strings I went to this website
#https://www.tutorialspoint.com/python/string_split.htm
#Stuff for lists I got here 
#https://www.tutorialspoint.com/python/python_lists.htm
stack =[] 
end = 0
arguments = sys.argv[1].split(" ")
#print (arguments)
for i in range(0, len(arguments)-1):
	#print(arguments[i])
	if arguments[i] == '+':
		#print("In addition")
		second = stack.pop()
		first = stack.pop()
		result = first + second
		stack.append(result)
	elif arguments[i] == "-":
		second = stack.pop()
		first = stack.pop()
		result = first - second
		stack.append(result)
	elif arguments[i] == "*":
		second = stack.pop()
		first = stack.pop()
		result = first * second
		stack.append(result)
	elif arguments[i] == "/":
		second = stack.pop()
		first = stack.pop()
		result = first / second
		stack.append(result)
	elif arguments[i] == "^":
		second = stack.pop()
		first = stack.pop()
		result = first ** second
		stack.append(result)
	else:
		stack.append(int(arguments[i]))
print (stack[0])

