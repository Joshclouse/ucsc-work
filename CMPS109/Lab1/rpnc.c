#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * Reverse Polish Notation (RPN) Calculator
 *
 * Accepts a single argument in RPN, evaluates it and prints the answer to stdout.
 * 
 * Returns -1 on error, 0 otherwise.
 */
//jclouse@ucsc.edu
//CS109
//Lab1
//RPN.c





int main(int argc, char* argv[])
{
	//for strtock I looked it up on Stack over flow at this link
	//https://www.tutorialspoint.com/c_standard_library/c_function_strtok.htm
	const char s[2] = " ";
	char* argument = strtok(argv[1], s);
	int end = 0;
	int first;
	int second;
	int sum;
	int stack[256];
	while(argument != NULL)
	{
		//printf("Tokenized argument: %c\n", argument[0]);
		if(argument[0] == '+')
		{
			//printf("%s\n", "inside of add");
			first = stack[end-2];
			second = stack[end -1];
			end -= 2;
			sum = first + second;
			stack[end] = sum;
			end++;
		}
		else if(argument[0] == '-')
		{
			first = stack[end-2];
			second = stack[end -1];
			end -= 2;
			sum = first - second;
			stack[end] = sum;
			end++;
		}
		else if(argument[0] == '*')
		{
			first = stack[end-2];
			second = stack[end -1];
			end -= 2;
			sum = first * second;
			stack[end] = sum;
			end++;
		}
		else if(argument[0] == '/')
		{
			first = stack[end-2];
			second = stack[end -1];
			end -= 2;
			sum = first / second;
			stack[end] = sum;
			end++;
		}
		else if(argument[0] == '^')
		{
			first = stack[end-2];
			second = stack[end -1];
			end -= 2;
			sum = 1;
			for(int j = 0; j < second; j++)
			{
				sum = sum * first;
			}
			stack[end] = sum;
			end++;
		}
		else
		{
			
			stack[end] = atoi(argument);
			//printf("Adding to the stack: %d\n", stack[end]);
			end++;
		}
		argument = strtok(NULL, s);
	}
	printf("%d\n", stack[end-1]);
	return(0);
}

