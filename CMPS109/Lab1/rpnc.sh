#
# Bash Reverse Polish Notation (RPN) Calculator
#
# Accepts an single argument a RPN, evaluates it and prints the answer to stdout.
#jclouse@ucsc.edu
#CMPS109
#Lab1
#Much of the syntax and source taken from:
#https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=764d4b0f-c245-4492-a317-4dded677a139
 
stack=()
end=0
arguments=`echo "$1" | sed 's/\*/x/g'`

for i in $arguments
do
	if [[ "$i" =~ ^[0-9]+$ ]]
	then
		stack[$end]=$i
	else
		first=${stack[end - 2]}
		second=${stack[end - 1]}
		((end -= 2))
		
		if [[ "$i" == "+" ]] 
		then
			(( value = $first + $second ))
		elif [[ "$i" == "-" ]]
		then
			(( value = $first - $second ))
		elif [[ "$i" == "/" ]]
		then
			(( value = $first / $second ))
		elif [[ "$i" == "x" ]]
		then
			((value = $first * $second))
		elif [[ "$i" == "^" ]]
		then
			(( value = $first ** $second ))
		fi
		stack[$end]=$value
	fi
	((end += 1))
done
echo ${stack[0]}

