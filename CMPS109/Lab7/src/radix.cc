/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include "radix.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <string>
#include <iostream>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <strings.h>
#include <string>



void ParallelRadixSort::msd(std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists, const unsigned int cores) { 
    //code taken from lecture to do embarassingly parallel sort from lab 5
	//https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=1a952c7b-5ebd-4215-a514-78fb6adfdb29
	std::vector<std::thread*> threads;
	sem_t vacancyOut;
	sem_t vacancyIn;
	int val;
	sem_init(&vacancyOut,1,1);
	sem_init(&vacancyIn,1,cores -1);
	sem_getvalue(&vacancyIn, &val);
	unsigned int total = 0;
	bool outerFree = true;
	for(std::vector<unsigned int> &list : lists){
		//Creating a new thread which will sort the given list into sublists and then pass those
		//sublists to another thread
		if(cores != 1)
			sem_wait(&vacancyOut);
		//cond[threads.size()] = new std::condition_variable;
		//status.push_back(false);
		//thread_id = status.size()-1;
		threads.push_back(new std::thread{[&list, &cores, &outerFree, &threads, &vacancyIn, &vacancyOut]{
			bool permission = false;
			
			unsigned int num = 0;
			
			unsigned int subTotal = 0;
			

			//creates a 2D vector of sublists to hold the sublists for each most sig. digit
			std::vector<std::vector<unsigned int>> subLists(10);
			std::vector<std::thread*> subThreads;
			//std::cout<<"Created sublist array"<<std::endl;
			for(unsigned int i = 0; i < list.size(); i++){
				//std::cout<<"creating sublists..."<<std::endl;
				std::string temp = std::to_string(list[i]);
				//std::cout<<"temp = "<<temp<<std::endl;
				if(temp.at(0) == '1'){
					subLists[0].push_back(list[i]);
				}
				else if(temp.at(0) == '2'){
					subLists[1].push_back(list[i]);
				}
				else if(temp.at(0) == '3'){
					subLists[2].push_back(list[i]);
				}
				else if(temp.at(0) == '4'){
					subLists[3].push_back(list[i]);
				}
				else if(temp.at(0) == '5'){
					subLists[4].push_back(list[i]);
				}
				else if(temp.at(0) == '6'){
					subLists[5].push_back(list[i]);
				}
				else if(temp.at(0) == '7'){
					subLists[6].push_back(list[i]);
				}
				else if(temp.at(0) == '8'){
					subLists[7].push_back(list[i]);
				}
				else if(temp.at(0) == '9'){
					subLists[8].push_back(list[i]);
				}
			}
			
			for(std::vector<unsigned int> &subList: subLists){
				int value;
				sem_getvalue(&vacancyIn, &value);
				if(cores != 1){
					sem_wait(&vacancyIn);
					subThreads.push_back(new std::thread{[&subList, &num, &subTotal, &vacancyIn, &vacancyOut, &permission, &threads, &cores, &subLists]{
						//std::cout<<"creating subthread"<<std::endl;
						num++;
						//std::unique_lock<std::mutex> bard(innerThread);
						std::sort(subList.begin(), subList.end(), [](const unsigned int& a, const unsigned int& b){
							return std::to_string(a).compare(std::to_string(b)) <0;
						});
						// for(unsigned int x = 0; x < subList.size(); x++){
						// 	std::cout<<subList[x]<<std::endl;
						// }
						subTotal++;
						num--;
						if(permission == false && subLists.size() - subTotal < cores - threads.size()){
							permission = true;
							sem_post(&vacancyOut);
						}else{
							
							sem_post(&vacancyIn);
						}
						//std::cout<<"sub finished"<<std::endl;
						//openInner.notify_one();

					}});
				}
				else{
					std::sort(subList.begin(), subList.end(), [](const unsigned int& a, const unsigned int& b){
							return std::to_string(a).compare(std::to_string(b)) <0;
						});
				}

			}
			if(cores != 1){
				for(std::thread *thread: subThreads)
					thread->join();
				subThreads.clear();
			}
			//std::cout<<"About to clear the list"<<std::endl;
			list.clear();
			
			for(unsigned int i = 0; i < subLists.size();i++){
				//std::cout<<"sorted subLists #"<<i<<std::endl;
				for(unsigned int j = 0; j < subLists[i].size();j++){
					list.push_back(subLists[i][j]);
					//std::cout<<subLists[i][j]<<std::endl;
				}
			}
			//std::cout<<"about to exit the big thread"<<std::endl;
			//status[thread_id] = true;
			if(cores != 1)
				sem_post(&vacancyIn);
		}});
		//std::cout<<"after big thread"<<std::endl;
		threads[0]->join();
		threads.erase(threads.begin());
		//open.notify_one();
		total++;
		if(total == lists.size()){
			//std::cout<<"inside if statement"<<std::endl;
			for(std::thread *thread : threads)
				thread->join();
			threads.clear();
		}
		//std::cout<<"after if statement"<<std::endl;
	}
}

//code for server and client was mainly derived from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=0a06d645-4145-445c-9cae-dfdb9c770e52
RadixServer::RadixServer(const int port, const unsigned int cores) {
	//std::vector<std::vector<unsigned int>>> lists;
	unsigned int zero = 0;
	zero = htonl(zero);
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) exit(-1);

	struct sockaddr_in server_addr;
	bzero((char*) &server_addr, sizeof(server_addr));

	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(port);

	if(bind(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
		exit(-1);
	listen(sockfd, 5);

	unsigned int temp = 0;
	unsigned int buffer = 0;
	int n;
	struct sockaddr_in client_addr;
	socklen_t len = sizeof(client_addr);

	int newsockfd = accept(sockfd, (struct sockaddr*)&client_addr, &len);
	if(newsockfd < 0) exit(-1);
	std::vector<unsigned int> list;

	for(;;){
		//std::cout<<"in for loop"<<std::endl;
		//lists.clear();
		//std::vector<unsigned int> list;
		// struct sockaddr_in client_addr;
		// socklen_t len = sizeof(client_addr);


		n = recv(newsockfd, (void*)&buffer, sizeof(unsigned int), 0);
		//std::cout<<"value of recv: "<<n<<std::endl;
		if(n == 0 )
			break;
		if(n < 0)
			exit(-1);
		//int j = 0;
		else{
			temp = ntohl(buffer);
			if(temp != 0){
				list.push_back(temp);
			}
			//code for sort taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=1a952c7b-5ebd-4215-a514-78fb6adfdb29
			else{
				std::sort(list.begin(), list.end(), [](const unsigned int& a, const unsigned int& b){
					return std::to_string(a).compare(std::to_string(b))<0;
				});
				//std::cout<<"size of the list: "<<list.size()<<std::endl;
				for(unsigned int i = 0; i < list.size()+1; i++){
					///std::cout<<"sending items back: ";
					
					if(i < list.size()){
						temp = htonl(list[i]);
						//std::cout<<temp<<std::endl;
						n = send(newsockfd, (void*)&temp, sizeof(unsigned int), 0);
						//std::cout<<"value of send: "<<n<<std::endl;
						if(n<0)
							exit(-1);
					}
					else{
						//std::cout<<"0"<<std::endl;
						n = send(newsockfd, (void*)&zero, sizeof(unsigned int), 0);
						//std::cout<<"value of send: "<<n<<std::endl;
						if(n <0)
							exit(-1);
					}
				}
				list.clear();
				//std::cout<<"right after for loop"<<std::endl;
			}
			//std::cout<<"made it out of the else"<<std::endl;
		}
		
		//lists.push_back(list);
		//ParallelRadixSort::msd(&lists, cores);
	}
	//std::cout<<"closing sockets"<<std::endl;
	close(newsockfd);
	close(sockfd);
}

void RadixClient::msd(const char *hostname, const int port, std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists) { 
	unsigned int zero = 0;
	zero = htonl(zero);
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    //std::cout<<"one"<<std::endl;
    if(sockfd < 0) exit(-1);

    struct hostent *server = gethostbyname(hostname);
    //std::cout<<"two"<<std::endl;
    if(server == NULL) exit(-1);

    struct sockaddr_in serv_addr;
    //std::cout<<"three"<<std::endl;
    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr, server->h_length);
    //std::cout<<"four"<<std::endl;

    serv_addr.sin_port = htons(port);
    //std::cout<<"five"<<std::endl;
    if(connect(sockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr)) < 0) exit(-1);

    for(std::vector<unsigned int> &list: lists){
    	//std::cout<<"in loop over lists"<<std::endl;
    	int n;
    	unsigned int temp;
    	for(unsigned int i = 0; i < list.size()+1; i++){
    		//std::cout<<"in loop for sublist: "<<i<<std::endl;
    		temp = htonl(list[i]);
    		if(i != list.size())
    			n = send(sockfd, (void*)&temp, sizeof(unsigned int), 0);
    		else
    			n = send(sockfd, (void*)&zero, sizeof(unsigned int), 0);
    		if(n < 0)
    			exit(-1);
    	}
    	//std::cout<<"after for loop"<<std::endl;
    	unsigned int buffer = 0;
		n = read(sockfd, (void*)&buffer, sizeof(unsigned int));
		//std::cout<<"after initial read"<<std::endl;
		int j = 0;
		temp = ntohl(buffer);
    	while(temp != 0){
    		//std::cout<<"in while loop for recieving: "<<j<<std::endl;
			if(n<0) exit(-1);
			list[j] = temp;
			j++;
			n = read(sockfd, (void*)&buffer, sizeof(unsigned int));
			temp = ntohl(buffer);
    	}
    	//std::cout<<"out of while loop"<<std::endl;
    }
    //std::cout<<"closing the socket"<<std::endl;
    close(sockfd);
}
