/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include "radix.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <string>
#include <future>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <semaphore.h>

void ParallelRadixSort::msd(std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists, unsigned int cores) { 

	//code taken from lecture to do embarassingly parallel sort from lab 5
	//https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=1a952c7b-5ebd-4215-a514-78fb6adfdb29
	std::condition_variable open;
	std::condition_variable openInner;
	std::mutex outerThread;
	std::vector<std::thread*> threads;
	sem_t vacancyOut;
	sem_t vacancyIn;
	int val;
	sem_init(&vacancyOut,1,1);
	sem_init(&vacancyIn,1,cores -1);
	sem_getvalue(&vacancyIn, &val);
	//std::cout<<"initial value of vacancyIn = "<<val<<std::endl;
	//std::cout<<"core limit: "<<cores<<std::endl;
	// std::vector<std::condition_variable> cond(24);
	// std::vector<std::condition_variable> subCond(24);
	// std::vector<std::unique_lock<std::mutex>> locks(24);
	// std::vector<std::unique_lock<std::mutex>> subLocks(24);
	unsigned int total = 0;
	bool outerFree = true;
	for(std::vector<unsigned int> &list : lists){
		//Creating a new thread which will sort the given list into sublists and then pass those
		//sublists to another thread
		if(cores != 1)
			sem_wait(&vacancyOut);
		//cond[threads.size()] = new std::condition_variable;
		//status.push_back(false);
		//thread_id = status.size()-1;
		threads.push_back(new std::thread{[&list, &cores, &outerFree, &threads, &vacancyIn, &vacancyOut]{
			bool permission = false;
			
			//std::cout<<"--------created big thread"<<std::endl;
			//std::mutex innerThread;
			//std::cout<<"After second mutex"<<std::endl;
			//std::condition_variable openInner;
			//std::cout<<"after conditional var"<<std::endl;
			//bool innerFree;
			//std::unique_lock<std::mutex> guard(outerThread);
			//std::cout<<"created second lock"<<std::endl;
			unsigned int num = 0;
			
			unsigned int subTotal = 0;
			

			//creates a 2D vector of sublists to hold the sublists for each most sig. digit
			std::vector<std::vector<unsigned int>> subLists(10);
			std::vector<std::thread*> subThreads;
			//std::cout<<"Created sublist array"<<std::endl;
			for(unsigned int i = 0; i < list.size(); i++){
				//std::cout<<"creating sublists..."<<std::endl;
				std::string temp = std::to_string(list[i]);
				//std::cout<<"temp = "<<temp<<std::endl;
				if(temp.at(0) == '1'){
					subLists[0].push_back(list[i]);
				}
				else if(temp.at(0) == '2'){
					subLists[1].push_back(list[i]);
				}
				else if(temp.at(0) == '3'){
					subLists[2].push_back(list[i]);
				}
				else if(temp.at(0) == '4'){
					subLists[3].push_back(list[i]);
				}
				else if(temp.at(0) == '5'){
					subLists[4].push_back(list[i]);
				}
				else if(temp.at(0) == '6'){
					subLists[5].push_back(list[i]);
				}
				else if(temp.at(0) == '7'){
					subLists[6].push_back(list[i]);
				}
				else if(temp.at(0) == '8'){
					subLists[7].push_back(list[i]);
				}
				else if(temp.at(0) == '9'){
					subLists[8].push_back(list[i]);
				}
			}
			
			// int sum = 0;
			// unsigned int avg;
			// for(unsigned int i = 0; i < subLists.size(); i++){
			// 	sum += subLists[i].size();
			// }
			// avg = sum/subLists.size();
			// std::cout<<"average: "<<avg<<std::endl;
			// for(unsigned int i =0; i < subLists.size(); i++){
			// 	if(subLists[i].size() > avg){
			// 		while(subLists[i].size() > avg){
			// 			// if(subLists[i+1] == NULL)
			// 			// 	subList.push_back(vector<unsigned int>);
			// 			//std::cout<<"before making more sublists"<<std::endl;
			// 			subLists[i+1].insert(subLists[i+1].begin(), subLists[i][subLists[i].size()-1]);
			// 			subLists[i].erase(subLists[i].end()-1);
			// 			//std::cout<<"after making more sublists"<<std::endl;
			// 		}
			// 	}
			// }
			// std::cout<<"the sublists are: "<<std::endl;
			// for(unsigned int i = 0; i < subLists.size(); i++){
			// 	std::cout<<"Sublist #"<<i<<"-------------------"<<std::endl;
			// 	for(unsigned int j = 0; j < subLists[i].size(); j++){
			// 		std::cout<<subLists[i][j]<<std::endl;
			// 	}
			// }
			
			//std::unique_lock<std::mutex> bard(innerThread);
			for(std::vector<unsigned int> &subList: subLists){
				//std::unique_lock<std::mutex> bard(innerThread);
				//std::cout<<"num= "<<num<<std::endl;
				 //std::cout<<"looping through sublists"<<std::endl;
				// std::cout<<"cores - threads.size()"<<cores - threads.size()<<std::endl;
				// if(permission == false && subLists.size() - subTotal < cores -threads.size()){
				// 	std::cout<<"in conditional subList.size()"<<std::endl;
				// 	permission = true;
				// 	sem_post(&vacancyOut);
				// }
				int value;
				sem_getvalue(&vacancyIn, &value);
				//std::cout<<"vacancyIn value = "<<value<<std::endl;
				
				// if(num != cores -1)
				// 	bard.unlock();
				//std::cout<<"right before creating a new thread"<<std::endl;
				if(cores != 1){
					sem_wait(&vacancyIn);
					subThreads.push_back(new std::thread{[&subList, &num, &subTotal, &vacancyIn, &vacancyOut, &permission, &threads, &cores, &subLists]{
						//std::cout<<"creating subthread"<<std::endl;
						num++;
						//std::unique_lock<std::mutex> bard(innerThread);
						std::sort(subList.begin(), subList.end(), [](const unsigned int& a, const unsigned int& b){
							return std::to_string(a).compare(std::to_string(b)) <0;
						});
						// for(unsigned int x = 0; x < subList.size(); x++){
						// 	std::cout<<subList[x]<<std::endl;
						// }
						subTotal++;
						num--;
						if(permission == false && subLists.size() - subTotal < cores - threads.size()){
							permission = true;
							sem_post(&vacancyOut);
						}else{
							
							sem_post(&vacancyIn);
						}
						//std::cout<<"sub finished"<<std::endl;
						//openInner.notify_one();

					}});
				}
				else{
					std::sort(subList.begin(), subList.end(), [](const unsigned int& a, const unsigned int& b){
							return std::to_string(a).compare(std::to_string(b)) <0;
						});
				}

			}
			if(cores != 1){
				for(std::thread *thread: subThreads)
					thread->join();
				subThreads.clear();
			}
			//std::cout<<"About to clear the list"<<std::endl;
			list.clear();
			
			for(unsigned int i = 0; i < subLists.size();i++){
				//std::cout<<"sorted subLists #"<<i<<std::endl;
				for(unsigned int j = 0; j < subLists[i].size();j++){
					list.push_back(subLists[i][j]);
					//std::cout<<subLists[i][j]<<std::endl;
				}
			}
			//std::cout<<"about to exit the big thread"<<std::endl;
			//status[thread_id] = true;
			if(cores != 1)
				sem_post(&vacancyIn);
		}});
		//std::cout<<"after big thread"<<std::endl;
		threads[0]->join();
		threads.erase(threads.begin());
		//open.notify_one();
		total++;
		if(total == lists.size()){
			//std::cout<<"inside if statement"<<std::endl;
			for(std::thread *thread : threads)
				thread->join();
			threads.clear();
		}
		//std::cout<<"after if statement"<<std::endl;
	}
}
