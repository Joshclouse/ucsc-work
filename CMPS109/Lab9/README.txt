The hardest lab of the quarter, this lab uses multithreading and distrubted computing to crack passwords. A list of passwords is broadcasted from a server
to all machines on the network. Those machines then must decide amongst themselves to determine who will decode which passwords as well as who will
recieve the decrypted passwords once everything is done (this will be referred to as the main machine). Once that is done the machines do a simple brute 
force algorithm which checks a hash for each combination of a 4 alphanumeric/limited symbol, password until it finds a match by using the "crypt" method
in C++. To do this each portion of possible combinations of the password is broken down into equal subsections and then given to a thread. Each thread checks their given
range of combinations, and once the password is found, sends it back to the main machine. Once the main machine has received all the decrypted passwords, it then sends its
results back to the server to be evaluated. 