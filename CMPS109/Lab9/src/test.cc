//Joshua Clouse
//CMPS109
//Lab9

#include "crack.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <string>
#include <iostream>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <string>



void sendMulticast(Message &msg, int &socket, sockaddr_in &multicastAddr){
	msg.num_passwds = htonl(msg.num_passwds);
	msg.port = htonl(msg.port);
	int n = sendto(socket, &msg, sizeof(Message), 0, (struct sockaddr *) &multicastAddr, sizeof(multicastAddr));
	std::cout<<"sending message with n = "<<n<<std::endl;
	if(n < 0) exit(-1);
}

void recUnicast(Message &msg, int &socket){
	int n = recv(socket, &msg, sizeof(Message), 0);
	msg.num_passwds = ntohl(msg.num_passwds);
	msg.port = ntohl(msg.port);
}

int main(int argc, char*argv[]){
	//Code taken from lecture https://classes.soe.ucsc.edu/cmps109/Spring18/SECURE/15.Distributed5.pdf
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockfd < 0) exit(-1);

	int ttl = 1;
	if(setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_TTL, (void*) &ttl, sizeof(ttl))<0)
		exit(-1);
	struct sockaddr_in multicastAddr;
	memset(&multicastAddr, 0, sizeof(multicastAddr));
	multicastAddr.sin_family = AF_INET;
	multicastAddr.sin_addr.s_addr = get_multicast_address();
	multicastAddr.sin_port = htons(get_multicast_port());

	Message msg;
	strcpy(msg.cruzid, "jclouse");
	strcpy(msg.passwds[0], "lqFucz6Kp.jPE");
	strcpy(msg.passwds[1], "00Pp9Oy0VWmn2");
	strcpy(msg.passwds[2], "zzOzL0bB0ocqo");
	strcpy(msg.passwds[3], "yyNhnfhEpDmTY");
	strcpy(msg.passwds[4], "5tQvIqEDV1gzw");
	strcpy(msg.passwds[5], "xxo0q4QVK0mOg");
	msg.num_passwds = 6;
	msg.port = 8080;
	char hostname[64];
	hostname[63] = '\0';
	gethostname(hostname, 63);
	strcpy(msg.hostname, hostname);
	sendMulticast(msg, sockfd, multicastAddr);

	//sets hostname of this machine to hostname
	// char hostname[64];
	// hostname[63] = '\0';
	// gethostname(hostname, 63);

	// strcpy(msg.hostname, hostname);
	// std::cout<<"hostname: "<<msg.hostname<<std::endl;

	//code taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=0a06d645-4145-445c-9cae-dfdb9c770e52
	int uniSock = socket(AF_INET, SOCK_STREAM, 0);
	if(uniSock < 0) exit(-1);

	struct sockaddr_in serv_addr;
	bzero((char*) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = 8080;

	if(bind(uniSock, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
		exit(-1);
	listen(uniSock, 5);
	struct sockaddr_in client_addr;
	socklen_t len = sizeof(client_addr);
	int newsockfd = accept(uniSock, (struct sockaddr*)&client_addr, &len);
	if(newsockfd < 0) exit(-1);
	recUnicast(msg, newsockfd);
	for(unsigned int i = 0; i < msg.num_passwds; i++){
		std::cout<<"received message with Password: "<<msg.passwds[i]<<std::endl;
	}
	
	close(sockfd);
	close(newsockfd);
	close(uniSock);
}