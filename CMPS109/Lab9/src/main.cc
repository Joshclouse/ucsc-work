//Joshua Clouse
//CMPS109
//Lab9

#include "crack.h"
#include <algorithm>
#include <vector>
#include <thread>
#include <string>
#include <iostream>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <string>
#include <crypt.h>
#include <time.h>



void recMulticast(Message &msg, int &socket){
	int n = recvfrom(socket, &msg, sizeof(Message), 0, NULL, 0);
	if(n < 0)
		exit(-1);
	msg.num_passwds = ntohl(msg.num_passwds);
	msg.port = msg.port;
}

void recUnicast(Message &msg, int &socket){
	int n = recv(socket, &msg, sizeof(Message), 0);
	if(n < 0){
		//std::cout<<"receieve went bad"<<std::endl;
		exit(-1);
	}
	msg.num_passwds = ntohl(msg.num_passwds);
	msg.port = ntohl(msg.port);
}

//help for crypt_r found here https://stackoverflow.com/questions/9335777/crypt-r-example
void crackCode(bool &cracked, int start, int end, char *hash, char *passwd){
	//std::cout<<"cracking password: "<<start<< " "<<end<<std::endl;
	// if(cracked == true)
	// 	return;
	char temp[3];
	temp[0] = hash[0];
	temp[1] = hash[1];
	const char *salt = temp;
	struct crypt_data data;
	data.initialized = 0;
	for(int i = start; i < end; i++){
		char firstPlace = i;
		passwd[0] = firstPlace;
		for(int j = 48; j < 123; j++){
			//if the ascii value gets to the end of the digits/uppercase skip to the next
			//usable ascii value
			if(j == 58)
				j = 65;
			if(j == 91)
				j = 97;
			char secondPlace = j;
			passwd[1] = secondPlace;
			for(int k = 48; k < 123; k++){
				if(k == 58)
					k = 65;
				if(k ==91)
					k = 97;
				char thirdPlace = k;
				passwd[2] = thirdPlace;
				for(int l = 48; l < 123; l++){
					// if(cracked == true)
					// 	return;
					if(l == 58)
						l = 65;
					if(l == 91)
						l = 97;
					char fourthPlace = l;
					passwd[3] = fourthPlace;
					//std::cout<<"checking password: "<<passwd<<std::endl;
					char *result = crypt_r(passwd, salt, &data);
					if(strcmp(result, hash) == 0){
						//std::cout<<"password cracked: "<<passwd<<std::endl;
						cracked = true;
						strcpy(hash, passwd);
						return;
					}
				}
			}
		}
	}
}

void sendUnicast(Message &msg, int &socket){
	msg.num_passwds = htonl(msg.num_passwds);
	msg.port = htonl(msg.port);
	int n = send(socket, &msg, sizeof(Message), 0);
	if(n < 0)
		exit(-1);	
}

void distribute(char *hostname, unsigned int &first, unsigned int &second, unsigned int &third, unsigned int &fourth, unsigned int &fifth, unsigned int num_passwds){
	if(num_passwds%4 == 0){
		first = 0;
		second = num_passwds/4;
		third = second+num_passwds/4;
		fourth = third+num_passwds/4;
		fifth = fourth + num_passwds/4;
	}
	if(num_passwds%4 == 1){
		first = 0;
		second = num_passwds/4;
		third = second+num_passwds/4;
		fourth = third+num_passwds/4;
		fifth = fourth + num_passwds/4 + 1;
	}
	if(num_passwds%4 == 2){
		first = 0;
		second = num_passwds/4;
		third = second+num_passwds/4;
		fourth = third+num_passwds/4+1;
		fifth = fourth + num_passwds/4+1;
	}
	if(num_passwds%4 == 3){
		first = 0;
		second = num_passwds/4;
		third = second+num_passwds/4+1;
		fourth = third+num_passwds/4+1;
		fifth = fourth + num_passwds/4+1;
	}
}

int main(int argc, char *argv[]){

	//creates a multicast socket for receiving message from client and sending to
	//other machines
	//Code taken from lecture https://classes.soe.ucsc.edu/cmps109/Spring18/SECURE/15.Distributed5.pdf

	//checks to see what machine 'this' is
	//time_t start;
	//time_t end;
	sem_t bigThread;
	sem_t littleThread;
	sem_init(&bigThread,1, 1);
	sem_init(&littleThread,1,22);
	char hostname[64];
	hostname[63] = '\0';
	gethostname(hostname, 63);
	//std::cout<<"hostname: "<<hostname<<std::endl;
	int sockfd;
	struct sockaddr_in server_addr;
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	bzero((char*) &server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(get_multicast_port());

	if(bind(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr))<0)
		exit(-1);
	struct ip_mreq multicastRequest;
	multicastRequest.imr_multiaddr.s_addr = get_multicast_address();
	multicastRequest.imr_interface.s_addr = htonl(INADDR_ANY);
	if(setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, 
		(void *) &multicastRequest, sizeof(multicastRequest)) < 0)
		exit(-1);
	
	//struct ip_mreq multicastRequest;
	int uniSock;
	unsigned int returnPort;

	//std::cout<<"hostname is grolliffe"<<std::endl;
	


	Message msg;
	char actualHost[MAX_HOSTNAME_LEN];
	std::vector<std::thread*>threads;
	unsigned int completed = 0;
	for(;;){
		unsigned int upperBound;
		unsigned int lowerBound;
		completed = 0;
		//std::cout<<"Ready to go"<<std::endl;

		//if we are in grolliffe, evenly distribute the hashes to all the servers
		recMulticast(msg, sockfd);
		//time(&start);
		//std::cout<<"Start"<<std::endl;
		strcpy(actualHost, msg.hostname);
		//std::cout<<"msg.hostname"<<msg.hostname<<std::endl;
		//std::cout<<"actualhost: "<<actualHost<<std::endl;
		if(strcmp(msg.cruzid, "jclouse") != 0){
			continue;
		}
		returnPort = msg.port;
		unsigned int first, second, third, fourth, fifth;
		distribute(hostname, first, second, third, fourth, fifth, msg.num_passwds);

		if(strcmp(hostname, "grolliffe") == 0){
			//std::cout<<"I am grolliffe"<<std::endl;
			lowerBound = first;
			upperBound = second;
			//std::cout<<"Lower: "<<lowerBound<<"Upper: "<<upperBound<<std::endl;
		}

		if(strcmp(hostname, "thor")== 0){
			//std::cout<<"I am thor"<<std::endl;
			lowerBound = second;
			upperBound = third;
			//std::cout<<"Lower: "<<lowerBound<<"Upper: "<<upperBound<<std::endl;
		}
		if(strcmp(hostname, "olaf")== 0){
			//std::cout<<"I am olaf"<<std::endl;
			lowerBound = third;
			upperBound = fourth;
			//std::cout<<"Lower: "<<lowerBound<<"Upper: "<<upperBound<<std::endl;
		}
		if(strcmp(hostname, "graculus")== 0){
			//std::cout<<"I am graculus"<<std::endl;
			lowerBound = fourth;
			upperBound = fifth;
			//std::cout<<"Lower: "<<lowerBound<<"Upper: "<<upperBound<<std::endl;
		}
		
		
		sem_wait(&bigThread);
		std::thread BT([&bigThread, &littleThread, &threads, &lowerBound, &upperBound, &completed, &msg]{
			unsigned int i = 0;
			for(auto &hash: msg.passwds){
				bool cracked = false;
				if(i >= msg.num_passwds)
						break;
				//std::cout<<"in cracking loop: "<<i<<std::endl;
				if(i >= lowerBound && i < upperBound){
					int digits = 0;
					int caps = 0;
					int lowerCase = 0;
					int threadNum = 0;
					
					while(cracked == false){
						//\char passwd[5];
						//std::cout<<"threadNum: "<<threadNum<<std::endl;
						sem_wait(&littleThread);
						if(threadNum < 5 && cracked == false){
							//sem_wait(&littleThread);
							threads.push_back(new std::thread{[&hash, digits, &cracked, &littleThread, &threads]{
								//std::cout<<"new thread "<<threads.size()<<std::endl;
								int start = 48 + digits;
								int end = start+2;
								char passwd[5];
								crackCode(cracked, start, end, hash, passwd);
								sem_post(&littleThread);
							}});
							digits+= 2;
						}
						if(threadNum >= 5 && threadNum < 13 && cracked == false){
							threads.push_back(new std::thread{[&hash, caps, &cracked, &littleThread, &threads]{
								//std::cout<<"new thread "<<threads.size()<<std::endl;
								int start = 65 + caps;
								int end = start+3;
								char passwd[5];
								crackCode(cracked, start, end, hash, passwd);
								sem_post(&littleThread);
							}});
							caps+=3;
						}
						if(threadNum == 13 && cracked == false){
							threads.push_back(new std::thread{[&hash, &cracked, &littleThread, &threads]{
								//std::cout<<"new thread "<<threads.size()<<std::endl;
								int start = 89;
								int end = start+2;
								char passwd[5];
								crackCode(cracked, start, end, hash, passwd);
								sem_post(&littleThread);
							}});
						}
						if(threadNum >= 14 && threadNum < 22 && cracked == false){
							threads.push_back(new std::thread{[&hash, lowerCase, &cracked, &littleThread, &threads]{
								//std::cout<<"new thread "<<threads.size()<<std::endl;
								int start = 97 + lowerCase;
								int end = start+ 3;
								char passwd[5];
								crackCode(cracked, start, end, hash, passwd);
								sem_post(&littleThread);
							}});
							lowerCase+=3;	
						}
						if(threadNum == 22 && cracked == false){
							threads.push_back(new std::thread{[&hash, &cracked, &littleThread, &threads]{
								//std::cout<<"new thread "<<threads.size()<<std::endl;
								int start = 121;
								int end = start+2;
								char passwd[5];
								crackCode(cracked, start, end, hash, passwd);
								sem_post(&littleThread);
							}});
						}

						if(cracked == true){
							//std::cout<<"password cracked: "<<hash<<std::endl;
							
						}
						if(threadNum < 23)
							threadNum++;
						else if(threadNum == 23){
							//std::cout<<"threads all created"<<std::endl;
							threadNum++;
						}
						
					}
					
					
					//crack(hash, passwd);
					//std::cout<<"cracked password: "<<passwd<<std::endl;
					//puts cracked password back into message
					//strcpy(hash, passwd);
					completed++;
					//std::cout<<"completed: "<<completed<<std::endl;
					
				}
				if(completed == upperBound-lowerBound){
					break;
				}

				i++;
			}
			sem_post(&bigThread);
		});
		//if you aren't grolliffe make a client socket and send your passwords over
		if(strcmp(hostname, "grolliffe") != 0){
			//make socket
			sem_wait(&bigThread);
			BT.join();
			int sock = socket(AF_INET, SOCK_STREAM, 0);
			//std::cout<<"sock: "<<sock<<std::endl;
			if(sock < 0) exit(-1);
			struct hostent *server = gethostbyname("grolliffe");
			if(server == NULL) exit(-1);
			struct sockaddr_in serv_addr;
			bzero((char*) &serv_addr, sizeof(serv_addr));
			serv_addr.sin_family = AF_INET;
			bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr, server->h_length);
			serv_addr.sin_port = htons(get_unicast_port());
			int c = connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
			// while(c < 0){
			// 	c = connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
			// }
			if(c < 0){
				 perror("sock is < 0");
				 exit(-1);
			} 
			strcpy(msg.hostname, hostname);
			//std::cout<<"sending message"<<std::endl;
			sendUnicast(msg, sock);
			close(sock);
			//std::cout<<"joining threads"<<std::endl;
			for(std::thread *thread : threads)
				thread->join();
			threads.clear();
			//std::cout<<"threads joined"<<std::endl;
			break;

		}
		//you are grolliffe, make a server socket and receive the passwords and place them in the right order
		else{
			
			for(int i = 0; i < 3; i++){
				int softServe = socket(AF_INET, SOCK_STREAM, 0);
				int option = 1;
				setsockopt(softServe, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
				//std::cout<<"softServe: "<<softServe<<std::endl;
				if(softServe < 0) exit(-1);
				struct sockaddr_in server_addr;
				bzero((char*) &server_addr, sizeof(server_addr));
				server_addr.sin_family = AF_INET;
				server_addr.sin_addr.s_addr = INADDR_ANY;
				server_addr.sin_port = htons(get_unicast_port());

				if(bind(softServe, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0){
					perror("bind went bad");
					exit(-1);
				}
				listen(softServe, 5);
				struct sockaddr_in client_addr;
				socklen_t len = sizeof(client_addr);

				int newsockfd = accept(softServe, (struct sockaddr*)&client_addr, &len);
				Message temp1;
				if(newsockfd < 0){
					//std::cout<<"newsock is bad"<<std::endl;
					exit(-1);
				}
				//std::cout<<"before receiving"<<std::endl;
				recUnicast(temp1, newsockfd);
				//std::cout<<"after receiving from slaves"<<std::endl;
				if(strcmp(temp1.hostname, "thor")==0){
					for(unsigned int j = second; j < third; j++){
						strcpy(msg.passwds[j], temp1.passwds[j]);
					}
				}
				if(strcmp(temp1.hostname,"olaf") == 0){
					for(unsigned int j = third; j < fourth; j++){
						strcpy(msg.passwds[j], temp1.passwds[j]);
					}
				}
				if(strcmp(temp1.hostname, "graculus") == 0){
					for(unsigned int j = fourth; j < fifth; j++){
						strcpy(msg.passwds[j], temp1.passwds[j]);
					}
				}
				close(newsockfd);
				close(softServe);
				//std::cout<<"after closing sockets"<<std::endl;
			}
			
			
		}

		//makes new socket to pass back uniCast password
		//code taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=0a06d645-4145-445c-9cae-dfdb9c770e52
		if(strcmp(hostname, "grolliffe") == 0){
			BT.join();
			//std::cout<<"time to send back"<<std::endl;
			uniSock = socket(AF_INET, SOCK_STREAM, 0);
			if(uniSock < 0) exit(-1);
			//std::cout<<"hostname to send back: "<<msg.hostname<<std::endl;
			struct hostent *server = gethostbyname(actualHost);
			if(server == NULL){ 
				perror("server went bad");
				exit(-1);
			}
			struct sockaddr_in serv_addr;
			bzero((char*)  &serv_addr, sizeof(serv_addr));
			serv_addr.sin_family = AF_INET;
			bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr, server->h_length);
			serv_addr.sin_port = returnPort;
			if(connect(uniSock, (struct sockaddr*)&serv_addr, sizeof(serv_addr))<0){
				perror("connect went bad");
				exit(-1);
			} 
			//sends back message via unicast TCP
			sendUnicast(msg, uniSock);
			//time(&end);
			//double seconds = difftime(end, start);
			//std::cout<<"Runtime: "<<seconds<<std::endl;
			//std::cout<<"stop"<<std::endl;
			//std::cout<<"joining threads"<<std::endl;
			for(std::thread *thread : threads)
				thread->join();
			threads.clear();
			//std::cout<<"threads joined"<<std::endl;
			
			break;
		}
		
	}
	//std::cout<<"closing socket"<<std::endl;
	close(sockfd);
	

}