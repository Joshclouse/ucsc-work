//jclouse@ucsc.edu
//CMPS109
//Lab2
//bounds.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "bounds.h"

/*
 * Intialise a two or three dimensional space of shape ARENA containing NUMSHAPES 
 * SHAPES, all of which may or may not be entirely contained within ARENA.
 */
static Shape *arena;
void setup(Shape *a, Shape *shapes[], int numShapes) {
    arena = a;
}


//HELPER FUNCTIONS----------------------------------------------------------------------------------------------------------------------
//Found this Algo online at: https://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
int pointInPoly(int n, Polygon *poly, float x, float y){
	int i, j, c = 0;
	for(i = 0, j = n-1; i < n; j=i++){
		if(((poly->vertices[i].y >= y) != (poly->vertices[j].y >= y))
			&& (x <= (poly->vertices[j].x - poly->vertices[i].x) * (y-poly->vertices[i].y)/(poly->vertices[j].y - poly->vertices[i].y)+ poly->vertices[i].x))
			c = !c;
	}
	return c;
}

//code for calculating the center of the polygon found here:
//https://stackoverflow.com/questions/19766485/how-to-calculate-centroid-of-polygon-in-c
//returns the coordinate that is the center of the polygon
Point PolyCenter(Polygon *poly){
	Point center;
	float signedArea = 0.0;
	float cx = 0.0;
	float cy = 0.0;
	float t = 0.0;
	int i, i1;
	//float a = 0.0;

	i1 = 1;
	for(i= 0; i < poly->numVertices; i++){
		signedArea += poly->vertices[i].x * poly->vertices[i1].y - poly->vertices[i1].x * poly->vertices[i].y;
		i1 = (i1 + 1) %poly->numVertices;
	}
	signedArea*= .5;

	i1 = 1;
	for(i = 0; i < poly->numVertices; i++)
	{
		t = poly->vertices[i].x * poly->vertices[i1].y - poly->vertices[i1].x * poly->vertices[i].y;
		cx += (poly->vertices[i].x + poly->vertices[i1].x) * t;
		cy += (poly->vertices[i].y + poly->vertices[i1].y) * t;
		i1 = (i1 + 1) % poly->numVertices;
	}
	cx = cx / (6.0 * signedArea);
	cy = cy / (6.0 * signedArea);
	center.y = cy;
	//printf("center x: %.9f \n", cx);
	center.x = cx;
	//printf("center y: %.9f \n", cy );
	center.z = 0;
	return center;
}

//finds the center of the Reuleaux Triangle
Point ReuleauxCenter(ReuleauxTriangle *RT){
	Point center;
	float totalX = 0;
	float totalY = 0;
	for(int i = 0; i < 3; i++){
		totalX += RT->vertices[i].x;
		totalY += RT->vertices[i].y;
	}
	center.x = totalX/3.0;
	center.y = totalY/3.0;
	return center;
}

//finds the center of a Reuleaux Tetrahedron
Point RTCenter(ReuleauxTetrahedron *RT){
	Point center;
	float totalX = 0;
	float totalY = 0;
	float totalZ = 0;
	for(int i = 0; i < 4; i++){
		//printf("beforeX: %.9f ", RT->vertices[i].x);
		//printf("beforeY: %.9f ", RT->vertices[i].y);
		//sprintf("beforeZ: %.9f \n", RT->vertices[i].z);
		totalX += RT->vertices[i].x;
		totalY += RT->vertices[i].y;
		totalZ += RT->vertices[i].z;
	}
	center.x = totalX/4.0;
	center.y = totalY/4.0;
	center.z = totalZ/4.0;
	return center;
}


//TWO DIM ---------------------------------------------------------------------------------------------

//Code taken from lecture
//https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=785d50a3-c653-46b8-b9f6-0c468e959ac7
//checks to see if two circles intersect
static bool CircleWithCircle(Circle *outer, Circle *inner){
	float distance = sqrt(
		pow(outer->center.x - inner->center.x,2) +
		pow(outer->center.y - inner->center.y,2));
	return distance <= outer->radius - inner ->radius;
}

//checks to see if a Polygon intersects wiht a circle
static bool PolyWithCircle(Polygon *outer, Circle *inner){
	//loop to cycle through all the vertices/edges
	float slope;
	float a;
	float b;
	float c;
	float dist;
	for(int i = 0; i < outer->numVertices; i++)
	{
		//printf("going through the loop");
		Point pointStart = outer->vertices[i];
		Point pointEnd;
		if(i != outer->numVertices-1)
			pointEnd = outer->vertices[i+1];
		else
			pointEnd = outer->vertices[0];
		slope = ((pointStart.y - pointEnd.y) / (pointStart.x - pointEnd.x));
		//if the slope is infinite
		if(slope < -FLT_MAX || slope > FLT_MAX)
		{
			//printf("slope= %.9f", slope);
			//Assigns the a b and c of the standard form of a line to calculate the distance from the center of the
			//circle
			c = pointStart.x;
			//printf("c= %.9f", c);
			a = 1;
			//printf("a= %.9f", a);
			b = 0;
		}
		//if the slope is not infinite
		else
		{
			b = 1;
			a = slope;
			c = (slope * (-1 *(pointStart.x))) + pointStart.y;
		}
		//distance between the center of the circle and the line
		//this line of code I got from: https://www.geeksforgeeks.org/check-line-touches-intersects-circle/
		dist = (fabs(a* inner->center.x + b * inner->center.y + c))/ sqrt(a*a + b*b);
		//printf("distance %.9f", dist);
		//the the distance between the two is greater than the radius of the circle, they can't be touching.
		if(inner->radius > dist){
			//printf("They intersect");
			return false;
		}
	}
	if(pointInPoly(outer->numVertices, outer, inner->center.x, inner->center.y) %2 == 0)
		return false;
	else
		return true;
	
	return false;
}

static bool CircleWithPoly(Circle *outer, Polygon *inner){
	Point polyCenter = PolyCenter(inner);
	//printf("Center x: %.9f \n", polyCenter.x);
	//printf("Center y: %.9f \n", polyCenter.y);
	//checks the distance of each vertice to the center of the cirlce. If at least one is inside 
	//and at least one is outside then we know they intersect.
	float dist;
	bool inside = false;
	bool outside = false;
	float vertx;
	float verty;
	for(int i = 0; i < inner->numVertices; i++){
		vertx = inner->vertices[i].x;
		verty = inner->vertices[i].y;
		dist = sqrt(pow((vertx - outer->center.x), 2) + pow((verty - outer->center.y), 2));
		if(dist> outer->radius)
			outside = true;
		if(dist < outer->radius)
			inside = true;
	}
	if(outside == true && inside == true){
		//printf("intersect \n");
		return false;
	}
	//checks to see if the center of the polygon lies outside the circle, then you know it can't be contained
	dist = sqrt(pow((polyCenter.x - outer->center.x), 2) + pow((polyCenter.y - outer->center.y), 2));
	if(dist > outer->radius)
		return false;
	else
		return true;
}


//code for determining if a point is within a polygon
static bool PolyWithPoly(Polygon *outer, Polygon *inner){
	bool inside = false;
	bool outside = false;
	for(int i = 0; i < inner->numVertices; i++){
		if(pointInPoly(outer->numVertices, outer, inner->vertices[i].x, inner->vertices[i].y) %2 == 0)
			outside = true;
		else if(pointInPoly(outer->numVertices, outer, inner->vertices[i].x, inner->vertices[i].y) %2 != 0)
			inside = true;

	}
	if(inside == true && outside == true){
		//printf("intersects");
		return false;
	}
	else if(inside == true && outside == false)
		return true;
	else
		return false;
	return true;
}

//code for determining whether a circle is within the bounds of a Reuleaux Triangle
static bool ReuleauxWithCircle(ReuleauxTriangle *outer, Circle *inner){
	float radius;
	bool intersect = false;
	float distance;
	radius = sqrt(pow(outer->vertices[0].x - outer->vertices[1].x, 2) + 
			 pow(outer->vertices[0].y - outer->vertices[1].y, 2));
	//printf("radius: %.9f \n", radius);
	for(int i = 0; i < 3; i++){
		distance = sqrt(pow(outer->vertices[i].x - inner->center.x, 2) +
				   pow(outer->vertices[i].y - inner->center.y, 2));
		//printf("distance: %.9f \n", distance);
		//if the distance between the two centers plus the radius of the inner circle is 
		//longer than the radius of the outercircle, you know that they must intersect
		if(distance + inner->radius > radius){
			//printf("they should intersect here");
			intersect = true;
		}
	}
	if(intersect == true)
		return false;
	else
		return true;
}

//code for determining whether a polygon is inside a reuleax triangle
static bool ReuleauxWithPoly(ReuleauxTriangle *outer, Polygon *inner){
	bool inside = true;
	float radius;
	float distance;
	radius = sqrt(pow(outer->vertices[0].x - outer->vertices[1].x, 2) + 
			 pow(outer->vertices[0].y - outer->vertices[1].y, 2));
	//printf("raius: %.9f \n", radius);
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < inner->numVertices; j++){
			distance = sqrt(pow(inner->vertices[j].x - outer->vertices[i].x, 2) + 
			 		   pow(inner->vertices[j].y - outer->vertices[i].y, 2));
			//printf("distance: %.9f\n", distance);
			if(distance > radius){
				//printf("set to false");
				inside = false;
			}
		}
	}
	return inside;
}

static bool PolyWithReuleaux(Polygon *outer, ReuleauxTriangle *inner){
	bool inside = true;
	int count = 0;
	Point center = ReuleauxCenter(inner);
	//printf("Center X: %.9f\n", center.x);
	//printf("Center Y: %.9f\n", center.y);
	if(pointInPoly(outer->numVertices, outer, center.x, center.y) %2 == 0)
		return false;
	float slope;
	float a;
	float b;
	float c;
	float dist;
	float radius;
	radius = sqrt(pow(inner->vertices[0].x - inner->vertices[1].x, 2) + 
			 pow(inner->vertices[0].y - inner->vertices[1].y, 2));
	for(int i = 0; i < outer->numVertices; i++)
	{
		count = 0;
			//printf("going through the loop");
			Point pointStart = outer->vertices[i];
			Point pointEnd;
			if(i != outer->numVertices-1)
				pointEnd = outer->vertices[i+1];
			else
				pointEnd = outer->vertices[0];
		slope = ((pointStart.y - pointEnd.y) / (pointStart.x - pointEnd.x));
		//if the slope is infinite
		if(slope < -FLT_MAX || slope > FLT_MAX)
		{
			//printf("slope= %.9f", slope);
			//Assigns the a b and c of the standard form of a line to calculate the distance from the center of the
			//circle
			c = pointStart.x;
			//printf("c= %.9f", c);
			a = 1;
			//printf("a= %.9f", a);
			b = 0;
		}
		//if the slope is not infinite
		else
		{
			b = 1;
			a = slope;
			c = (slope * (-1 *(pointStart.x))) + pointStart.y;
		}
			//distance between the center of the circle and the line
			//this line of code I got from: https://www.geeksforgeeks.org/check-line-touches-intersects-circle/
			for(int j = 0; j < 3; j++){
				dist = (fabs(a* inner->vertices[j].x + b * inner->vertices[j].y + c))/ (sqrt(a*a + b*b));
			    //printf("distance %.9f\n", dist);
				//printf("radius %.9f\n", radius);
				//the the distance between the two is greater than the radius of the circle, they can't be touching.
				if(radius > dist){
					count++;
					//printf("Count: %d\n", count);
				}
				if(count == 3){
					inside = false;
				}
			}
		
	}
	return inside;
}

static bool CircleWithReuleaux(Circle *outer, ReuleauxTriangle *inner){
	float distance;
	//printf("something");
	for(int i = 0; i < 3; i++){
		distance = sqrt(pow(inner->vertices[i].x - outer->center.x, 2) + 
			 		   pow(inner->vertices[i].y - outer->center.y, 2));
		if(distance > outer->radius)
			return false;
	}
	return true;
}

static bool ReuleauxWithReuleaux(ReuleauxTriangle *outer, ReuleauxTriangle *inner){
	bool inside = true;
	float radius;
	float distance;
	radius = sqrt(pow(outer->vertices[0].x - outer->vertices[1].x, 2) + 
			 pow(outer->vertices[0].y - outer->vertices[1].y, 2));
	//printf("raius: %.9f \n", radius);
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			distance = sqrt(pow(inner->vertices[j].x - outer->vertices[i].x, 2) + 
			 		   pow(inner->vertices[j].y - outer->vertices[i].y, 2));
			//printf("distance: %.9f\n", distance);
			if(distance > radius){
				//printf("set to false");
				inside = false;
			}
		}
	}
	return inside;
}

//THREE DIM-----------------------------------------------------------------------------
//calculates whether a sphere is within a sphere
static bool SphereWithSphere(Sphere *outer, Sphere* inner){
	float distance;
	distance = sqrt(pow(inner->center.x - outer->center.x, 2) + 
			 		   pow(inner->center.y - outer->center.y, 2) +
			 		   pow(inner->center.z - outer->center.z, 2));
	//if the distance between the two centers plus the additional radius of the smaller
	//sphere is larger than the arena sphere's radius, you know it can't fit inside the larger
	if(outer->radius < inner->radius + distance)
		return false;
	return true;
}

//calculates whether a sphere is within an Reuleax Tetrahedron
static bool ReuleauxTetrahedronWithSphere(ReuleauxTetrahedron *outer, Sphere *inner){
	float radius;
	bool intersect = false;
	float distance;
	radius = sqrt(pow(outer->vertices[0].x - outer->vertices[1].x, 2) + 
			 pow(outer->vertices[0].y - outer->vertices[1].y, 2) +
			 pow(outer->vertices[0].z - outer->vertices[1].z, 2));
	//printf("radius: %.9f \n", radius);
	for(int i = 0; i < 4; i++){
		distance = sqrt(pow(inner->center.x - outer->vertices[i].x, 2) + 
			 		   pow(inner->center.y - outer->vertices[i].y, 2) +
			 		   pow(inner->center.z - outer->vertices[i].z, 2));
		//printf("distance: %.9f \n", distance);
		//if the distance between the two centers plus the radius of the inner circle is 
		//longer than the radius of the outercircle, you know that they must intersect
		if(distance + inner->radius > radius){
			//printf("they should intersect here");
			intersect = true;
		}
	}
	if(intersect == true)
		return false;
	else
		return true;
}

//calculates whether the RT is within the sphere
static bool SphereWithReuleauxTetrahedron(Sphere *outer, ReuleauxTetrahedron *inner){
	float distance;
	//printf("something");
	//Point centroid = RTCenter(inner);
	//	printf("CenterX afterX: %.9f ", centroid.x);
	//	printf("CenterY afterY: %.9f ", centroid.y);
	//	printf("CenterZ afterZ: %.9f\n", centroid.z);

	for(int i = 0; i < 4; i++){
		    //printf("afterX: %.9f ", inner->vertices[i].x);
			//printf("afterY: %.9f ", inner->vertices[i].y);
			//printf("afterZ: %.9f \n", inner->vertices[i].z);
			//printf("outerX: %.9f ",outer->center.x);
			//printf("outerY: %.9f ",outer->center.y);
			//printf("outerZ: %.9f \n",outer->center.z);
		distance = sqrt(pow(inner->vertices[i].x - outer->center.x, 2) + 
			 		   pow(inner->vertices[i].y - outer->center.y, 2) +
			 		   pow(inner->vertices[i].z - outer->center.z, 2));
		//printf("Distance: %.9f\n", distance);
		//printf("radius: %.9f\n", outer->radius);
		if(distance-.00015 > outer->radius)
			return false;
	}
	return true;
}

static bool RTwithRT(ReuleauxTetrahedron *outer, ReuleauxTetrahedron *inner){
	bool inside = true;
	float radius;
	float distance;
	radius = sqrt(pow(outer->vertices[0].x - outer->vertices[1].x, 2) + 
			 pow(outer->vertices[0].y - outer->vertices[1].y, 2) +
			 pow(outer->vertices[0].z - outer->vertices[1].z, 2));
	//printf("raius: %.9f \n", radius);
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			distance = sqrt(pow(inner->vertices[j].x - outer->vertices[i].x, 2) + 
			 		   pow(inner->vertices[j].y - outer->vertices[i].y, 2) +
			 		   pow(inner->vertices[j].z - outer->vertices[i].z, 2));
			//printf("distance: %.9f\n", distance);
			if((distance -.00015) > radius){
				//printf("set to false");
				inside = false;
			}
		}
	}
	return inside;
}


/*
 * Move the centroid of SHAPE to POINT. 
 *
 * Return TRUE if SHAPE remains entirely within the space defined by the 
 * ARENA parameter of setup(), FALSE otherwise.
 */
bool move(Shape *shape, Point *point) {
	if(arena->type == CIRCLE && shape->type == CIRCLE){
		Circle *inner = (Circle *) shape;
		inner->center.x = point->x;
		inner->center.y = point->y;
		return CircleWithCircle((Circle *)arena, inner);
	}
	if(arena->type == POLYGON && shape->type == CIRCLE){
		Circle *inner = (Circle *) shape;
		inner->center.x = point->x;
		inner->center.y = point->y;
		return PolyWithCircle((Polygon *)arena, inner);
	}
	if(shape->type == POLYGON){
		Polygon *Poly = (Polygon *) shape;
		Point polyCenter = PolyCenter(Poly);
		//finds the distance in X and Y from the center of the polygon to the new point
		float moveX = fabs(polyCenter.x - point->x);
		float moveY = fabs(polyCenter.y - point->y);
		//if the point needs to move to the left it sets moveX to negative
		if(polyCenter.x > point->x){
			moveX *= -1.0;
		}
		//if the point needs to move down it sets moveY to negative
		if(polyCenter.y > point->y){
			moveY *= -1.0;
		}
		//moves all the points of the polygon accordingly
		for(int i = 0; i < Poly->numVertices; i++){
			Poly->vertices[i].x += moveX;
			Poly->vertices[i].y += moveY;
		}
		if(arena->type == CIRCLE)
			return CircleWithPoly((Circle *) arena, Poly);
		else if(arena->type == POLYGON)
			return PolyWithPoly((Polygon *) arena, Poly);
		else if(arena->type == REULEAUX_TRIANGLE)
			return ReuleauxWithPoly((ReuleauxTriangle *) arena, Poly);

	}
	if(arena->type == REULEAUX_TRIANGLE && shape->type == CIRCLE){
		Circle *inner = (Circle *) shape;
		inner->center.x = point->x;
		inner->center.y = point->y;
		return ReuleauxWithCircle((ReuleauxTriangle *) arena, inner);
	}
	if(shape->type == REULEAUX_TRIANGLE){
		ReuleauxTriangle *RT = (ReuleauxTriangle *) shape;
		
		Point RTCenter = ReuleauxCenter(RT);
		//finds the distance in X and Y from the center of the polygon to the new point
		float moveX = fabs(RTCenter.x - point->x);
		float moveY = fabs(RTCenter.y - point->y);
		//if the point needs to move to the left it sets moveX to negative
		if(RTCenter.x > point->x){
			moveX *= -1.0;
		}
		//if the point needs to move down it sets moveY to negative
		if(RTCenter.y > point->y){
			moveY *= -1.0;
		}
		//moves all the points of the polygon accordingly
		for(int i = 0; i < 3; i++){
			RT->vertices[i].x += moveX;
			RT->vertices[i].y += moveY;
			
		}
		
		if(arena->type == POLYGON){
			return PolyWithReuleaux((Polygon *) arena, RT);
		}
		if(arena->type == CIRCLE){
			return CircleWithReuleaux((Circle *) arena, RT);
		}
		if(arena->type == REULEAUX_TRIANGLE)
			return ReuleauxWithReuleaux((ReuleauxTriangle *) arena, RT);
	}
	//THRE DIM------------------------------------------------------------------------

	if(shape->type == SPHERE){
		Sphere *sphere = (Sphere *)shape;
		sphere->center.x = point->x;
		sphere->center.y = point->y;
		sphere->center.z = point->z;
		if(arena->type == SPHERE){
			return SphereWithSphere((Sphere *) arena, sphere);
		}
		if(arena->type == REULEAUX_TETRAHEDRON){
			return ReuleauxTetrahedronWithSphere((ReuleauxTetrahedron *) arena, sphere);
		}
	}

	if(shape->type == REULEAUX_TETRAHEDRON){
		ReuleauxTetrahedron *inner = (ReuleauxTetrahedron *) shape;
		Point centroid = RTCenter(inner);
		//printf("CenterX Before: %.9f ", centroid.x);
		//printf("CenterY Before: %.9f ", centroid.y);
		//printf("CenterZ Before: %.9f\n", centroid.z);
		//printf("Point->x: %.9f point->y: %.9f point->z: %.9f\n", point->x, point->y, point->z);
		//finds the distance in X and Y and Z from the center of the polygon to the new point
		float moveX = fabs(centroid.x - point->x);
		float moveY = fabs(centroid.y - point->y);
		float moveZ = fabs(centroid.z - point->z);
		//printf("ShouldMove: %.9f  %.9f  %.9f\n", moveX, moveY, moveZ);
		//if the point needs to move to the left it sets moveX to negative
		if(centroid.x > point->x){
			moveX *= -1.0;
		}
		//if the point needs to move down it sets moveY to negative
		if(centroid.y > point->y){
			moveY *= -1.0;			
		}
		//if the point needs to move backward sets moveZ to negative
		if(centroid.z > point->z){
			moveZ *= -1.0;
		}
		//moves all the points of the polygon accordingly
		for(int i = 0; i < 4; i++){
			
			inner->vertices[i].x += moveX;
			inner->vertices[i].y += moveY;
			inner->vertices[i].z += moveZ;
			//printf("afterX: %.9f ", inner->vertices[i].x);
			//printf("afterY: %.9f ", inner->vertices[i].y);
			//printf("afterZ: %.9f\n ", inner->vertices[i].z);
			
		}
		centroid = RTCenter(inner);
		//printf("CenterX afterX: %.9f ", centroid.x);
		//printf("CenterY afterY: %.9f ", centroid.y);
		//printf("CenterZ afterZ: %.9f\n", centroid.z);
		
		if(arena->type == SPHERE){
			return SphereWithReuleauxTetrahedron((Sphere *) arena, inner);
		}
		if(arena->type == REULEAUX_TETRAHEDRON){
			return RTwithRT((ReuleauxTetrahedron *) arena, inner);
		}
	}
	return false;
}
