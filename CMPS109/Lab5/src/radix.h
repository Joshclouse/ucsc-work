/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include <vector>
#include <functional>
#include <thread>

/*
 * Simple multi-threaded Radix Sort with support for Most Significant Digit 
 * sorting only.
 */
class RadixSort {
public:
    /*
     * Create a multi-threaded RadiX Sort restricted to using no more than 
     * CORES processor cores.
     */
	unsigned int coreLimit;
	int numLists;
	unsigned int activeThreads;
    RadixSort(const unsigned int cores);

    /*
     * Perform an in-place Most Significant Digit Radix Sort on each list of
     * unsigned integers in LISTS.
     */
    void msd(std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists);
    static int charAt(std::string s, unsigned int i);
    static void sort(unsigned int s[], unsigned int aux[], int lo, int hi, int at);
    static void sort2(std::vector<unsigned int> &list);
};