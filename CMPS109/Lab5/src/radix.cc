/*
 * Copyright (C) 2018 David C. Harrison. All right reserved.
 *
 * You may not use, distribute, publish, or modify this code without 
 * the express written permission of the copyright holder.
 */

#include "radix.h"
#include <iostream>
#include <string>
#include <future>
#include <thread>
#include <chrono>

using namespace std;

RadixSort::RadixSort(const unsigned int cores) { 
    coreLimit = cores;
}



//code taken from lecture https://opencast-player-1.lt.ucsc.edu:8443/engage/theodul/ui/core.html?id=bdacac7d-79ee-4ed5-a25b-6f1f3af9cb27
static int R = 2<<8;

int RadixSort::charAt(std::string s, unsigned int i){
	if(i < s.length())
		return s.at(i);
	return -1;
}

void RadixSort::sort(unsigned int s[], unsigned int aux[], int lo, int hi, int at){
	//cout<< "in sort"<< endl;
	if(hi <= lo)
		return;
	int count[R+2];
	for(int i = 0; i <= R+2; i++){
		count[i] = 0;
	}
	for(int i = lo; i <= hi; ++i){
		count[charAt(std::to_string(s[i]), at)+2]++;
	}
	for(int i = 0; i < R+1; ++i){
		count[i+1] += count[i];
	}
	for(int i = lo; i <= hi; i++){
		aux[count[charAt(std::to_string(s[i]), at)+1]++] = s[i];
	}
	for(int i = lo; i <= hi; i++){
		s[i] = aux[i-lo];
	}
	for(int r = 0; r < R; ++r){
		sort(s, aux, lo+count[r], lo+count[r+1]-1, at+1);
	}
}

void RadixSort::sort2(vector<unsigned int> &list){
	//cout<<"hello world"<< endl;
	int len = list.size();
	unsigned int s[len];
	for(int j = 0; j < len; j++){
		//cout<<"initial numbers1: "<<list[j]<<endl;
		s[j] = list[j];
		//cout<<"initial numbers2: "<<s[j]<<endl;
	}
	
	unsigned int aux[len];

	int lo = 0;
	int hi = len -1;
	int at = 0;
	sort(s, aux, lo, hi, at);
	for(int i = 0; i < len; i++){
		list[i]= s[i];
	}
}

void RadixSort::msd(std::vector<std::reference_wrapper<std::vector<unsigned int>>> &lists) { 
    numLists = lists.size();
    
    //activeThreads = 0;
    vector<thread> theBoys;
    for(vector<unsigned int> &list: lists){
    	if(theBoys.size() == coreLimit){
	    	theBoys[0].join();
    		theBoys.erase(theBoys.begin());
    		//cout<<"too big"<<endl;
    	}
    	//cout<<"before push"<<endl;
    	theBoys.push_back(thread(sort2, ref(list)));
    	//cout<<"after push"<<endl;
    	//cout<<"size: "<<theBoys.size()<<endl;
			//activeThreads++;
		
			//open = true;
			//finished.set_value(true);
    	//auto status = future.wait_for(0ms);
    	//cout<<"thread joining"<<endl;
    	//sorter.join();
    	//if(status == std::future_status::ready)
    		//activeThreads--;

    	// thread sorter(sort2, ref(list));
	    // sorter.join();

    }
    for(auto& th: theBoys){
    	//cout<<"join1"<<endl;
    	//cout<<"i: "<<i<<endl;
    	th.join();
    	//cout<<"join"<<endl;
    }

 }
// int main(){
// 	unsigned int s[] = {43, 102, 11, 21, 32, 110, 34, 99, 745};
// 	sort(s,9);
// 	for(int i = 0; i < 9; i++)
// 		std::cout<< std::to_string(s[i]) << std::endl;
// }