//Graph.c
//Josh Clouse
//jclouse@ucsc.edu
//pa4



#include<stdio.h>
#include<stdlib.h>
#include "Graph.h"
#include "List.h"

//structs-----------------------------------------------------

//private GraphObj type
typedef struct GraphObj{
	List *neighbor; 
	int *color;
	int *parent;
	int *distance;
	int order;
	int size;
	int currentVert;
}GraphObj;


//Constructors-Destructors------------------------------------------
//newGraphObj
//retruns reference to new Graph object.
//private
Graph newGraph(int n)
{
	Graph G = malloc(sizeof(GraphObj));
	G->order = n;
	G->size = 0;
	G->currentVert = NIL;
	G->neighbor = calloc(n+1, (sizeof(List)));
	//initializes all the lists in graph
	for(int i = 0; i < n + 1; i++)
	{
		G->neighbor[i] = newList();
	}
	//makes all the colors white
	G->color = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->color[i] = 1;
	}
	//makes all the parentes NIL
	G->parent = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->parent[i] = NIL;
	}
	//makes all the distances INF
	G->distance = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->distance[i] = INF;
	}
	return G;
}

//freeGraph()
//frees heap memory pointed to by *pG, sets *pG to NULL
void freeGraph(Graph* pG)
{
	if(pG != NULL && *pG != NULL)
	{
		free((*pG)->color);
        	(*pG)->color = NULL;
        	free((*pG)->parent);
        	(*pG)->parent = NULL;
        	free((*pG)->distance);
	        (*pG)->distance = NULL;

		for(int i = 0; i < (*pG)->order + 1; i++)
		{
			freeList(&((*pG)->neighbor[i]));
		}
		free((*pG)->neighbor);
		(*pG)->neighbor = NULL;
		free(*pG);
		*pG = NULL;	
	}
}


//ACCESSOR FUNCTIONS-----------------------------------------------
//getOrder()
//gets the number of vertices of the graph
int getOrder(Graph G)
{
	return G->order;
}

//getSize()
//gets the number of edges of the graph
int getSize(Graph G)
{
	return G->size;
}

//getSource()
//gets the most recent vertex used by BFS
int getSource(Graph G)
{
	return G->currentVert;
}

//getParent()
//gets the parent of vertex u
int getParent(Graph G, int u)
{
	if(1<= u && u <= getOrder(G))
		return G->parent[u];
	printf("Error in function getParent(): Invalid vertex\n");
	exit(1);
}

//getDist()
//returns the distance from the most recent BFS source to vertex u
int getDist(Graph G, int u)
{
	if(u < 1 || u > getOrder(G))
	{
		printf("Error in function getParent(): Invalid vertex\n");
        	exit(1);
	}
	if(G->currentVert == NIL)
		return INF;
	else
		return (G->distance[u]);

}

//getPath()
//appends the vertices on the shortest path onto list L from the source to u
void getPath(List L, Graph G, int u)
{
	if(u < 1 || u > getOrder(G))
	{
		printf("Error in function getParent(): Invalid vertex\n");
		exit(1);
	}
	if(getSource(G) == NIL)
	{
		printf("Error in function getParent(): BFS() has not been run\n");
		exit(1);
	}
	int s = getSource(G);
	//if the source and input vertex are the same then return the input vertex
	if(s == u)
	{
		append(L, s);
	}
	//if the input vertex is not a descendant of source return NIL
	else if(G->parent[u] == NIL)
	{
		append(L, NIL);
	}
	//recursively add all the ancestors of the source till you get to the desired vertex
	else
	{
		getPath(L, G, G->parent[u]);
		append(L,u);
	}
	
}


//MANIPULATION PROCEDURES----------------------------------------------
//makeNull()
//deletes all edges in G restoring it to its original state
void makeNull(Graph G)
{
	for(int i = 0; i < getOrder(G) + 1; i++)
	{
		clear(G->neighbor[i]);
	}
	G->size = 0;
}

//addEdge() 
//inserts a new edge joining u to v i.e. u is added to the adjecency list of v
//and v to the adjacency list of u
void addEdge(Graph G, int u, int v)
{
	if(u < 1 || u > getOrder(G) || v < 1 || v > getOrder(G))
	{
		printf("Error in function addEdge(): invalid vertices\n");
		exit(1);
	}
	List L = G->neighbor[u];
	moveFront(L);
	//goes through all of the adjacent vertices of u and checks to see where to put v
	while(index(L) != -1)
	{
		if(get(L) > v)
		{
			insertBefore(L, v);
			break;
		}
		moveNext(L);
	}
	if(index(L) == -1)
		append(L, v);
	
	L = G->neighbor[v];
	moveFront(L);
	//goes through all the adj vertices of v and checks where to put u
	while(index(L) != -1)
        {
                if(get(L) > u)
                {
                        insertBefore(L, u);
                        break;
                }
                moveNext(L);
        }
        if(index(L) == -1)
                append(L, u);
	G->size++;

}

//addArc()
//inserts a new directed edge from u to v i.e. v is added to the adjacency List of U
void addArc(Graph G, int u, int v)
{
	
        if(u < 1 || u > getOrder(G) || v < 1 || v > getOrder(G))
        {
                printf("Error in function addEdge(): invalid vertices\n");
                exit(1);
        }
	List L = G->neighbor[u];
        moveFront(L);
	//goes through all the adj vertices of u and checks where to put v
        while(index(L) != -1)
        {
                if(get(L) > v)
                {
                        insertBefore(L, v);
                        break;
                }
                moveNext(L);
        }
        if(index(L) == -1)
                append(L, v);
	G->size++;
}

//BFS()
//runs the BFS algorithm on graph G with source s, setting the color, distance, parent
//and source fields accordingly
void BFS(Graph G, int s)
{
	if(s < 1 || s > getOrder(G))
		printf("Error in function BFS(): invalid vertex\n");
	G->currentVert = s;
	List Queue = newList();
	int x;
	int y;
	List L;
	//assings all vertices white and no parent with inf distance
	for(int i = 1; i < getOrder(G) + 1; i++)
	{
		G->color[i] = 1;
		G->parent[i] = NIL;
		G->distance[i] = INF;
	}
	//assings the source vertex grey and distance 0
	G->color[s] = 2;
	G->distance[s] = 0;
	G->parent[s] = NIL;
	append(Queue, s);
	//while there is still something in the queue
	while(length(Queue) != 0)
	{
		//assigns x to the dequeued int
		moveFront(Queue);
		x = get(Queue);
		deleteFront(Queue);
		L = G->neighbor[x];
		moveFront(L);
		//loops through all the adj vertices of x and assigns their values
		while(index(L) != -1)
		{
			y = get(L);
			if(G->color[y] == 1)
			{
				G->color[y] = 2;
				G->distance[y] = 1 + G->distance[x];
				G->parent[y] = x;
				append(Queue, y); 
			}
			moveNext(L);
		}
		//colors x black
		G->color[x] = 3;
	}
	freeList(&Queue);
}

//OTHER FUNCTIONS----------------------------------------------------------
//printGraph()
//prints out the graph's adjacency matrix
void printGraph(FILE* out, Graph G)
{
	List L;
	for(int i = 1; i < getOrder(G)+1; i++)
	{
		L = G->neighbor[i];
		if(length(L) > 0)
		{
			moveFront(L);
			fprintf(out, "%d:", i);
			printList(out, L);
		}
		else
			fprintf(out, "%d:", i);
		if(i < getOrder(G)) 
			fprintf(out, "\n");
		
	}
}
