//GraphTest.c
//Josh Clouse
//jclouse@ucsc.edu
//pa4


#include<stdio.h>
#include<stdlib.h>
#include"Graph.h"

int main(int argc, char* argv[])
{
	int i;
	int n = 10;
	Graph G = NULL;
	G = newGraph(n);
	for(i = 1; i < n; i++)
	{
		addEdge(G, i, i+1);
	}
	//tests print graph and adding verts
	printGraph(stdout, G);
	addEdge(G, 9, 6);
	addEdge(G, 8, 2);
	addEdge(G, 7, 5);	
	printGraph(stdout, G);

	//Runs DFS
	BFS(G, 3);
	//PRints the source vert
	printf("%d\n", getSource(G));
	//Prints the Distance from the source to vertex
	printf("%d\n", getDist(G, 7 ));
	List L = newList();
	//gets the path from source to vertex
	getPath(L, G, 7);
	printList(stdout, L);
	//checks order and size
	printf("size %d Order %d", getSize(G), getOrder(G));
	makeNull(G);
	printGraph(stdout, G);
	freeList(&L);
	freeGraph(&G);
	return(0);
}
