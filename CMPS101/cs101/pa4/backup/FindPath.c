//FindPath.c
//Josh Clouse
//jclouse@ucsc.edu
//pa4


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"
#include"List.h"


int main(int argc, char * argv[])
{
	FILE *in, *out;
	if(argc != 3)
	{
		printf("Usage: %s <input file> <output file>\n", argv[0]);
		exit(1);
	}

	in = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	if(in == NULL)
	{
		printf("Unable to open file %s for reading\n", argv[1]);
		exit(1);
	}
	if(out == NULL)
	{
		printf("Unable to open file %s for writing\n", argv[2]);
		exit(1);
	}
	int n, v, b;
	List L = newList();
	
	//creates a new graph of size n
	fscanf(in, "%d", &n);
	Graph G = newGraph(n);

	//assigns the next values to v and b
	fscanf(in, "%d", &v);
	fscanf(in, "%d", &b);

	while(v != 0 && b != 0)
	{
		addEdge(G, v, b);
		fscanf(in, "%d", &v);
		fscanf(in, "%d", &b);
	}
	printGraph(out, G);
	fprintf(out, "\n\n");

	fscanf(in, "%d", &v);
	fscanf(in, "%d", &b);
	while(v != 0 && b != 0)
	{
		BFS(G, v);
		if(getDist(G, b) != INF)
		{
			fprintf(out, "The distance from %d to %d is %d\n", v, b, getDist(G, b));
			fprintf(out, "A shortest %d-%d path is: ", v, b);
			getPath(L, G, b);
			printList(out, L); 
			//fprintf(out, "\n");
		}
		else
		{
                        fprintf(out, "The distance from %d to %d is infinity\n", v, b);
			fprintf(out, "No %d-%d path exists", v, b);
		}
		fprintf(out, "\n\n");
		fscanf(in, "%d", &v);
		fscanf(in, "%d", &b);
		clear(L);
	}
	fclose(out);
	fclose(in);
	freeGraph(&G);
	freeList(&L);
	return 0;
}
