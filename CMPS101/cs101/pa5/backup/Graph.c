//Graph.c
//Josh Clouse
//jclouse@ucsc.edu
//pa5



#include<stdio.h>
#include<stdlib.h>
#include "Graph.h"
#include "List.h"

//structs-----------------------------------------------------

//private GraphObj type
typedef struct GraphObj{
	List *neighbor; 
	int *color;
	int *parent;
	//int *distance;
	int *discover;
	int *finish;
	int order;
	int size;
	//int currentVert;
}GraphObj;


//Constructors-Destructors------------------------------------------
//newGraphObj
//retruns reference to new Graph object.
//private
Graph newGraph(int n)
{
	Graph G = malloc(sizeof(GraphObj));
	G->order = n;
	G->size = 0;
	//G->currentVert = NIL;
	G->neighbor = calloc(n+1, (sizeof(List)));
	//initializes all the lists in graph
	for(int i = 0; i < n + 1; i++)
	{
		G->neighbor[i] = newList();
	}
	//makes all the colors white
	G->color = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->color[i] = 1;
	}
	//makes all the parentes NIL
	G->parent = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->parent[i] = NIL;
	}
	
	//makes all the discover times NIL
	G->discover = calloc(n+1, (sizeof(int)));
	for(int i = 0; i < n + 1; i++)
	{
		G->discover[i] = UNDEF;
	}
	//makes all the finish times NIL
	G->finish = calloc(n+ 1, (sizeof(int)));
	for(int i = 0; i < n+1; i++)
	{
		G->finish[i] = UNDEF;
	}
	return G;
}

//freeGraph()
//frees heap memory pointed to by *pG, sets *pG to NULL
void freeGraph(Graph* pG)
{
	if(pG != NULL && *pG != NULL)
	{
		free((*pG)->color);
        	(*pG)->color = NULL;
        	free((*pG)->parent);
        	(*pG)->parent = NULL;
        	free((*pG)->discover);
		(*pG)->discover = NULL;
		free((*pG)->finish);
		(*pG)->finish = NULL;

		for(int i = 0; i < (*pG)->order + 1; i++)
		{
			freeList(&((*pG)->neighbor[i]));
		}
		free((*pG)->neighbor);
		(*pG)->neighbor = NULL;
		free(*pG);
		*pG = NULL;	
	}
}


//ACCESSOR FUNCTIONS-----------------------------------------------
//getOrder()
//gets the number of vertices of the graph
int getOrder(Graph G)
{
	return G->order;
}

//getSize()
//gets the number of edges of the graph
int getSize(Graph G)
{
	return G->size;
}


//getParent()
//gets the parent of vertex u
int getParent(Graph G, int u)
{
	if(1<= u && u <= getOrder(G))
		return G->parent[u];
	printf("Error in function getParent(): Invalid vertex\n");
	exit(1);
}


//getDiscover()
//gets the discover time of the specified vertex of BFS
int getDiscover(Graph G, int u)
{
	if(u < 1 || u > getOrder(G))
	{
		printf("Error in function getDiscover(): Invalid vertex\n");
		exit(1);
	}
	else
	{
		return G->discover[u];
	}
}

//getFinish()
//gets the finish time of the specified vertex of BFS
int getFinish(Graph G, int u)
{
	if(u < 1 || u > getOrder(G))
	{
		printf("Error in function getFinish(): Invalid vertex\n");
		exit(1);
	}
	else
	{
		return G->finish[u];
	}
}

//MANIPULATION PROCEDURES----------------------------------------------
//makeNull()
//deletes all edges in G restoring it to its original state
void makeNull(Graph G)
{
	for(int i = 0; i < getOrder(G) + 1; i++)
	{
		clear(G->neighbor[i]);
	}
	G->size = 0;
}

//addEdge() 
//inserts a new edge joining u to v i.e. u is added to the adjecency list of v
//and v to the adjacency list of u
void addEdge(Graph G, int u, int v)
{
	if(u < 1 || u > getOrder(G) || v < 1 || v > getOrder(G))
	{
		printf("Error in function addEdge(): invalid vertices\n");
		exit(1);
	}
	List L = G->neighbor[u];
	moveFront(L);
	//goes through all of the adjacent vertices of u and checks to see where to put v
	while(index(L) != -1)
	{
		if(get(L) > v)
		{
			insertBefore(L, v);
			break;
		}
		moveNext(L);
	}
	if(index(L) == -1)
		append(L, v);
	
	L = G->neighbor[v];
	moveFront(L);
	//goes through all the adj vertices of v and checks where to put u
	while(index(L) != -1)
        {
                if(get(L) > u)
                {
                        insertBefore(L, u);
                        break;
                }
                moveNext(L);
        }
        if(index(L) == -1)
                append(L, u);
	G->size++;

}

//addArc()
//inserts a new directed edge from u to v i.e. v is added to the adjacency List of U
void addArc(Graph G, int u, int v)
{
	
        if(u < 1 || u > getOrder(G) || v < 1 || v > getOrder(G))
        {
                printf("Error in function addEdge(): invalid vertices\n");
                exit(1);
        }
	List L = G->neighbor[u];
        moveFront(L);
	//goes through all the adj vertices of u and checks where to put v
        while(index(L) != -1)
        {
                if(get(L) > v)
                {
                        insertBefore(L, v);
                        break;
                }
                moveNext(L);
        }
        if(index(L) == -1)
                append(L, v);
	G->size++;
}

//visit()
//changes color of the vertex to grey, sets discovery time, then goes through adjacency list. Once
//done with the adj list, sets color to black and then finish time.
int visit(Graph G, List S, int i, int time)
{
        G->color[i] = 1;
	time++;
        G->discover[i] = time;
        List L = G->neighbor[i];
        moveFront(L);
        while(index(L) != -1)
        {
                if(G->color[get(L)] == 0)
                {
                        G->parent[get(L)] = i;
                        time = visit(G, S, get(L), time);
                }
                moveNext(L);
        }
        G->color[i] = 2;
	time++;
        G->finish[i] = time;
	insertAfter(S, i);
	return time;
}


//DFS(Graph G, List S)
//pre: length(S)==getOrder(G)
//Runs DFS on the Graph G using the List S as the order of viewing vertices
void DFS(Graph G, List S)
{
	if(length(S) != getOrder(G))
	{
		printf("Error in method DFS(): Invalid size of list\n");
		exit(1);
	}
	for(int i = 1; i < getOrder(G) + 1; i++)
	{
		G->color[i] = 0;
		G->parent[i] = NIL;
	}
	int time = 0;
	moveBack(S);
	while(index(S) != -1)
	{
		if(G->color[front(S)] == 0)
			time = visit(G, S, front(S), time);
		deleteFront(S);
	}
}



//OTHER FUNCTIONS----------------------------------------------------------
//transpose()
//reverses the direction of the arcs on Graph G
Graph transpose(Graph G)
{
	List L;
	Graph N = newGraph(getOrder(G));
	for(int i = 1; i < getOrder(G) + 1; i++)
	{
		L = G->neighbor[i];
		moveFront(L);
		while(index(L) != -1)
		{
			addArc(N, get(L), i);
			moveNext(L);
		}
	}
	return N;
}

//copyGraph()
//returns a copy of graph G
Graph copyGraph(Graph G)
{
	List L;
	Graph N = newGraph(getOrder(G));
	for(int i = 1; i < getOrder(G) + 1; i++)
	{
		L = G->neighbor[i];
		moveFront(L);
		while(index(L) != -1)
		{
			addArc(N, i, get(L));
			moveNext(L);
		}
	}
	return N;
}


//printGraph()
//prints out the graph's adjacency matrix
void printGraph(FILE* out, Graph G)
{
	List L;
	for(int i = 1; i < getOrder(G)+1; i++)
	{
		L = G->neighbor[i];
		if(length(L) > 0)
		{
			moveFront(L);
			fprintf(out, "%d: ", i);
			printList(out, L);
		}
		else
			fprintf(out, "%d: ", i);
		//if(i < getOrder(G)) 
		fprintf(out, "\n");
		
	}
}
