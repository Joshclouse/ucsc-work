//Queue.c
//Josh Clouse
//jclouse@ucsc.edu
//pa2

#include<stdio.h>
#include<stdlib.h>
#include "List.h"

//structs---------------------------------------------------------
//private NodeObj type
typedef struct NodeObj
{
	int data;
	struct NodeObj* next;
	struct NodeObj* previous;
}NodeObj;

//private Node type
typedef NodeObj* Node;

//private ListObj type
typedef struct ListObj
{
	Node front;
	Node back;
	Node cursor;
	int cursorInd;
	int length;
}ListObj;

//Constructors-Deconstructors--------------------------------------

//newNode()
//Returns reference to new Node object. Initializes next and data fields
//private
Node newNode(int data)
{
	Node N = malloc(sizeof(NodeObj));
	N->data = data;
	N->next = NULL;
	N->previous = NULL;
	return(N);
}

//freeNode()
//Frees heap memory pointed to by *pN, sets *pN to NULL
//Private
void freeNode(Node* pN)
{
	if(pN!= NULL && *pN != NULL)
	{
		free(*pN);
		*pN = NULL;
	}
}

//newList()
//Returns reference to new empty Queue object.
List newList(void)
{
	List L;
	L = malloc(sizeof(ListObj));
	L->front = L->back = L->cursor = NULL;
	L->length = 0;
	L->cursorInd = -1;
	return(L);
}

//freeList()
//Frees all heap memory associated with Queue *pL, and sets *pL to NULL.S
void freeList(List* pL)
{
	if(pL!=NULL && *pL!=NULL)
	{

		clear(*pL);
		free(*pL);
		*pL = NULL;
	}
}


//Access functions---------------------------------------------------

//length()
//gets the length of the list
int length(List L)
{
	if(L==NULL)
	{
		printf("List Error: Calling length() on a NULL List reference\n");
		exit(1);
	}	
	return(L->length);
}

//index()
//returns the index of the cursor
int index(List L)
{
	if(L == NULL)
	{
		printf("List Error: calling length() on NULL List reference\n");
		exit(1);
	}
	return(L->cursorInd);	
}

//front()
//returns the front of the List
int front(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling front() on NULL List reference\n");
                exit(1);
        }

	if(L->front== NULL)
	{
		printf("List Error: Calling front() on an empty List \n");
		exit(1);
	}
	else
		return(L->front->data);
}

//back()
//returns the last element of the list
int back(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling back() on NULL List reference\n");
                exit(1);
        }

	if(L->back == NULL)
	{
		printf("List Error: Calling back() on an empty List\n");
		exit(1);
	}
	else
		return(L->back->data);
}

//get()
//retrieves the data from the cursor
int get(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling get() on NULL List reference\n");
                exit(1);
        }

	if(L->length <= 0)
	{
		printf("List Error: Calling get() on an empty list\n");
		exit(1);
	}
	else if(L->cursorInd < 0)
	{
		printf("List Error: Calling get() on an undefined cursor\n");
		exit(1);
	}
	else
		return(L->cursor->data);
}

//equals()
//returns 1 if both the lists are equal to each other and -1 otherwise
int equals(List A, List B)
{
	if(A == NULL || B == NULL)
	{
		printf("List Error: calling equals() on NULL List reference\n");
		exit(1);
	}
	if(A->length != B->length)
		return 0;
	Node N = A->front;
	Node M = B->front;
	while(N != NULL && M != NULL)
	{
		if(N->data != M->data)
			return 0;
		else
		{
			N = N->next;
			M = M->next;
		}
	}
	return 1;	
}

//Manipulation Procedures-----------------------------------------------

//clear()
//Resets the list to an empty state
void clear(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling clear() on NULL List reference\n");
                exit(1);
        }

	if(L->length <= 0)
		return;
	while(L->length >0)
	{
		deleteFront(L);
	}
}

//moveFront()
//moves cursor to the front of the list
void moveFront(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling moveFront() on NULL List reference\n");
                exit(1);
        }
	//if(L->length <= 0)
	//{
	//	printf("List Error: calling moveFront() on an empty List\n");
	//}

	if(L->length > 0)
	{
		L->cursor = L->front;
		L->cursorInd = 0;
	}
}

//moveBack()
//moves cursor to the last element of the list
void moveBack(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling length() on NULL List reference\n");
                exit(1);
        }
	if(L->length <=0)
	{
		printf("List Error: calling moveBack on an empty List\n");
	}

	if(L->length > 0)
	{
		L->cursor = L->back;
		L->cursorInd = L->length -1;
	}
}

//movePrev()
//moves cursor back one element in the list
void movePrev(List L)
{
	if((L->cursorInd >= 0))
	{
		L->cursor = L->cursor->previous;
		L->cursorInd--;
	}
	else
	{
		L->cursor = NULL;
		L->cursorInd = -1;
	}
}

//moveNext()
//moves the cursor forward one element in the list
void moveNext(List L)
{
	if((L->cursorInd >= 0) && (L->cursorInd < L->length-1))
	{
		L->cursor = L->cursor->next;
		L->cursorInd++;
	}
	else
	{
		L->cursorInd = -1;
		L->cursor = NULL;
	}
}

//prepend()
//adds a new item to the front of the list
void prepend(List L, int data)
{
	Node n = newNode(data);
	if(L->length <= 0)
	{
		L->front = n;
		L->back = n;
	}
	else
	{
		L->front->previous = n;
		n->next = L->front;
		L->front = n;
		if(L->cursorInd != -1)
			L->cursorInd++;
	}
	L->length++;
}

//append()
//adds a new item to the end of the list
void append(List L, int data)
{
	Node n = newNode(data);
	if(L->length <= 0)
	{
		L->front = n;
		L->back = n;
	}
	else
	{
		L->back->next = n;
		n->previous = L->back;
		L->back = n;
	}
	L->length++;
}

//insertBefore()
//inserts an element before the cursor
void insertBefore(List L, int data)
{
	if(L->length <= 0)
	{
		printf("Error in function insertBefore(): List is empty\n");
		exit(1);
	}
	
	Node n = newNode(data);
	if((L->length > 0) && (L->cursorInd>0))
	{
		n->next = L->cursor;
		n->previous = L->cursor->previous;
		L->cursor->previous->next = n;
		L->cursor->previous = n;
		L->cursorInd++;
		L->length++;	
	}
	else if(L->length > 0 && L->cursorInd == 0)
	{
		prepend(L, data);
		//L->cursorInd++;
	}
	//else
	//{
	//	printf("Error in function insertBefore(): List is empty\n");
	//	exit(1);
	//}	
}

//insertAfter
//inserts an element after the cursor
void insertAfter(List L, int data)
{
	Node n = newNode(data);
	n->previous = L->cursor;
	if(L->cursor->next != NULL)
	{
		n->next = L->cursor->next;
		L->cursor->next->previous = n;
	}
	else
	{
		L->back = n;
	}
	L->cursor->next = n;
	L->length++;
}

//deleteFront()
//deletes the front element in the list
void deleteFront(List L)
{
	if(L== NULL)
        {
                printf("List Error: calling length() on NULL List reference\n");
                exit(1);
        }
	if(L-> length <= 0)
	{
		printf("List Error: calling deleteFront() on a empty List\n");
		exit(1);
	}

	Node N = L->front;
	
	if(L->length == 0)
		return;
	else if(L->length == 1)
	{
		L->cursorInd = -1;
		L->cursor = NULL;
	}
	else
	{
		L->front = L->front->next;
		L->front->previous = NULL;
		if(L->cursorInd == 0)
		{
			L->cursor = NULL;
			L->cursorInd = -1;
		}
		else
		{
			L->cursorInd--;
		}
		
	}
	freeNode(&N);
	L->length--;
	
}

//deleteBack()
//deletes the last element in the list
void deleteBack(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling length() on NULL List reference\n");
                exit(1);
        }
        if(L-> length <= 0)
        {
                printf("List Error: calling deleteFront() on a empty List\n");
		exit(1);
        }

	Node N = L->back;
	if(L->length <= 0)
		return;
	else if(L->length == 1)
	{
		L->cursorInd = -1;
		L->cursor = NULL;
	}
	else
	{
		L->back = L->back->previous;
		L->back->next = NULL;
		if(L->cursorInd == L->length -1)
		{
			L->cursor = NULL;
			L->cursorInd = -1;
		}
		
	}
	freeNode(&N);
	L->length--;
}

//delete()
//deletes the element that the cursor is on
void delete(List L)
{
	if(L == NULL)
        {
                printf("List Error: calling length() on NULL List reference\n");
                exit(1);
        }
        if(L-> length <= 0)
        {
                printf("List Error: calling deleteFront() on a empty List\n");
		exit(1);
        }
	if(L->cursorInd < 0)
	{
		printf("List Error: calling delete() on a invalid index\n");
	}
	Node N = L->cursor;
	if(L->cursor == NULL)
		return;
	if(L->cursorInd == 0)
	{
		deleteFront(L);
		return;
	}
	if(L->cursorInd == L->length -1)
	{
		deleteBack(L);
		return;
	}
	L->cursorInd = -1;
	if(L->cursor->next->next == NULL)
		L->back = L->cursor->next;
	else if(L->cursor->previous->previous == NULL)
		L->front = L->cursor->previous;
	else
	{
		L->cursor->previous->next = L->cursor->next;
		L->cursor->next->previous = L->cursor->previous;
	}
	freeNode(&N);
	L->length--;	
}

//Other Operations------------------------------------------------------------

//printList()
//Prints out the list to a file output
void printList(FILE* out, List L)
{
	Node N = L->front;
	while(N->next != NULL)
	{
		fprintf(out, "%d ", N->data);
		N = N->next;
	}
	fprintf(out, "%d", N->data);
}

//copyList()
//Returns a new list that is a copy of the original
List copyList(List L)
{
	List light = newList();
	Node N = L->front;
	while(N != NULL)
	{
		append(light, N->data);
		N = N->next;
	}
	return light;
}









