//GraphTest.c
//Josh Clouse
//jclouse@ucsc.edu
//pa5


#include<stdio.h>
#include<stdlib.h>
#include"Graph.h"

int main(int argc, char* argv[])
{
	int i;
	int n = 11;
	Graph G = NULL;
	G = newGraph(n);
	/*for(i = 1; i < n; i++)
	{
		addArc(G, i, i+1);
	}*/
	//tests print graph and adding verts
	//printGraph(stdout, G);
	addArc(G, 1, 2);
	addArc(G, 2, 5);
	addArc(G, 3, 2);
	addArc(G, 3, 5);
	addArc(G, 4, 1);
	addArc(G, 5, 4);
	addArc(G, 5, 6);
	addArc(G, 6, 3);
	addArc(G, 6, 9);
	addArc(G, 6, 10);
	addArc(G, 7, 3);
	addArc(G, 7, 6);
	addArc(G, 8, 4);
	addArc(G, 9, 4);
	addArc(G, 9, 5);
	addArc(G, 9, 8);
	addArc(G, 10, 9);
	addArc(G, 10, 11);
	addArc(G, 11, 7);	
	printGraph(stdout, G);

	//tests DFS start and finish times and parents
	List L = newList();
	for(int i = 1; i < getOrder(G) + 1; i++)
	{
		append(L, i);
	}
	printList(stdout, L);
	printf("\n");
	DFS(G, L);
	for(i = 1; i < getOrder(G) + 1; i++)
	{
		printf("%d: Discover time: %d Finish time: %d", i, getDiscover(G, i), getFinish(G, i));
		printf("Parent: %d\n", getParent(G, i));
	}
	printList(stdout, L);
	printf("\nTranspose:\n");
	
	//test to see if transpose works
	Graph F = transpose(G);
	printGraph(stdout, F);
	printf("\n");

	//test to see if copy works
	Graph P = copyGraph(G);
	printGraph(stdout, P);
	printf("\n");


	freeList(&L);
	freeGraph(&F);
	freeGraph(&P);
	freeGraph(&G);
	return(0);
}
