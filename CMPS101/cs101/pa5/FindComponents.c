//FindPath.c
//Josh Clouse
//jclouse@ucsc.edu
//pa4


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"
#include"List.h"


int main(int argc, char * argv[])
{
	FILE *in, *out;
	if(argc != 3)
	{
		printf("Usage: %s <input file> <output file>\n", argv[0]);
		exit(1);
	}

	in = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	if(in == NULL)
	{
		printf("Unable to open file %s for reading\n", argv[1]);
		exit(1);
	}
	if(out == NULL)
	{
		printf("Unable to open file %s for writing\n", argv[2]);
		exit(1);
	}
	int n, v, b;
	//List L = newList();
	
	//creates a new graph of size n
	fscanf(in, "%d", &n);
	Graph G = newGraph(n);

	//assigns the next values to v and b
	fscanf(in, "%d", &v);
	fscanf(in, "%d", &b);

	while(v != 0 && b != 0)
	{
		addArc(G, v, b);
		fscanf(in, "%d", &v);
		fscanf(in, "%d", &b);
	}
	fprintf(out, "Adjacency list representation of G:\n");
	printGraph(out, G);
	fprintf(out, "\n");

	//creates a list and fill it in with the vertices
	int comp = 0;
	List L = newList();
	for(int i = 1; i < getOrder(G)+1; i++)
	{
		append(L, i);
	}	
	//runs DFS on G and L and then on the transpose of G and L
	DFS(G, L);
	Graph T = transpose(G);
	DFS(T, L);
	moveFront(L);
	//finds the number of strongly connected components
	while(index(L) != -1)
	{
		if(getParent(T, get(L)) == NIL)
			comp++;
		moveNext(L);
	}
	fprintf(out, "G contains %d strongly connected components:\n", comp);
	//loops through the list L and prints out the connected commponents
	moveBack(L);
	int marker = 1;
	for(int i = 1; i < comp+1; i ++)
	{
		fprintf(out, "Component %d: ", i);
		//loops till it finds the start of a tree
		if(index(L) != -1)
		{
			//fprintf(out, "Made it here"); 
			while((getParent(T,get(L)) != NIL) && (index(L) != -1))
			{
				movePrev(L);
				marker++;
			}
			//if it finds a start of a tree it 
			if(index(L) != -1)
			{
				//fprintf(out, "Made it there");
				for(int j = 0; j < marker; j++)
				{
					fprintf(out, "%d ", get(L));
					if(j < marker-1)
						moveNext(L);
				}
				fprintf(out, "\n");
				for(int j = 0; j < marker; j++)
				{
					movePrev(L);
				}
			}
			marker = 1;
		}
	}


	fclose(out);
	fclose(in);
	freeGraph(&T);
	freeGraph(&G);
	freeList(&L);
	return 0;
}
