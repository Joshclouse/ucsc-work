//Matrix.java
//Pa3
//jclouse@ucsc.edu
//Josh Clouse

class Matrix
{
	private class Entry
	{
		double data;
		int column;
		Entry(int i, double d)
		{
			data = d;
			column = i;
		}

		//checks to see if two entries are equal
		public boolean equals(Object x)
		{
			Entry e;
			if(x instanceof Entry == true)
			{
				e = (Entry)x;
				if(e.data == this.data && e.column == this.column)
					return true;
				else
					return false;
			}
			return false;
		}

		//toString for Entry
		public String toString()
		{
			String output = ("(" + column + ", " + data+")");
			return output;

		}

	}
	private int size;
	private int nnz;
	private List[] matrix;
	//Makes a new nxn zero Matrix. pre n>=1
	Matrix(int n)
	{
		size = n;
		nnz = 0;
		matrix = new List[size+1];
		for(int i = 0; i < size +1; i++)
		{
			matrix[i] = new List();
		}
	}

	//ACCESSOR FUNCTIONS-----------------------------------------------------
	//Returns n, the number of rows and columns of this matrix
	int getSize()
	{
		return size;
	}

	//Returns the number of non zero entries in this matrix
	int getNNZ()
	{
		return nnz;
	}

	//Overrides the Object method and checks to see if the two matrices are equal
	public boolean equals(Object x)
	{
		Matrix M;
		//checks to see if it is a matrix
		if((x instanceof Matrix)== false)
			return false;
		else
		{
			M = (Matrix)x;
		}
		if(M.size != this.size)
		{
			return false;
		}
		else
		{
			List L;
			List W;
			//simply compares the entries
			for(int i = 1; i < this.size+1; i ++)
			{
				L = this.matrix[i];
				W = M.matrix[i];
				if(L.length() != W.length())
					return false;
				else
				{
					L.moveFront();
					W.moveFront();
					while(L.index() != -1)
					{
						if(L.get().equals(W.get()) == false)
							return false;
						L.moveNext();
						W.moveNext();
					}
				}
			}
		}
		return true;
	}

	//MANIPULATION PROCEDURES--------------------------------------------------
	//changes entry at specified location
	void changeEntry(int i, int j, double x)
	{
		if(i<1 || i > size)
			throw new RuntimeException("Matrix Error: changeEntry() called on invalid Matrix Entry");
		else if(j < 1 || j > size)
			throw new RuntimeException("Matrix Error: changeEntyr() called on invalid Matrix Entry");
		Entry ent;
		//if you're inputting a zero i.e. deleting an entry
		if(x == 0)
		{
			//if there's nothing in the list then you don't need to do anything
			if(matrix[i].length() == 0)
				return;
			else
			{
				matrix[i].moveFront();
				for(int k = 0; k < matrix[i].length(); k++)
				{
					ent = (Entry)matrix[i].get();
					//if you have reached the entry that has the same row and column then you delete the entry
					if(ent.column == j)
					{
						matrix[i].delete();
						nnz--;
						return;
					}
					matrix[i].moveNext();
				}
			}
		}
		//You're inputting something other than 0
		else
		{
			Entry e = new Entry(j, x);
			//there's nothing in the list you just append
			if(matrix[i].length() == 0)
			{
				matrix[i].append(e);
				nnz++;
				return;
			}
			//there are entries so you must sort
			else
			{
				matrix[i].moveFront();
				for(int k = 0; k < matrix[i].length(); k++)
				{
					//if you're replacing an entry i.e. there is already something at column J you must insert the new entry and
					//delete the old one.
					ent = (Entry)matrix[i].get();
					if(ent.column == j)
					{
						matrix[i].insertBefore(e);
						matrix[i].delete();
						nnz++;
						return;
					}
					//if you get to a column that is past the column of your entry, just add the entry in before
					else if(ent.column > j)
					{
						matrix[i].insertBefore(e);
						nnz++;
						return;
					}

					matrix[i].moveNext();
				}
				matrix[i].append(e);
				nnz++;
				return;
			}
		}
	}

	//sets this matrix to the zero state
	void makeZero()
	{
		for(int i = 1; i < size + 1; i ++)
		{
			matrix[i].clear();
			nnz = 0;
		}
	}

	//Makes a copy of the matrix
	Matrix copy()
	{
		Entry ent;
		Matrix newBoy = new Matrix(size);
		for(int i = 1; i < size+1; i++)
		{
			this.matrix[i].moveFront();
			//goes through the List at matrix[i] and appends the entries
			for(int j = 0; j < this.matrix[i].length(); j++)
			{
				ent = (Entry)this.matrix[i].get();
				Entry entryBoy = new Entry(ent.column, ent.data);
				newBoy.matrix[i].append(entryBoy);
				newBoy.nnz += 1;
				this.matrix[i].moveNext();
			}
		}
		return newBoy;
	}

	//returns a new matrix that is the scalar product of this Matrix with x
	Matrix scalarMult(double x)
	{
		Entry ent;
		Matrix newBoy = this.copy();
		for(int i = 1; i < size+1; i++)
		{
			newBoy.matrix[i].moveFront();
			//Loops through each list multiplying its data by x
			for(int j = 0; j < newBoy.matrix[i].length(); j++)
			{
				ent = (Entry)newBoy.matrix[i].get();
				ent.data*= x;
				newBoy.matrix[i].moveNext();
			}
		}
		return newBoy;
	}

	//returns a new Matrix that is the sum of this Matrix with M
	Matrix add(Matrix M)
	{
		if(this.getSize() != M.getSize())
			throw new RuntimeException("Matrix Error: add() is called on incompatible matrices");
		//declares a new matrix
		Matrix newBoy = new Matrix(this.size);
		List light;
		List yagami;
		List L;
		//if the matrices are equal just scalar mult it by 2
		if(this.equals(M))
		{
			newBoy = this.scalarMult(2);
			return newBoy;
		}
		//calls listAdd on each of the lists for both matrices and places the new list into the new matrix
		for(int i = 1; i < this.size + 1; i++)
		{
			light = this.matrix[i];
			yagami = M.matrix[i];
			newBoy.matrix[i] = listAdd(light, yagami);
			L = newBoy.matrix[i];
			//System.out.println(L.length());
			newBoy.nnz += L.length();
		}
		return newBoy;

	}

	//returns a new Matrix that is the difference of this matrix with M
	//pre: getSize() == M.getSize()
	Matrix sub(Matrix M)
	{
		if(this.getSize() != M.getSize())
			throw new RuntimeException("Matrix Error: sub() is called on incompatible matrices");
		//declares a new matrix
		Matrix newBoy = new Matrix(this.size);
		List light;
		List yagami;
		//if both arrays are equal to each other return an empty matrix
		if(this.equals(M))
		{
			return newBoy;
		}
		//same with list add, just call listSub on each pair of lists for the two matrices
		//and put that new LIst in the new matrix
		for(int i = 1; i < this.size + 1; i++)
		{
			light = this.matrix[i];
			yagami = M.matrix[i];
			newBoy.matrix[i] = listSub(light, yagami);
			newBoy.nnz += newBoy.matrix[i].length();
		}
		return newBoy;
	}

	//Returns a new matrix that is the transpose of this matrix
	Matrix transpose()
	{
		Matrix newBoy = new Matrix(this.size);
		List L;
		Entry ent;
		if(this.nnz == 0)
			return newBoy;
		else
		{
			//loops through the matrix and placing each element in the new matrix reversing the row and column
			for(int i = 1; i < this.size + 1; i++)
			{
				L = this.matrix[i];
				L.moveFront();
				while(L.index() != -1)
				{
					ent = (Entry)L.get();
					newBoy.changeEntry(ent.column, i, ent.data);
					L.moveNext();
				}
			}
		}
		return newBoy;
	}

	//returns a new matrix that is the product of this Matrix with M
	//re: getSize() == M.getSize()
	Matrix mult(Matrix M)
	{
		if(this.size != M.size)
			throw new RuntimeException("Matrix Error: mult() called on incompatible matrices");
		Matrix newBoy = new Matrix(this.size);
		//if there's nothing inside the matrices then just return an empty matrix
		if(this.size == 0)
			return newBoy;
		List L;
		List K;
		double tinyBoy;
		/*if(this.equals(M))
		{
			Entry ent;
			for(int i = 1; i < this.size+1; i ++)
			{
				L = this.matrix[i];
				L.moveFront();

			}
		}
		*/
		Matrix N = M.transpose();
		//makes a new transposed matrix and then dot products the Lists in both
		for(int i = 1; i < this.size + 1; i++)
		{
			L = this.matrix[i];
			for(int j = 1; j < M.size + 1; j++)
			{
				K = N.matrix[j];
				tinyBoy = dot(L, K);
				newBoy.changeEntry(i, j, tinyBoy);
			}
		}
		return newBoy;
	}

	//HELPER FUNCTIONS------------------------------------------------------------
	//Takes two lists and adds elements of the same column
	List listAdd(List L, List W)
	{
		Entry e;
		Entry p;
		List newBoy = new List();
		//checks to see if one of the lists has no elements, if that's the case just return the other one
		if(W.length() == 0)
		{
			newBoy = L;
			return newBoy;
		}
		else if(L.length() == 0)
		{
			newBoy = W;
			return newBoy;
		}
		//moves cursors to the front to begin the loop
		W.moveFront();
		L.moveFront();
		int i = 0;
		int j = 0;
		while(i < W.length() && j < L.length())
		{
			//sets e and p to their respecitve entries
			e = (Entry)W.get();
			p = (Entry)L.get();
			//if they are both in the same row and col, add them together and put the result in the new list
			if(p.column == e.column)
			{
				if(e.data + p.data != 0)
				{
					Entry ent = new Entry(e.column, e.data+p.data);
					newBoy.append(ent);
				}
				W.moveNext();
				L.moveNext();
				i++;
				j++;
			}
			//if the e column is less than the p, append e to the end of the new list
			else if(e.column < p.column)
			{
				Entry ent = new Entry(e.column, e.data);
				newBoy.append(ent);
				W.moveNext();
				i++;
			}
			//if the p column is less than e, append p to the end of the list essentially ordering the new list
			else if(e.column > p.column)
			{
				Entry ent = new Entry(p.column, p.data);
				newBoy.append(ent);
				L.moveNext();
				j++;
			}
		}
		//checks to see which of the lists didn't finish in the case that one was shorter than the other
		//and then adds the rest to the end of the new list
		if(i < W.length())
		{
			while(i < W.length())
			{
				e = (Entry)W.get();
				Entry ent = new Entry (e.column, e.data);
				newBoy.append(ent);
				W.moveNext();
				i++;
			}
		}
		else if(j < L.length())
		{
			while(j < L.length())
			{
				p = (Entry)L.get();
				Entry ent = new Entry (p.column, p.data);
				newBoy.append(ent);
				L.moveNext();
				j++;
			}
		}
		return newBoy;
	}

	//takes two lists and subtracts them
	List listSub(List W, List L)
	{
		Entry e;
		Entry p;
		List newBoy = new List();
		//checks to see if one of the lists has no elements, if that's the case just return the other one
		if(W.length() == 0)
		{
			L.moveFront();
			for(int k = 0; k < L.length(); k++)
			{
				p = (Entry)L.get();
				Entry ent = new Entry(p.column, p.data * -1);
				newBoy.append(ent);
				L.moveNext();
			}
			return newBoy;
		}
		else if(L.length() == 0)
		{
			newBoy = W;
			return newBoy;
		}
		//moves cursors to the front to begin the loop
		W.moveFront();
		L.moveFront();
		int i = 0;
		int j = 0;
		while(i < W.length() && j < L.length())
		{
			//sets e and p to their respecitve entries
			e = (Entry)W.get();
			p = (Entry)L.get();
			//if they are both in the same row and col, add them together and put the result in the new list
			if(p.column == e.column)
			{
				if(p.data != e.data)
				{
					Entry ent = new Entry(e.column, e.data-p.data);
					newBoy.append(ent);
				}
				W.moveNext();
				L.moveNext();
				i++;
				j++;
			}
			//if the e column is less than the p, append e to the end of the new list
			else if(e.column < p.column)
			{
				Entry ent = new Entry(e.column, e.data);
				newBoy.append(ent);
				W.moveNext();
				i++;
			}
			//if the p column is less than e, append p to the end of the list essentially ordering the new list
			else if(e.column > p.column)
			{
				Entry ent = new Entry(p.column, (p.data * -1));
				newBoy.append(ent);
				L.moveNext();
				j++;
			}
		}
		//checks to see which of the lists didn't finish in the case that one was shorter than the other
		//and then adds the rest to the end of the new list
		if(i < W.length())
		{
			while(i < W.length())
			{
				e = (Entry)W.get();
				Entry ent = new Entry (e.column, e.data);
				newBoy.append(ent);
				W.moveNext();
				i++;
			}
		}
		else if(j < L.length())
		{
			while(j < L.length())
			{
				p = (Entry)L.get();
				Entry ent = new Entry (p.column, (p.data * -1));
				newBoy.append(ent);
				L.moveNext();
				j++;
			}
		}
		return newBoy;
	}

	//take the dot product of two lists
	private static double dot(List P, List Q)
	{
		double hot = 0;
		Entry e;
		Entry p;
		P.moveFront();
		Q.moveFront();
		//loops through the List and for each of the columns that line up, multiplies them together
		while(P.index() != -1 && Q.index() != -1)
		{
			e = (Entry)P.get();
			p = (Entry)Q.get();
			if(e.column == p. column)
			{
				hot += p.data*e.data;
				P.moveNext();
				Q.moveNext();
			}
			else if(e.column < p.column)
			{
				P.moveNext();
			}
			else if(e.column > p.column)
			{
				Q.moveNext();
			}
		}
		return hot;
	}

	//OTHER FUNCTIONS--------------------------------------------------------------
	//Overrides Object's toString() to convert the matrix into a string
	public String toString()
	{
		String output = "";
		for(int i = 1; i < size+1; i++)
		{
			if(i != size)
			{
				if(matrix[i].length() != 0)
					output = (output + i + ": " + matrix[i] + "\n");
			}
			else
			{
				if(matrix[i].length() != 0)
					output = (output + i + ": " + matrix[i]);
			}
		}
		return output;
	}
}
