//Sparse.java
//pa3
//jclouse@ucsc.edu
//Josh Clouse

import java.io.*;
import java.util.Scanner;


class Sparse
{
	public static void main(String[] args) throws IOException
	{
		Scanner in = null;
		PrintWriter out = null;
		int size = 0;
		int numA = 0;
		int numB = 0;
		int row, col = 0;
		double data = 0;
		if(args.length < 2)
		{
			System.err.println("Usage: FileIO infile outfile");
			System.exit(1);
		}
		in = new Scanner(new File(args[0]));
		out = new PrintWriter(new FileWriter(args[1]));

		size = in.nextInt();
	
		numA = in.nextInt();
		
		numB = in.nextInt();
		
		Matrix A = new Matrix(size);
		Matrix B = new Matrix(size);
		for(int i = 0; i < numA; i++)
		{
			row = in.nextInt();
			col = in.nextInt();
			data = in.nextDouble();
			A.changeEntry(row, col, data);
		}
		for(int i = 0; i < numB; i++)
		{
			row = in.nextInt();
			col = in.nextInt();
			data = in.nextDouble();
			B.changeEntry(row, col, data);
		}
		in.close();

		out.println("A has " + A.getNNZ() + " non-zero entries:");
		out.println(A+ "\n");
		out.println("B has " + B.getNNZ() + " non-zero entries:");
		out.println(B+ "\n");
		out.println("(1.5)*A =");
		out.println(A.scalarMult(1.5)+ "\n");
		out.println("A+B =");
		out.println(A.add(B)+ "\n");
		out.println("A+A =");
		out.println(A.add(A)+ "\n");
		out.println("B-A =");
		out.println(B.sub(A)+ "\n");
		out.println("A-A =");
		out.println(A.sub(A)+ "\n");
		out.println("Transpose(A) =");
		out.println(A.transpose()+ "\n");
		out.println("A*B =");
		out.println(A.mult(B)+ "\n");
		out.println("B*B =");
		out.println(B.mult(B)+ "\n");
		out.close();
	}
}
