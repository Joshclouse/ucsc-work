Creates a sparse matrix, I.E. a large NxN matrix where most entries are left blank. Arithmetic can be performed on this matrix such as transpose, copy, add, subtract, multiply
and dot product.