//List.java
//pa1
//jclouse@ucsc.edu
//Josh Clouse

class List
{
   private class Node
   {
      //fields
      int data;
      Node next;
      Node previous;
   

	   //constructor
	   Node(int data)
	   {
	      this.data = data;
	      next = null;
	   }

	   public String toString()
	   {
	      return String.valueOf(data);
	   }

	   public boolean equals(Object x)
	   {
	      boolean eq = false;
	      Node that;
	      if(x instanceof Node)
	      {
	         that = (Node) x;
	         eq = (this.data==that.data);
	      }
	      return eq;
	   }
   }

	//fields
	private Node front;
	private Node back;
	private int length;
	private Node cursor;
	private int cursorInd;

	//constructor
	List()
	{
	   front = back = cursor = null;
	   length = 0;   
	}

	//Acess Functions-----------------------------------
	//isEmpty()
	//Returns true if this List is empty, false otherwise.
	boolean isEmpty()
	{
	   return length==0;
	}

	//length()
	//Returns the length of the list
	int length()
	{
	   return length;
	}

	//index()
	//if the cursor is defined returns the index of the cursor element,
	//otherwise returns -1
	int index()
	{
	  if(cursor == null)
	  	return -1;
	  else
	  	return cursorInd;
	}

	//front()
	//returns the front of the list
	int front()
	{
		if(length() <=0)
			throw new RuntimeException("List is Empty");
		return front.data;
	}

	//back()
	//returns the data from the last element of the list
	int back()
	{
		if(length() <= 0)
			throw new RuntimeException("List is Empty");
		return back.data;
	}

	//get()
	//returns the cursor element
	int get()
	{
		if(length() <= 0)
			throw new RuntimeException("List is empty");
		else if(index() < 0)
			throw new RuntimeException("Index out of bounds");
		return cursor.data;
	}

	//equals(List L)
	//returns true if and only if this list and L are the same
	//Integer sequence. The states of the cursors in the two lists 
	//are not used in determining equality
	boolean equals(List L)
	{
		if(L.length() != this.length())
			return false;
		else
		{
			Node n = L.front;
			Node m = this.front;
			while(n!= null && m!= null)
			{
				if(n.data != m.data)
					return false;
				else
				{
					n = n.next;
					m = m.next;
				}
			}
		}
		return true;
	}


	//Manipulation Procedures

	//clear()
	//Resets this list to its original empty state
	void clear()
	{
		front = null;
		back = null;
		length = 0;
		cursorInd = -1;
	}

	//moveFront
	//takes the cursor and moves it to the front element
	void moveFront()
	{
		if(length() >0)
		{
			cursorInd = 0;
			cursor = front;
		}
	}

	//moveBack()
	//takes the cursor and moves it to the back of the list
	void moveBack()
	{
		if(length() > 0)
		{
			cursorInd = length() -1;
			cursor = back;
		}
	}

	//movePrev()
	//if cursor is defined and not at front, moves cursor a step forward
	//if the cursor is at the front of the list it becomes undefined
	void movePrev()
	{
		if((cursorInd >= 0))
		{
			cursor = cursor.previous;
			cursorInd --;
		}
		else
		{
			cursorInd = -1;
			cursor = null;
		}
	}

	//moveNext()
	//if cursor is defioned and not at the back it will move to the next 
	//element, if it is at the back it will become undefined
	void moveNext()
	{
		if((cursorInd >= 0) && (cursorInd < length-1))
		{
			cursor = cursor.next;
			cursorInd ++;
		}
		else
		{
			cursorInd = -1;
			cursor = null;
		}
	}

	//prepend() insert new element into this list. If List is non-empty
	//insertino takes place before front element
	void prepend(int data)
	{
		Node n = new Node(data);
		if(length == 0)
		{
			
			length++;
			front = n;
			back = n;
		}
		else
		{
			
			front.previous = n;
			n.next = front;
			front = n;
			if(cursorInd != -1)
				cursorInd++;
			length ++;
		}
	}

	//append() 
	//Insert a new element onto the end of the list
	//the list is non empty
	void append(int data)
	{
		if(length == 0)
		{
			Node n = new Node(data);
			front = n;
			back = n;
		}
		else
		{
			Node n = new Node(data);
			back.next = n;
			n.previous = back;
			back = n;
		}
		length++;
	}

	//insertBefore(int data)
	//insert a new element before the cursor
	//pre: Length() > 0, index ()>= 0
	void insertBefore(int data)
	{
		if(length > 0 && cursorInd > 0)
		{
			Node n = new Node(data);
			n.next = cursor;
			n.previous = cursor.previous;
			cursor.previous.next = n;
			cursor.previous = n;
			length++;
			cursorInd ++;
		}
		else if(length > 0 && cursorInd == 0)
		{
			prepend(data);
			cursorInd++;
		}
		else if(length <= 0)
		{
			throw new RuntimeException("List is empty");

		}
		else
			throw new RuntimeException("Index is null");
	}

	//insertAfter(int data)
	//inserts new elemtn after currsor
	//pre: length ()> 0, index ()> 0
	void insertAfter(int data)
	{
		Node n = new Node(data);
		n.previous = cursor;
		if(cursor.next != null){
			n.next = cursor.next;
			cursor.next.previous = n;

		}else{
			back = n;
		}
		cursor.next = n;
		length++;		
	
	}

	//deleteFront()
	//deletes the front element. Pre: Length > 0
	void deleteFront()
	{
		
		if(front == null)
			return;
		if(front.next != null)
			front.next.previous = null;
		if(cursorInd == 0)
		{
			cursor = front.next;
			front = cursor;
		}
		else
			front = front.next;
		if(cursorInd > 0)
			cursorInd--;
		else
			cursorInd = -1;
		length--;
	}

	//deleteBack()
	//deletes the back element Pre: length()>0
	void deleteBack()
	{
		if(length == 1)
			cursorInd = -1;
		if(back == null)
			return;
		if(back.previous != null)
			back.previous.next = null;
		back = back.previous;
		if(cursor != null)
		{
			if(cursor.next == null)
				cursorInd = -1;
		}
		length--;
	}

	//delete() deletes cursors element, making cursor undefined
	//pre: length()> 0, index()>=0
	void delete()
	{
		if(cursor == null)
			return;
		if(cursorInd == 0)
		{
			deleteFront();
			return;
		}
		cursorInd = -1;
		if(cursor.next != null)
			cursor.next.previous = cursor.previous;
		else if(cursor.previous != null)
			back = cursor.previous;
		if(cursor.previous != null)
			cursor.previous.next = cursor.next;
		cursor = null;
		length--;
	}

	//toString()
	//converts list into a string
	public String toString()
	{
		String output = "";
		Node n = front;
		while(n != null)
		{
			if(n.next != null)
				output = (output + n.data + " ");
			else
				output = (output + n.data + "");
			n = n.next;
		}
		return output;
	}

	//copy()
	//returns a new List representing the same integer sequence as this
	//list. the cursor in the new list is undefined, regardless of the 
	//state of the cursor in the list. this list is unchanged.
	List copy()
	{
		Node n = front;
		List l = new List();
		while(n != null)
		{
			l.append(n.data);
			n = n.next;
		}
		return l;
	}

	//concat(List L)
	//returns a new list which is the concatenation of
	//this list followed by L. The cursor in the new list is undefined
	//, regardless of the steates of the cursors in this list and L.
	//The states of this List and L are unchanged
	/*List Concat(List L)
	{
		List m = this.copy();
		for(int i = 0; i < L.length()-1; i++)
		{

		}
	}*/
}
