//Lex.java
//jclouse
//Joshua Clouse
//PA1

import java.io.*;
import java.util.Scanner;


public class Lex
{
	public static void main(String[] args) throws IOException
	{
		Scanner in = null;
		PrintWriter out = null;
		String line = null;
		String[] token = null;
		int i, n, lineNumber = 0;

		if(args.length < 2)
		{
			System.err.println("Usage: FileIO inflie outfile");
			System.exit(1);
		}
		in = new Scanner(new File(args[0]));
		out = new PrintWriter(new FileWriter(args[1]));

		while(in.hasNextLine())
		{
			lineNumber++;
			in.nextLine();
		}
		String[] input = new String[lineNumber];
		Scanner in2 = null;
		in2 = new Scanner(new File(args[0]));
		int x = 0;
		while(in2.hasNextLine())
		{
			input[x] = in2.nextLine() + " ";
			x++;
		}
		in.close();
		in2.close();

		//Creates a list and assings it values
		List light = new List();
		light.append(0);
		//runs sorting algorithm

		//ensures that the list is more than 1 element
		if(input.length > 1)
		{
			String temp = null;
			//first level loop goes from the 2nd element and works its way up. All elements before j should be sorted.
			for(int j = 1; j < input.length; j++)
			{
				light.moveFront();
				temp = input[j];
                                //System.out.println(light.index()+"");
				//puts the cursor on the element to be sorted i.e. element j
				for(int k = 0; k < j-1; k++)
				{
					light.moveNext();
                                        //System.out.println(light.index()+"");
				}
				while((light.index() >= 0) && (temp.compareTo(input[light.get()]) < 0))
				{
				       // if(j == 9) System.out.println(light.index()+"First");
					light.movePrev();
					//if(j==9)System.out.println(light.index()+"Second");
				}
				if(light.index()== -1)
				{
					System.out.println(j+ "PRE light.index(): " + light.index());
					light.prepend(j);
				}
				else{
					System.out.println("Cursor.get() = " + light.get());	
					System.out.println(j+ "AFTER  light.index(): " + light.index());
					light.insertAfter(j);
				}
				//System.out.println(light+"");
				//testing
				light.moveFront();
				for(int p = 0; p < light.length(); p++)
				{
					System.out.print(input[light.get()]+ " ");
					light.moveNext();
				}
				System.out.println();				
			}
				
		}
		light.moveFront();
		for(int j = 0; j < input.length; j++)
		{
			out.println(input[light.get()]);
			light.moveNext();
		}
		out.close();
	}
}
