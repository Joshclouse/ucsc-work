//Lex.c
//joshua Clouse
//jclouse@ucsc.edu
//pa2


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"List.h"

#define MAX_LEN 160

int main(int argc, char * argv[])
{
	int n, count = 0;
	char line[MAX_LEN];
	FILE *in, *in2, *out;

//checks command line for correct number of arguments
	if(argc != 3)
	{
		printf("Usage: %s <input file> <output file>\n", argv[0]);
		exit(1);
	}

//open files for reading and writing
	in = fopen(argv[1], "r");
	in2 = fopen(argv[1], "r");
	out = fopen(argv[2], "w");
	if(in == NULL)
	{
		printf("Unable to open file %s for reading\n", argv[1]);
		exit(1);
	}
	if(out==NULL)
	{
		printf("Unable to open file %s for writing\n", argv[2]);
		exit(1);
	}

//read each line and then count and print tokens
	while(fgets(line, MAX_LEN, in) != NULL)
	{
		count++;
	}
	fclose(in);	
	char* stringArray[count];
	while(n < count)
	{
		stringArray[n] = malloc(sizeof(char)*MAX_LEN);
		fgets(stringArray[n], MAX_LEN, in2);
		n++;
	}
	fclose(in2);

//creates a new list
	List yagami = newList();
	append(yagami, 0);

//starts the sorting algorithm
//ensures the list is more than 1 element
	if(count > 1)
	{
		char* temp;
		for(int j = 1; j< count; j++)
		{
			temp = stringArray[j]; 
			moveFront(yagami);

		//puts the cursor on the element to bbe sorted i.e. element j
			for(int k = 0; k < j-1; k++)
			{
				moveNext(yagami);
			}
			while((index(yagami) >= 0) && (strcmp(temp, stringArray[get(yagami)]) < 0))
			{
				movePrev(yagami);
			}
			if(index(yagami) == -1)
			{
				prepend(yagami, j);
			}
			else
			{
				insertAfter(yagami, j);
			}
			moveFront(yagami);
		}
		moveFront(yagami);
		for(int j = 0; j < count; j++)
		{
			if(j < count -1)
				fprintf(out, "%s", stringArray[get(yagami)]);
			else
				fprintf(out, "%s", stringArray[get(yagami)]);
			moveNext(yagami);
		}
		fclose(out);	
	}
	freeList(&yagami);
	for(int i = count-1; i >= 0; i--)
	{
		free(stringArray[i]);
	}
	return 0;
}


















